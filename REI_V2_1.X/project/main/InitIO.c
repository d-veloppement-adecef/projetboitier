/* 
 * File:   CMInit.c
 * Author: LFRAD
 *
 * Created on 24 septembre 2021, 11:40
 */

#include <p18F97J60.h>
#include <stdio.h>
#include <stdlib.h>
#include <main.h>
#include <i2c.h>
#include <string.h>

void INITGESTIONCARTEIO(void) {
    // Enable 4x/5x/96MHz PLL on PIC18F87J10, PIC18F97J60, PIC18F87J50, etc.
    OSCTUNE = 0x40;
    //PORT A
    TRISAbits.RA0 = 0; //LED A activit� eth. D�j� d�fini dans MAC_Init
    TRISAbits.RA1 = 0; //LED B activit� eth. D�j� d�fini dans MAC_Init
    TRISAbits.RA2 = 0; //LED C  
    TRISAbits.RA3 = 0; //Inutilis�
    TRISAbits.RA4 = 0; //LED E
    TRISAbits.RA5 = 0; //Inutilis�

    //PORTAbits.RA0 = 0; //Utilis� par la fonction d'activit� du port RJ45
    //PORTAbits.RA1 = 0; //Utilis� par la fonction d'activit� du port RJ45
    PORTAbits.RA2 = 0; //LED C
    PORTAbits.RA3 = 0; //Inutilis�
    PORTAbits.RA4 = 0; //LED E
    PORTAbits.RA5 = 0; //Inutilis�

    //PORT B
    TRISBbits.RB0 = 1; //Refresh
    TRISBbits.RB1 = 1; //Raz
    TRISBbits.RB2 = 0; //IRQ0		(ZIGBEE)
    TRISBbits.RB3 = 0; //IRQ1		(ZIGBEE)
    TRISBbits.RB4 = 0; //Inutilis�
    TRISBbits.RB5 = 0; //Inutilis�
    TRISBbits.RB6 = 1; //PGC
    TRISBbits.RB7 = 1; //PGD
    
    //PORTBbits.RB0 = 0; //Entr�e
    //PORTBbits.RB1 = 0; //Entr�e
    PORTBbits.RB2 = 0; //Inutilis� 	(ZIGBEE)
    PORTBbits.RB3 = 0; //Inutilis� 	(ZIGBEE)
    PORTBbits.RB4 = 0; //Inutilis�
    PORTBbits.RB5 = 0; //Inutilis�
    //PORTBbits.RB6 = 0; //Entr�e
    //PORTBbits.RB7 = 0; //Entr�e
    
    //PORT C
    TRISCbits.RC0 = 0; //Inutilis� 
    TRISCbits.RC1 = 0; //Inutilis� 
    TRISCbits.RC2 = 0; //Inutilis�   
    TRISCbits.RC3 = 1; //Configure SCL as Input 
    TRISCbits.RC4 = 1; //Configure SDA as Input
    TRISCbits.RC5 = 0; //Inutilis�
    TRISCbits.RC6 = 0; //TX1		(RS232)
    TRISCbits.RC7 = 1; //RX1		(RS232)

    PORTCbits.RC0 = 0; //Inutilis�
    PORTCbits.RC1 = 0; //Inutilis�
    PORTCbits.RC2 = 0; //Inutilis�
    //PORTCbits.RC3 = 0; //Entr�e
    //PORTCbits.RC4 = 0; //Entr�e
    PORTCbits.RC5 = 0; //Inutilis�
    PORTCbits.RC6 = 0; //TX1		(RS232)
    //PORTCbits.RC7 = 0; //Entr�e	(RS232)
        
    //PORT D
    TRISDbits.RD0 = 0; //Inutilis�
    TRISDbits.RD1 = 0; //Inutilis�
    TRISDbits.RD2 = 0; //HOLD		(EE_SPI)
    TRISDbits.RD3 = 0; //Inutilis�
    TRISDbits.RD4 = 0; //MOSI		(ZIGBEE)
    TRISDbits.RD5 = 0; //MISO		(ZIGBEE)
    TRISDbits.RD6 = 0; //SDK		(ZIGBEE)
    TRISDbits.RD7 = 0; //SS1		(Point de test)

    PORTDbits.RD0 = 0; //Inutilis�
    PORTDbits.RD1 = 0; //Inutilis�
    PORTDbits.RD2 = 0; //Inutilis�	(EE_SPI)
    PORTDbits.RD3 = 0; //Inutilis�
    PORTDbits.RD4 = 0; //Inutilis� 	(ZIGBEE)
    PORTDbits.RD5 = 0; //Inutilis� 	(ZIGBEE)
    PORTDbits.RD6 = 0; //Inutilis� 	(ZIGBEE)
    PORTDbits.RD7 = 0; //Inutilis�	(Point de test)
    
    //PORT E
    TRISEbits.RE0 = 0; //Inutilis�
    TRISEbits.RE1 = 0; //RESET      (ZIGBEE)
    TRISEbits.RE2 = 0; //CSCON		(ZIGBEE)
    TRISEbits.RE3 = 0; //Inutilis�
    TRISEbits.RE4 = 0; //CSDATA		(ZIGBEE)
    TRISEbits.RE5 = 0; //Inutilis�
    TRISEbits.RE6 = 0; //Inutilis�
    TRISEbits.RE7 = 0; //Inutilis�

    PORTEbits.RE0 = 0; //Inutilis�
    PORTEbits.RE1 = 0; //Inutilis�	(ZIGBEE)
    PORTEbits.RE2 = 0; //Inutilis�	(ZIGBEE)
    PORTEbits.RE3 = 0; //Inutilis�
    PORTEbits.RE4 = 0; //Inutilis�	(ZIGBEE)
    PORTEbits.RE5 = 0; //Inutilis�
    PORTEbits.RE6 = 0; //Inutilis�
    PORTEbits.RE7 = 0; //Inutilis�
    
    //PORT F
    TRISFbits.RF0 = 0; //#S			(S�lection de voie)
    TRISFbits.RF1 = 0; //Inutilis�
    TRISFbits.RF2 = 0; //Inutilis�
    TRISFbits.RF3 = 0; //Inutilis�
    TRISFbits.RF4 = 0; //Inutilis�
    TRISFbits.RF5 = 0; //Inutilis�
    TRISFbits.RF6 = 0; //Inutilis�
    TRISFbits.RF7 = 0; //Inutilis�

    PORTFbits.RF0 = 0; //#S
    PORTFbits.RF1 = 0; //Inutilis�
    PORTFbits.RF2 = 0; //Inutilis�
    PORTFbits.RF3 = 0; //Inutilis�
    PORTFbits.RF4 = 0; //Inutilis�
    PORTFbits.RF5 = 0; //Inutilis�
    PORTFbits.RF6 = 0; //Inutilis�
    PORTFbits.RF7 = 0; //Inutilis�
        
    //PORT G
    TRISGbits.RG0 = 0; //LED D
    TRISGbits.RG1 = 0; //Inutilis�
    TRISGbits.RG2 = 0; //Inutilis�
    TRISGbits.RG3 = 0; //Inutilis�
    TRISGbits.RG4 = 0; //Inutilis�
    TRISGbits.RG5 = 0; //Inutilis�
    TRISGbits.RG6 = 0; //Inutilis�
    TRISGbits.RG7 = 0; //Inutilis�

    PORTGbits.RG0 = 0; //LED D
    PORTGbits.RG1 = 0; //Inutilis�
    PORTGbits.RG2 = 0; //Inutilis�
    PORTGbits.RG3 = 0; //Inutilis�
    PORTGbits.RG4 = 0; //Inutilis�
    PORTGbits.RG5 = 0; //Inutilis�
    PORTGbits.RG6 = 0; //Inutilis�
    PORTGbits.RG7 = 0; //Inutilis�
    
    //PORT H
    TRISHbits.RH0 = 0; //Inutilis�
    TRISHbits.RH1 = 0; //DTRu1		(RS232)
    TRISHbits.RH2 = 1; //DSRu		(RS232)
    TRISHbits.RH3 = 0; //Inutilis�
    TRISHbits.RH4 = 0; //Inutilis�
    TRISHbits.RH5 = 0; //Inutilis�
    TRISHbits.RH6 = 0; //Inutilis�
    TRISHbits.RH7 = 0; //Inutilis�

    PORTHbits.RH0 = 0; //Inutilis�
    PORTHbits.RH1 = 1; //DTRu1 mont� � 1 par d�faut
    //PORTHbits.RH2 = 0; //Entr�e	(RS232)
    PORTHbits.RH3 = 0; //Inutilis�
    PORTHbits.RH4 = 0; //Inutilis�
    PORTHbits.RH5 = 0; //Inutilis�
    PORTHbits.RH6 = 0; //Inutilis�
    PORTHbits.RH7 = 0; //Inutilis�    
    
    //PORT J
    TRISJbits.RJ0 = 0; //Inutilis�
    TRISJbits.RJ1 = 0; //Inutilis�
    TRISJbits.RJ2 = 0; //Inutilis�
    TRISJbits.RJ3 = 0; //Inutilis�
    TRISJbits.RJ4 = 0; //Inutilis�
    TRISJbits.RJ5 = 0; //Inutilis�
    TRISJbits.RJ6 = 0; //Inutilis�
    TRISJbits.RJ7 = 0; //Inutilis�

    PORTJbits.RJ0 = 0; //Inutilis�
    PORTJbits.RJ1 = 0; //Inutilis�
    PORTJbits.RJ2 = 0; //Inutilis�
    PORTJbits.RJ3 = 0; //Inutilis�
    PORTJbits.RJ4 = 0; //Inutilis�
    PORTJbits.RJ5 = 0; //Inutilis�
    PORTJbits.RJ6 = 0; //Inutilis�
    PORTJbits.RJ7 = 0; //Inutilis�    
    
    // D�sactivation des fonctions analogiques
    ADCON0 = 0x00; // ADON coup� : Module d�sactiv�
    ADCON1 = 0x0F; // Vdd/Vss = +/-REF, tout est en num�rique
    ADCON2 = 0xBE; // Right justify, 20TAD ACQ time, Fosc/64 (~21.0kHz)
    
    // Configure USART1
    RCSTA1bits.SPEN = 1; //Serial port is enabled (configures RX1/DT1 and TX1/CK1 pins as serial port pins)
    RCSTA1bits.RX9 = 0; //Selects 8-bit reception
    RCSTA1bits.CREN = 1; //Enables receiver

    TXSTA1bits.SYNC = 0; //Asynchronous mode
    TXSTA1bits.BRGH = 0; //Low speed
    TXSTA1bits.TXEN = 1; //Enables transmitter
    TXSTA1bits.TX9 = 0; //Selects 8-bit transmission
    
    BAUDCON1bits.BRG16 = 1; //16-bit Baud Rate Generator : SPBRGH1 and SPBRG1
    //Baud rate 1200. Voir Documentation PIC. Baud Rate 1200 => SPBRG = 2169 => 0x879
    SPBRG1 = 0x79; // Write baudrate to SPBRG1
    SPBRGH1 = 0x08; // For 16-bit baud rate generation

    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus.  Slew rate control is enabled for High-Speed mode (400 kHz)
    SSP1CON1 = 0x28; //Enable MSSP Master I2C
    SSP1CON2 = 0x00; //Clear MSSP Control register
    SSP1ADD = 0x19; // 0x19 for 400kHz  (41 666 667Hz/((4*4OOkHz))-1 = 25 ou 0x19
    
    IPR1bits.RC1IP = 1; //EUSART1 Receive Interrupt Priority bit : High priority
    RCONbits.IPEN = 1; //Enable priority levels on interrupts
    INTCONbits.GIE = 1; //Enables all high-priority interrupts (avec IPEN = 1) 
    INTCONbits.PEIE = 1; //Enables all low-priority peripheral interrupts (avec IPEN = 1)
    PIE1bits.TX1IE = 0; //EUSART1 Transmit Interrupt Enable bit : Disabled
    PIE1bits.RC1IE = 0; //EUSART1 Receive Interrupt Enable bit : Disabled
}
