#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <p18F97J60.h>
#include <main.h>
#include <i2c.h>
#include <tick.h>

//unsigned char RS232_RecoitTrame(unsigned char *cTableau, char cLongueur, unsigned char cCarac) {
//    unsigned char cBcl; //Variable de boucle
//    unsigned char cErreur; //Variable d'erreur � renvoyer
//    DWORD tTime = 0;
//
//    for (cBcl = 0; cBcl < cLongueur; cBcl++) {
//        *(cTableau + cBcl) = 0xFF;
//    }
//    *(cTableau + cBcl - 1) = 0;
//    cErreur = 'a'; //Caract�re informant qu'il n'y a pas d'erreur
//    for (cBcl = 0; cBcl < cLongueur; cBcl++) { //Boucle allant du premier caract�re � recevoir au dernier
//        tTime = TickGet();
//        while ((PIR1bits.RC1IF == 0) && (TickGet() - tTime < (DWORD) (TICK_SECOND * 15ul))); //Tant qu'aucune r�ception et WatchDog non d�pass�
//        if (RCSTA1bits.FERR) { //Si erreur FERR
//            cErreur = RCREG1; //Vidage tampon de lecture
//            cErreur = 'f'; //Caract�re d'erreur FERR
//        } else {
//            *(cTableau + cBcl) = (RCREG1 & 0x7F); //Lecture du caract�re re�u et suppression de la parit�
//        }
//        if ((RCSTA1bits.OERR) || (TickGet() - tTime >= (DWORD) (TICK_SECOND * 15ul))) { //Si erreur OERR ou d�passement watchdog
//            cErreur = 'o'; //Caract�re d'erreur OERR
//            RCSTA1bits.CREN = 0;
//            RCSTA1bits.CREN = 1;
//            break;
//        }
//        if ((*(cTableau + cBcl)) == cCarac) {
//            break;
//        }
//    }
//
//    if (cErreur != 'a') { //Si erreur 
//        for (cBcl = 0; cBcl < cLongueur; cBcl++) {
//            *(cTableau + cBcl) = 0xFF; //On vide le tableau de stockage
//        }
//        *(cTableau + cBcl - 1) = 0;
//    }
//    LEDd ^= 1;
//    return cErreur; //Renvoie de l'erreur
//}

void RS232_EnvoiTrame(unsigned char *cTableau, char cLongueur) {
    int iBcl; //Variable de boucle
    Nop();        
    while (PIR1bits.TXIF == 0); //Attente fin transmission  
    for (iBcl = 0; iBcl < cLongueur; iBcl++) { //Boucle allant du premier caract�re � envoyer au dernier
        TXREG1 = *(cTableau + iBcl); //Envoie du caract�re point�
        Nop();        
        while (PIR1bits.TXIF == 0); //Attente fin transmission  
    }
    LEDd ^= 1;
}

void RS232_EnvoiCarac(unsigned char cCarac) {
    //unsigned char cTable = cCarac;
    RS232_EnvoiTrame(&cCarac, 1);
}
