/*********************************************************************
 *
 *  Main Application Entry Point and TCP/IP Stack Demo
 *  Module for Microchip TCP/IP Stack
 *   -Demonstrates how to call and use the Microchip TCP/IP stack
 *   -Reference: Microchip TCP/IP Stack Help (TCPIP Stack Help.chm)1150

 *
 *********************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    TCPIP.h
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.11b or higher
 *                  Microchip C30 v3.24 or higher
 *                  Microchip C18 v3.36 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *      ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *      used in conjunction with a Microchip ethernet controller for
 *      the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 * File Description:
 * Change History:
 * Rev   Description
 * ----  -----------------------------------------
 * 1.0   Initial release
 * V5.36 ---- STACK_USE_MPFS support has been removed 
 ********************************************************************/
/*
 * This macro uniquely defines this file as the main entry point.
 * There should only be one such definition in the entire project,
 * and this file must define the AppConfig variable as described below.
 */



// PIC18F97J60 Configuration Bit Settings

// 'C' source line config statements

#include <p18F97J60.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// CONFIG1L
//#pragma config WDT = OFF        // D�j� d�fini dans le fichier "HWP PICDN2_ETH97.h"
#pragma config STVR = ON        // Stack Overflow/Underflow Reset Enable bit (Reset on stack overflow/underflow enabled)
//#pragma config XINST = OFF      // D�j� d�fini dans le fichier "HWP PICDN2_ETH97.h"

// CONFIG1H
#pragma config CP0 = OFF        // Code Protection bit (Program memory is not code-protected)

// CONFIG2L
//#pragma config FOSC = HSPLL     // D�j� d�fini dans le fichier "HWP PICDN2_ETH97.h"
//#pragma config FOSC2 = ON       // D�j� d�fini dans le fichier "HWP PICDN2_ETH97.h"
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor enabled)
#pragma config IESO = ON        // Two-Speed Start-up (Internal/External Oscillator Switchover) Control bit (Two-Speed Start-up enabled)

// CONFIG2H
#pragma config WDTPS = 32768    // Watchdog Timer Postscaler Select bits (1:32768)

// CONFIG3L
#pragma config EASHFT = ON      // External Address Bus Shift Enable bit (Address shifting enabled; address on external bus is offset to start at 000000h)
#pragma config MODE = MM        // External Memory Bus (Microcontroller mode, external bus disabled)
#pragma config BW = 16          // Data Bus Width Select bit (16-Bit Data Width mode)
#pragma config WAIT = OFF       // External Bus Wait Enable bit (Wait states for operations on external memory bus disabled)

// CONFIG3H
#pragma config CCP2MX = ON      // ECCP2 MUX bit (ECCP2/P2A is multiplexed with RC1)
#pragma config ECCPMX = ON      // ECCP MUX bit (ECCP1 outputs (P1B/P1C) are multiplexed with RE6 and RE5; ECCP3 outputs (P3B/P3C) are multiplexed with RE4 and RE3)
//#pragma config ETHLED = ON      // D�j� d�fini dans le fichier "HWP PICDN2_ETH97.h"


#pragma udata trame
unsigned char cTrameTest[404];
#pragma udata

unsigned char cAlim = 0x80;
unsigned char cType = 0x1F;
unsigned char cAlarme = 0x00;
unsigned char cMalDeclare = 0x01;
unsigned char cDemandeMinitel = 0;
unsigned char cAttente = 0;
unsigned char cReset = 0;
unsigned char cDemarrage = 1;

extern volatile unsigned char* pTable;

#define THIS_IS_STACK_APPLICATION

// Include all headers for any enabled TCPIP Stack functions
#include <TCPIP.h>

// Include functions specific to this stack application
#include <MainDemo.h>
#include <main.h>
#include <i2c.h>

// Declare AppConfig structure and some other supporting stack variables
APP_CONFIG AppConfig;
static unsigned short wOriginalAppConfigChecksum; // Checksum of the ROM defaults for AppConfig
BYTE AN0String[8];

// Private helper functions.
// These may or may not be present in all applications.
static void InitAppConfig(void);
//static void InitializeBoard(void);

//
// PIC18 Interrupt Service Routines
// 
// NOTE: Several PICs, including the PIC18F4620 revision A3 have a RETFIE FAST/MOVFF bug
// The interruptlow keyword is used to work around the bug when using C18

#pragma interruptlow LowISR

void LowISR(void) {
    TickUpdate();
}

#pragma interruptlow HighISR

void HighISR(void) {
    Reception();
}

#pragma code lowVector=0x18

void LowVector(void) {
    _asm goto LowISR _endasm
}
#pragma code highVector=0x8

void HighVector(void) {
    _asm goto HighISR _endasm
}
#pragma code // Return to default code section


//
// Main application entry point.
//
extern unsigned char cTableauModif[NombreVoiesCarte][4] ;

void main(void) {
    char cCompteur; //Compteur g�n�rique pour faire clignoter les DELs
    DWORD tAttente = 0, t3 = 0; //Timer g�n�rique pour faire clignoter les DELs
    //Initialisation de la carte : I/O, RS232, I2C.
    INITGESTIONCARTEIO();

    // Initialize stack-related hardware components that may be 
    // required by the UART configuration routines
    TickInit();
#if defined(STACK_USE_MPFS2)
    MPFSInit();
#endif

    // Initialize Stack and application related NV variables into AppConfig.
    InitAppConfig();


    // Initialize core stack layers (MAC, ARP, TCP, UDP) and
    // application modules (HTTP, SNMP, etc.)
    StackInit();

    RemplissageEEPROM();
    //Boucle de clignotement des LEDs
    cCompteur = 0; //Initialisation compteur
    LEDe = 1; //LED rouge allum�e
    LEDd = 0; //LED verte �teinte
    do {
        LEDe ^= 1; //Alternance LED rouge
        LEDd ^= 1; //Alternance LED verte
        t3 = TickGet(); //Mise � jour du timer
        while (TickGet() - t3 <= (TICK_SECOND / 3)); //D�lai de 500 ms
        cCompteur++; //Compteur +1
    } while (cCompteur < 5); //On sort si le compteur d�passe 5
    //Fin de boucle
    LEDe = 0; //Exctinction des LED
    LEDd = 0;

    for (cCompteur = 0; cCompteur < NombreVoiesCarte; cCompteur++) {
        for(t3 = 0; t3 < 4; t3++)
            cTableauModif[cCompteur][t3] = 0;
    }
    //Boucle principale
    while (1) {
        //Si appui du bouton de RAZ
        if (Raz == 0) {
            cCompteur = 0; //Initialisation
            LEDe = 0;
            LEDd = 0;
            do {
                LEDe ^= 1;
                LEDd ^= 1; //Clignotement LED orange
                t3 = TickGet(); //Mise � jour du timer
                while (TickGet() - t3 <= TICK_SECOND); //D�lai de 1 sec. 
                cCompteur++;
            } while ((Raz == 0)&&(cCompteur < 5)); //Tant que le bouton est appuy�, ou que le compteur est inf�rieur � 5
            if (cCompteur >= 5) { //V�rification que l'op�rateur a appuy� pendant plus de 5 sec
                LEDe = 1;
                LEDd = 1; //LED orange fixe pour signaler � l'op�rateur d'arr�ter d'appuyer
                ProtocoleReset(); //D�clenchement du protocole de r�initialisation
            }
            LEDe = 0;
            LEDd = 0; //Si moins de 5 sec. exctinction de la LED orange
        }
        // This task performs normal stack task including checking
        // for incoming packet, type of packet and calling
        // appropriate stack entity to process it.
        StackTask();

        // This tasks invokes each of the core stack application tasks
        StackApplications();
        // See if the polarty swap timer has expired (happens every 429ms)
        if ((DWORD) TickGet() - t3 > (DWORD) 3 * TICK_SECOND) {
            if (ReadPHYReg(PHSTAT2).PHSTAT2bits.LSTAT) {
            } else {// Not linked
                Swap ^= 1; //#S est modifi� pour tester si la ligne inverse fonctionne
                LEDc = Swap; //Indication par la LED que l'orientation de la ligne change
            }
            t3 = (DWORD) TickGet();
        }
        //Op�rations vis-�-vis du rack REI
        if ((DWORD)(TickGet() - tAttente) >= (TICK_SECOND * (DWORD)cAttente)) {
            cAttente = 0;
            if(cReset){ //Si dans le programme, on demande le reset
                Reset();
            }
            if (cDemandeMinitel == 0) { //Si on demande le minitel ou le modem.
                modem();
            } else {
                minitel();
            }
            tAttente = TickGet();
        }
        //Op�rations vis-�-vis de Telgat
        GenericTCPServer();
    } //Fin de la boucle principale
}


/*********************************************************************
 * Function:        void InitAppConfig(void)
 *
 * PreCondition:    MPFSInit() is already called.
 *
 * Input:           None
 *
 * Output:          Write/Read non-volatile config variables.
 *
 * Side Effects:    None
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
// MAC Address Serialization using a MPLAB PM3 Programmer and 
// Serialized Quick Turn Programming (SQTP). 
// The advantage of using SQTP for programming the MAC Address is it
// allows you to auto-increment the MAC address without recompiling 
// the code for each unit.  To use SQTP, the MAC address must be fixed
// at a specific location in program memory.  Uncomment these two pragmas
// that locate the MAC address at 0x1FFF0.  Syntax below is for MPLAB C 
// Compiler for PIC18 MCUs. Syntax will vary for other compilers.
//#pragma romdata MACROM=0x1FFF0
static ROM BYTE SerializedMACAddress[6] = {MY_DEFAULT_MAC_BYTE1, MY_DEFAULT_MAC_BYTE2, MY_DEFAULT_MAC_BYTE3, MY_DEFAULT_MAC_BYTE4, MY_DEFAULT_MAC_BYTE5, MY_DEFAULT_MAC_BYTE6};
//#pragma romdata

static void InitAppConfig(void) {
    unsigned long cE1 = 0, cE2 = 0, cE3 = 0, cE4 = 0;

    while (1) {
        // Start out zeroing all AppConfig bytes to ensure all fields are 
        // deterministic for checksum generation
        memset((void*) &AppConfig, 0x00, sizeof (AppConfig));

        AppConfig.Flags.bIsDHCPEnabled = TRUE;
        AppConfig.Flags.bInConfigMode = TRUE;
        memcpypgm2ram((void*) &AppConfig.MyMACAddr, (ROM void*) SerializedMACAddress, sizeof (AppConfig.MyMACAddr));
        //        {
        //            _prog_addressT MACAddressAddress;
        //            MACAddressAddress.next = 0x157F8;
        //            _memcpy_p2d24((char*)&AppConfig.MyMACAddr, MACAddressAddress, sizeof(AppConfig.MyMACAddr));
        //        }
        LectureIP0Masque1Pass2((unsigned char*) &cE1, (unsigned char*) &cE2, (unsigned char*) &cE3, (unsigned char*) &cE4,0);
        AppConfig.MyIPAddr.Val = cE1 | cE2 << 8ul | cE3 << 16ul | cE4 << 24ul;
        AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
        LectureIP0Masque1Pass2((unsigned char*) &cE1, (unsigned char*) &cE2, (unsigned char*) &cE3, (unsigned char*) &cE4,1);
        AppConfig.MyMask.Val = cE1 | cE2 << 8ul | cE3 << 16ul | cE4 << 24ul;
        AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
        LectureIP0Masque1Pass2((unsigned char*) &cE1, (unsigned char*) &cE2, (unsigned char*) &cE3, (unsigned char*) &cE4,2);
        AppConfig.MyGateway.Val = cE1 | cE2 << 8ul | cE3 << 16ul | cE4 << 24ul;
        AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2 << 8ul | MY_DEFAULT_PRIMARY_DNS_BYTE3 << 16ul | MY_DEFAULT_PRIMARY_DNS_BYTE4 << 24ul;
        AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2 << 8ul | MY_DEFAULT_SECONDARY_DNS_BYTE3 << 16ul | MY_DEFAULT_SECONDARY_DNS_BYTE4 << 24ul;

        // Load the default NetBIOS Host Name
        memcpypgm2ram(AppConfig.NetBIOSName, (ROM void*) MY_DEFAULT_HOST_NAME, 16);
        FormatNetBIOSName(AppConfig.NetBIOSName);


        // Compute the checksum of the AppConfig defaults as loaded from ROM
        wOriginalAppConfigChecksum = CalcIPChecksum((BYTE*) & AppConfig, sizeof (AppConfig));

        break;
    }
}
