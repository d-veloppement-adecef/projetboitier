/* 
 * File:   TrameTelgat.c
 * Author: LFRAD
 *
 * Created on 3 d�cembre 2021, 15:44
 */

#include <stdio.h>
#include <stdlib.h>
#include <p18F97J60.h>
#include <main.h>
#include <string.h>

extern char cAlim;

void TypeDeCom(unsigned char* cTrame, unsigned char cType, unsigned int* iLong) {
    cTrame[0] = STX; //Debut de texte
    cTrame[1] = 'C';
    cTrame[2] = ':';
    cTrame[3] = cType; //Type de com
    cTrame[4] = ETX; //Fin de texte
    IntToChar(CRC16(cTrame, 5), cTrame + 5, 5); //Ecriture du CRC
    cTrame[10] = CarriageReturn; //Retour � la ligne
    *iLong = 11; //Longueur de la trame � envoyer
}

void NomDuSite(unsigned char* cTrame, unsigned int* iLong) {
    unsigned char cBcl;
    unsigned char cNomSite[20];

    EESequRead(0, PageNomSite, cNomSite, 20);

    cTrame[0] = STX; //Debut de texte
    cTrame[1] = 'S';
    cTrame[2] = ':';
    for (cBcl = 3; cBcl < 23; cBcl++) { //Ecriture du nom du site
        cTrame[cBcl] = cNomSite[cBcl - 3];
    }
    cTrame[23] = ETX; //Fin de texte
    IntToChar(CRC16(cTrame, 24), cTrame + 24, 5); //Ecriture du CRC
    cTrame[29] = CarriageReturn; //Retour � la ligne
    *iLong = 30; //Longueur de la trame � envoyer
}

void ModeDeCom(unsigned char* cTrame, unsigned int* iLong) {
    cTrame[0] = STX; //Debut de texte
    cTrame[1] = 'E';
    cTrame[2] = ':';
    cTrame[3] = '0';
    cTrame[4] = ETX; //Fin de texte
    IntToChar(CRC16(cTrame, 5), cTrame + 5, 5); //Ecriture du CRC
    cTrame[10] = CarriageReturn; //Retour � la ligne
    *iLong = 11; //Longueur de la trame � envoyer
}

void TrameCable(unsigned char* cTrame, char* cVoie, unsigned int* iLong) {
    unsigned int cBcl;
    unsigned int iCompt;

    unsigned int iSeuilBas[10];
    unsigned char cEtatParaGrp[10];
    unsigned int iValeur[10];
    unsigned int iSeuilHaut[10];

    unsigned char cEtatCapteur[NombreCapteursVoie];
    unsigned int iSeuil[NombreCapteursVoie];
    unsigned int iPression[NombreCapteursVoie];
    char cVoieValide = 0;
    static char cVoieGroupe = 0;
    
    cTrame[0] = STX; //Debut de texte

    //Ecriture de la date
    cTrame[4] = '2';
    cTrame[5] = '9';
    cTrame[6] = '1';
    cTrame[7] = '1';
    cTrame[8] = '0';
    cTrame[9] = '7';
    cTrame[10] = '1';
    cTrame[11] = '5';

    if (*cVoie == 0) {
        cVoieValide = 1; //On valide la voie
        IntToChar(*cVoie, cTrame + 1, 3); //Ecriture du num�ro de la voie
        (*cVoie)++; //On incr�mente la voie � scruter
        EESequRead(0, PageParametreGroupe + 70, &cVoieGroupe, 1);
        cVoieGroupe++;
        LectureParametresGroupe(iSeuilBas, cEtatParaGrp, iValeur, iSeuilHaut); //Lecture des param�tres groupe

        //Initialisation de la valeur de comptage
        iCompt = 12;

        for (cBcl = 0; cBcl < 10; cBcl++) {
            if (cBcl != 6) { //On n'�crit pas la 7eme case des param�tres groupe
                IntToChar(cBcl + 1, cTrame + (iCompt), 3); //Ecriture de l'emplacement du capteur

                //Type de capteur : param�tre groupe = '07'
                cTrame[iCompt + 3] = '0';
                cTrame[iCompt + 4] = '7';

                IntToChar(iSeuilBas[cBcl], cTrame + (iCompt + 5), 4); //Ecriture du seuil bas

                //Distance (inutilis�e, mise � 0)
                cTrame[iCompt + 9] = '0';
                cTrame[iCompt + 10] = '0';
                cTrame[iCompt + 11] = '0';
                cTrame[iCompt + 12] = '0';
                cTrame[iCompt + 13] = '0';

                IntToChar(cEtatParaGrp[cBcl], cTrame + (iCompt + 14), 2); //Ecriture de l'etat

                IntToChar(iValeur[cBcl], cTrame + (iCompt + 16), 4); //Ecriture de la valeur

                IntToChar(iSeuilHaut[cBcl], cTrame + (iCompt + 20), 4); //Ecriture du seuil haut

                iCompt += 24; //Incr�mentation du compteur de 24 pour l'�criture du capteur suivant 
            }
        }
    } else if (*cVoie < 101) { //Si lecture des voies (Param�tres groupe en voie 0)
        cVoieValide = 0; //Mise � 0 de la validation de voie
        //cCarte = (*(cVoie) - 1) / NombreVoiesCarte; //R�cup�ration de la carte en fonction de la voie
        do {
            while ((!(cAlim & (0x01 << (*(cVoie) - 1) / NombreVoiesCarte)))&&(*cVoie < 101)) {
                (*cVoie) += NombreVoiesCarte; //On incr�mente la voie � scruter
            }
            if (*cVoie < 101) {
                if(*cVoie != cVoieGroupe){
                    LectureDonneesTelgat((*cVoie) - 1, cEtatCapteur, iSeuil, iPression); //Lecture des donn�es de la voie en cours
                    for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) {
                        if (cEtatCapteur[cBcl] != 99) {
                            cVoieValide = 1; //On valide que la voie est � envoyer
                            IntToChar(*cVoie, cTrame + 1, 3); //Ecriture du num�ro de la voie
                            break;
                        } //Si d�tection d'un capteur
                    }
                }
                (*cVoie)++; //On incr�mente la voie � scruter
            }
        } while (cVoieValide == 0 && *cVoie < 101); //Tant que la voie scrut�e n'a pas de capteur et que toutes les voies n'ont pas �t� v�rifi�s 

        if (cVoieValide == 1) { //Si la voie est valide � envoyer

            //Initialisation de la valeur de comptage
            iCompt = 12;

            for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) { //Pour les 16 capteur de la voie
                if (cEtatCapteur[cBcl] != 99) { //Si d�tection d'un capteur
                    IntToChar(cBcl + 1, cTrame + (iCompt), 3); //Ecriture de l'emplacement du capteur

                    //Type de capteur : TP = '02'
                    cTrame[iCompt + 3] = '0';
                    cTrame[iCompt + 4] = '2';

                    //Seuil bas Parametres groupe (inutilis�, mis � 0)
                    cTrame[iCompt + 5] = '0';
                    cTrame[iCompt + 6] = '0';
                    cTrame[iCompt + 7] = '0';
                    cTrame[iCompt + 8] = '0';

                    //Distance (inutilis�e, mise � 0)
                    cTrame[iCompt + 9] = '0';
                    cTrame[iCompt + 10] = '0';
                    cTrame[iCompt + 11] = '0';
                    cTrame[iCompt + 12] = '0';
                    cTrame[iCompt + 13] = '0';

                    if (cEtatCapteur[cBcl] == 80) {
                        cEtatCapteur[cBcl] = 50;
                    }
                    IntToChar(cEtatCapteur[cBcl], cTrame + (iCompt + 14), 2); //Ecriture de l'etat

                    IntToChar(iPression[cBcl], cTrame + (iCompt + 16), 4); //Ecriture de la pression

                    IntToChar(iSeuil[cBcl], cTrame + (iCompt + 20), 4); //Ecriture du seuil

                    iCompt += 24; //Incr�mentation du compteur de 24 pour l'�criture du capteur suivant 
                }
            }
        }
    }
    if (cVoieValide == 1) { //Si on a une voie � envoyer
        cTrame[iCompt] = ETX; //Fin de texte
        IntToChar(CRC16(cTrame, iCompt + 1), cTrame + iCompt + 1, 5); //Ecriture du CRC
        cTrame[iCompt + 6] = CarriageReturn; //Retour � la ligne
        *iLong = iCompt + 7; //Ecriture de la longueur de la trame
    } else { //Si toutes les voies ont �t� faites
        cTrame[0] = EOT; //EOT indiquant � Telgat que toutes les voies ont �t� envoy�es
        *iLong = 1; //Longueur de 1 pour l'envoie du EOT
        *cVoie = 0; //Remise � 0 de la voie
        cVoieGroupe = 0;
    }
}

void IntToChar(unsigned int iValeur, unsigned char *cChaineCar, char cTaille) { //Ecriture d'un int dans un string 
    char cTampon[10];
    char cBcl, cDiffLong, cLongTamp;

    sprintf(cTampon, (const far rom char*) "%u", iValeur); //Stockage de la valeur � convertir dans le tampon sous forme de chars
    cLongTamp = strlen(cTampon); //Regarde la longueur du tampon
    cDiffLong = cTaille - cLongTamp; //R�cup�re la difference de longueur entre la longueur du tampon et la taille souhait�
    for (cBcl = 1; cBcl <= cLongTamp; cBcl++) { //On d�calle au cran souhait� les caract�res stock�s dans le tampon
        cTampon[cTaille - cBcl] = cTampon[cLongTamp - cBcl];
    }
    for (cBcl = 0; cBcl < cDiffLong; cBcl++) { //On met � 0 les caract�res � gauche de la donn�e
        cTampon[cBcl] = '0';
    }
    for (cBcl = 0; cBcl < cTaille; cBcl++) { //On transfert le tampon dans le tableau de sortie
        cChaineCar[cBcl] = cTampon[cBcl];
    }
}
