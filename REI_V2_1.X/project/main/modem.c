#include <stdio.h>
#include <stdlib.h>
#include <p18F97J60.h>
#include <main.h>
#include <string.h>
#include <i2c.h>
#include <Tick.h>
extern unsigned char cAlim;
extern unsigned char cAttente;
extern unsigned char cType;
extern unsigned char cAlarme;
extern unsigned char cMalDeclare;
extern unsigned char cDemandeMinitel;
extern DWORD tTimeout;
extern unsigned char cTrameLecture[90];
extern unsigned int iTableau_version[7];
extern unsigned char cTableauModif[NombreVoiesCarte][4];
extern unsigned char cDemarrage;
extern unsigned char cCompteurErreur;
extern volatile unsigned char* pTable;

DWORD tAppelREI = 0x7FFFFFFF; //t = 0x7FFFFFFF pour d�clencher un appel REI au d�marrage

void modem(void) {
    unsigned char cIndex = 0xFF, cBcl;
    static unsigned char cTblRecept[72], cFlagGroupe, cEmplacementGroupe = VoieCarteGroupeNonDeclare;
    static DWORD t2 = 0;

    static enum _EtatModem {
        SM_HOME = 0,
        SM_LISTENING,
        SM_CLOSING,
    } EtatModem = SM_HOME;

    static enum _EtatREI {
        Accroche = 0,
        Reponse,
        Trame,
    } EtatREI = Accroche;
    switch (EtatModem) {
        case SM_HOME: //Etat par d�faut quand il n'y a aucune communication
            if (DSR) { //V�rification si le rack est connect�
                LEDd = 0; //On s'assure que la LED verte est �teinte
                if (TickGet() - t2 >= (TICK_SECOND / 2)) { //Si non, on fait clignoter la LED rouge toutes les 500ms
                    LEDe ^= 1; //Clignotement LED rouge
                    t2 = TickGet(); //Mise � jour du timer
                }
            } else { //Si oui, on fait clignoter une fois toutes les 10 secondes, pendant 100 ms, la LED verte
                LEDe = 0; //On s'assure que la LED rouge est �teinte
                if (((TickGet() - t2 >= (TICK_SECOND * 10))&&(LEDd == 0)) || //Toutes les 10 secondes...
                        ((TickGet() - t2 >= (TICK_SECOND / 5))&&(LEDd == 1))) { //On allume la LED pendant 100 ms
                    LEDd ^= 1;
                    t2 = TickGet();
                }
            }
            //Correctif mis en place : Une fois toutes les 10-15 minutes, le rack REI se met indisponible, et envoie une trame sp�ciale d�s qu'il est disponible.
            //Attendre une seconde �vite d'avoir un conflit.
            if (TickGet() - t2 >= (DWORD) (TICK_SECOND * 1ul)) {
                if (((TickGetDiv64K() - tAppelREI >= (DWORD) (TICK_HOUR >> 16ul)) || (Refresh == 0))&&(!DSR)) { //Une fois par heure, ou si l'utilisateur le demande en local ou � distance, et que le rack est disponible
                    LEDe = 0; //On �teint la LED rouge, et on allume la verte
                    LEDd = 1;
                    DTR = 0; //On demande une communication avec le rack
                    EtatModem = SM_LISTENING; //Etape suivante
                    InitRecept();
                }
            }
            break;

        case SM_LISTENING:
            switch (EtatREI) { //Gestion de la communication avec REI.
                case Accroche: //Tentatives d'accroche avec le rack REI.
                    if (TickGet() - tTimeout > TICK_SECOND * 15ul) { //Si on d�passe le temps
                        EtatModem = SM_CLOSING; //On sort de la communication
                        return;
                    }
                    cIndex = CheckRecept('a'); //On regarde si on a re�u un 'a'
                    if ((cIndex != 0xFF)&&(cTrameLecture[cIndex - 1] == 0x1B)) { //On a demand� la communication avec REI, celui-ci doit r�pondre <FF><ESC>a (0x0C 0x1B 0x61)
                        FinRecept(); //Si on a bien re�u '<ESC>a'
                        EtatREI++; //Etape suivante
                    }
                    break;
                case Reponse:
                    cTblRecept[0] = '\\'; // Pour r�pondre au rack, il faut renvoyer '\C' (0x5C 0xC3)
                    cTblRecept[1] = 'C';
                    RS232_EnvoiTrame(cTblRecept, 2);
                    InitRecept();
                    EtatREI++; //Etape suivante
                    cFlagGroupe = 0; //On s'assure que le flag "param�tre groupe" est remis � z�ro
                    break;
                case Trame: //Ici, on filtre les trames qui ne nous int�ressent pas.
                    if (TickGet() - tTimeout > TICK_SECOND * 5ul) {
                        RS232_EnvoiCarac(NACK); //On renvoie un 'NACK'
                        InitRecept();
                        cCompteurErreur++;
                        if (cCompteurErreur >= 5)
                            EtatModem = SM_CLOSING;
                        return;
                    }
                    if (!strchr(cTrameLecture, 0x04)) //On attend de recevoir un 'EOT' (0x04)), sinon on continue la r�ception
                        return;
                    pTable = (volatile unsigned char *) cTrameLecture; //On r�initialise le pointeur de lecture
                    cIndex = CheckRecept(STX); //On v�rifie qu'on a le d�but de la trame
                    if (cIndex == 0xFF) //Sinon on sort
                        return;
                    FinRecept(); //Si on a bien re�u un 'STX'
                    cCompteurErreur = 0;
                    switch (cTrameLecture[cIndex + 1]) {
                        case 'T': //Version du MCD
                            iTableau_version[0] = (int) (cTrameLecture[cIndex + 8] - 48)*100 + (int) (cTrameLecture[cIndex + 10] - 48)*10 + (int) (cTrameLecture[cIndex + 11] - 48);
                            break;
                        case 'P': //Types et pr�sence des cartes
                            cAlim = 0x1F; //On initialise la variable de pr�sence des cartes.
                            cType = 0x1F; //On initialise la variable de type des cartes.
                            for (cBcl = 0; cBcl < 5; cBcl++) { //Pour les 5 cartes
                                if (cTrameLecture[cIndex + cBcl + 2] == '0') { //Si l'�tat est '0', la carte n'existe pas, on passe le bit correspondant � 0
                                    cAlim ^= (0x01 << cBcl);
                                } else {
                                    cTableauModif[cBcl][0] = 4;
                                    if (cTrameLecture[cIndex + cBcl + 2] == 'R') {
                                        cType ^= (0x01 << cBcl);
                                    }
                                }
                            }
                            cAlarme = 0x00; //On initialise la variable des alarmes. 3 �tats sont possibles :
                            for (cBcl = 0; cBcl < 3; cBcl++) { //Pour chacun
                                if (cTrameLecture[cIndex + cBcl + 7] == 'N') { //L'alarme est surveill�e, mais pas mont�e: on note '10'
                                    cAlarme |= (0x02 << (4 - (cBcl * 2)));
                                } else if (cTrameLecture[cIndex + cBcl + 7] == 'O') { //L'alarme est mont�e : on note '11'
                                    cAlarme |= (0x03 << (4 - (cBcl * 2)));
                                }//Sinon, on laisse � 0 : l'alarme n'est pas surveill�e
                            }
                            InitEeprom(); //On r�initialise toutes les valeurs des voies dans l'EEPROM.
                            EESequRead(0x0, PageParametreGroupe + 70, &cEmplacementGroupe, 1); //On regarde si on doit traiter un param�tre groupe
                            if (cEmplacementGroupe != VoieCarteGroupeNonDeclare) { //S'il est d�clar�
                                //Initialisation param�tres groupe
                                for (cBcl = 0; cBcl < 70; cBcl++) {
                                    cTrameLecture[cBcl] = '0';
                                }
                                ConversionParamGroupeREICMS((unsigned char *) cTrameLecture);
                                //Fin initialisation
                            }
                            break;
                        case 0x1A: //Si on re�oit '<SUB>', la transmission est finie
                            if ((cEmplacementGroupe != VoieCarteGroupeNonDeclare) && (cFlagGroupe != 1)) { //V�rification que le groupe est pass�
                                cMalDeclare = 1;
                            }
                            EtatModem = SM_CLOSING; //Fermeture de la connexion
                            return;
                    }
                    if (cTrameLecture[cIndex + 1] != '0') { //S�curit� pour le NCD, et MCD 2.65+
                        switch (cTrameLecture[cIndex + 2]) {
                            case 'M': //La trame a le marqueur 'M'
                                for (cBcl = 0; cBcl < 72; cBcl++)
                                    cTblRecept[cBcl] = cTrameLecture[cBcl + cIndex];
                                break; //On r�cup�re la trame suivante, qui contient soit le marqueur 'S', soit le marqueur 'E' selon le type de carte
                            case 'S': //V�rification si acquisition des param�tres groupe et contr�le que la carte mesur�e est bien une adressable le cas �ch�ant 
                                //Si la carte est adressable, marqueur 'S'
                                VoieLogiqueAdressable((unsigned char *) cTblRecept, (unsigned char *) &cTrameLecture[cIndex]);
                                if (cEmplacementGroupe == REI_Abs(cTrameLecture[cIndex + 1] - 48, HexASCIIToInt((unsigned char *) &cTrameLecture[cIndex + 3], 2))) {
                                    cFlagGroupe = 1; //Flag de signalement que le groupe est pass�
                                    ConversionParamGroupeREICMS((unsigned char *) cTblRecept); //Conversion
                                }
                                break;
                            case 'E': //Si la carte est r�sistive, marqueur 'E'
                                LogiqueResistive((unsigned char *) cTblRecept, (unsigned char *) &cTrameLecture[cIndex]);
                                break;
                        }
                    }
                    RS232_EnvoiCarac(ACK); //Acquittement
                    InitRecept();
                    break;
            }
            break;
        case SM_CLOSING:
            cEmplacementGroupe = VoieCarteGroupeNonDeclare; //Nettoyage des donn�es
            LEDd = 0; //Extinction de la LED verte
            DTR = 1; //Fermeture de la communication avec REI
            EtatModem = SM_HOME; //R�initialisation des �tats
            EtatREI = Accroche;
            cCompteurErreur = 0;
            if (cDemarrage) {
                cDemandeMinitel = 1;
                cAttente = 5;
            } else {
                tAppelREI = TickGetDiv64K(); //Mise � jour du timer pour la prochaine connexion avec REI
            }
            break;
    }
}
