/* 
 * File:   StockageDonnees.c
 * Author: LFRAD
 *
 * Created on 26 novembre 2021, 09:36
 */

#include <p18F97J60.h>
#include <stdio.h>
#include <stdlib.h>
#include <main.h>
#include <i2c.h>

void StockageDonneesTrame(unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cConso, unsigned char* cModul, unsigned char* cTrame) { //On stock les donn�es de pression et d'�tat dans deux tableaux correspondants
    const char* cTrameInterne = cTrame;
    char cNumCapteur;

    cNumCapteur = atob(cTrameInterne + 4) - 1;

    cEtat[cNumCapteur] = atob(cTrameInterne + 7); //Stockage de l'etat

    iSeuil[cNumCapteur] = atoi(cTrameInterne + 10);

    iPression[cNumCapteur] = atoi(cTrameInterne + 15); //Stockage de la pression

    cConso[cNumCapteur] = atob(cTrameInterne + 20); //Stockage de la conso

    cModul[cNumCapteur] = atob(cTrameInterne + 24); //Stockage de la modul
}

void StockageDonneeVoie(unsigned char* cCarte, unsigned char* cVoie, unsigned int* iReposPaire, unsigned int* iReposImpaire, unsigned char* cEtatVoiePaire, unsigned char* cEtatVoieImpaire, unsigned char* cTrame) {
    const char* cTrameInterne = cTrame;

    *cCarte = atob(cTrameInterne + 1);
    *cVoie = atob(cTrameInterne + 3);

    *cEtatVoiePaire = atob(cTrameInterne + 6);
    *cEtatVoieImpaire = atob(cTrameInterne + 9);
    
    *iReposPaire = atoi(cTrameInterne + 12);
    *iReposImpaire = atoi(cTrameInterne + 16);
}

unsigned char ReecritureEtatVoie(unsigned char* cEtatCapteur, unsigned char cEtatVoie) {
    unsigned char cBcl;
    unsigned char cEtatSortie;
    char cSansCapteur;
    cEtatSortie = 00;
    cSansCapteur = 2;


    if(cEtatVoie == 90 || cEtatVoie == 80 || cEtatVoie == 70){
        cEtatSortie = cEtatVoie;
    }else{
        for (cBcl = 0; cBcl < 16; cBcl++) {
            if (cEtatCapteur[cBcl] != 99 && cEtatCapteur[cBcl] != 0) {
                cEtatSortie = 30;
            }
        }
    }
    for (cBcl = 0; cBcl < 16; cBcl++) {
        if (cEtatCapteur[cBcl] != 99) {
            cSansCapteur = 0;
        }
    }
    cEtatSortie += cSansCapteur;
    return cEtatSortie;
}

char CreationEtat(unsigned int iPression, unsigned int iSeuil) { //Code1 les �tats sont �crit en d�cimal pour simplifier la lecture, � r��crire en hexa
    char cEtat;

    if (iPression > 1700) { //Si le capteur � une pression au dessus de la gamme
        cEtat = 40; // Code1 Rajouter que si la carte est r�sistive, cEtat = 0x50 

    } else if (iPression >= iSeuil) { //Si le capteur est au dessus du seuil 
        cEtat = 00; //Le capteur est bon

    } else if (iPression > 900) { //Si le capteur est en dessous du seuil
        cEtat = 30; //La pression n'est pas bonne, d�clenchement de l'alarme

    } else if (iPression > 0) { //Si le capteur est en dessous de la gamme
        cEtat = 40; //.Le capteur est hors gamme

    } else { //Sinon
        cEtat = 50; //Le capteur est sans r�ponse
    }

    return cEtat;
}
