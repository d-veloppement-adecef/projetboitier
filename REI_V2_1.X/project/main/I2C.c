/* 
 * File:   I2C.c
 * Author: LFRAD
 *
 * Created on 1 d�cembre 2021, 09:30
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <p18F97J60.h>
#include <main.h>
#include <i2c.h>
#include <TCPIP_ETH97.h>
#include <TCPIP.h>

static unsigned char cTrameLectureEcriture[128];
extern unsigned char cReset;
extern unsigned char cAttente;

//Fonction pour mettre � jour l'emplacement du param. groupe dans les voies. On entre une voie abs, la voie est test�e, et si ok, les param�tres groupes sont mis � jour
unsigned char MaJUserEmplacementParam(unsigned char cVoieAbs) {
    unsigned int iValeur[10], iSeuilBas[10], iSeuilHaut[10];
    unsigned char cBcl;
    EcritureEmplacementParam(cVoieAbs); //Dans tous les cas, on note le choix de l'utilisateur en m�moire
    EESequRead(0x0, cVoieAbs * Page, cTrameLectureEcriture, 128); //Lecture dans l'Eeprom de la page contenant les donn�e de la voie s�lectionn�e par l'utilisateur
    for (cBcl = 0; cBcl < 10; cBcl++) { //On regarde sur toute la trame, jusqu'au 10ieme capteur et sauf le 7ieme, si un capteur est pr�sent. Si non, erreur de s�lection par l'utilisateur
        if (cBcl != 6) //Pas de capteur 7
        {
            if (cTrameLectureEcriture[cBcl * OctetsCapteur] == 99) //Capteur non pr�sent
                return 1;
        }
    }  
    for (cBcl = 0; cBcl < 10; cBcl++) {
        iValeur[cBcl] = CalculGroupe(cBcl + 1, Char2_Int(cTrameLectureEcriture[cBcl * OctetsCapteur + 3], cTrameLectureEcriture[cBcl * OctetsCapteur + 4])); //Conversion selon le capteur consid�r�, en valeur groupe
    }
    RemplissageTrame(0, 128, 0);
    LectureParametresGroupe(iSeuilBas, (void*) 0, (void*) 0, iSeuilHaut); //On lit les seuils pour les int�grer dans la fonction d'�criture suivante
    EcritureParametresGroupe(iSeuilBas, iValeur, iSeuilHaut); //Mise � jour des param�tres groupes
    return 0;
}

//Ecriture des donn�es d'un param�tre groupe dans l'Eeprom
void EcritureParametresGroupe(unsigned int* iSeuilBas, unsigned int* iValeur, unsigned int* iSeuilHaut) {
    unsigned char cBcl;
    unsigned char cTampon;
    //On lit la page du groupe pour sauvegarder toutes les informations du groupe
    EESequRead(0x0, PageParametreGroupe, cTrameLectureEcriture, 128); 

    for (cBcl = 0; cBcl < 10; cBcl++) { //On met � jour l'�tat des param�tres groupe dans cTrameLectureEcriture
        ModificationParametresGroupes(cBcl, iSeuilBas[cBcl], iSeuilHaut[cBcl], iValeur[cBcl], 0);
    }

    //R�cup�ration du num�ro de voie abs au niveau de 70.
    //Remplacement des donn�es dans la voie du groupe par des capteurs vides
    cTampon = cTrameLectureEcriture[70];

    RemplissageTrame(71, 128, 0xFF); //On Remplit la trame de 0xFF, car non n�cessaire 
    EEPaWrite(0x0, PageParametreGroupe, cTrameLectureEcriture); //On met � jour les param�tres groupe
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom

    EESequRead(0x0, cTampon * Page, cTrameLectureEcriture, 128); //Lecture de la voie contenant les capteurs du param. groupe
    cTrameLectureEcriture[PositionEtatVoie] = 01; //Ecriture de l'�tat de la voie pour l'identifier sur le web et Telgat comme � ne pas transmettre
    EEPaWrite(0, cTampon * Page, cTrameLectureEcriture); //Envoie dans l'Eeprom de la page contenant les donn�e de la voie � l'adresse correspondante
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    RemplissageTrame(0, 128, 0);
}

void LectureParametresGroupe(unsigned int* iSeuilBas, unsigned char* cEtat, unsigned int* iValeur, unsigned int* iSeuilHaut) { //R�cup�ration de toutes les donn�es des param�tres groupe
    unsigned char cBcl;
    unsigned char cIndex;
    EESequRead(0, PageParametreGroupe, cTrameLectureEcriture, 128); //On lit la page correspondant au groupe

    for (cBcl = 0; cBcl < 10; cBcl++) { //On convertit pour faire rentrer dans le tableau en entr�e
        cIndex = cBcl*OctetsCapteur;
        iSeuilBas[cBcl] = Char2_Int(cTrameLectureEcriture[cIndex], cTrameLectureEcriture[cIndex + 1]);
        cEtat[cBcl] = cTrameLectureEcriture[cIndex + 2]; //Ecriture de l'�tat
        iValeur[cBcl] = Char2_Int(cTrameLectureEcriture[cIndex + 3], cTrameLectureEcriture[cIndex + 4]);
        iSeuilHaut[cBcl] = Char2_Int(cTrameLectureEcriture[cIndex + 5], cTrameLectureEcriture[cIndex + 6]);
    }
    RemplissageTrame(0, 128, 0);
}
//Utilis� dans deux cas : On modifie un seuil par l'utilisateur ou tout le groupe.
//Dans le premier cas, on lit tout le groupe. Et on extrait la valeur du param�tre � modifier.
//On modifie le seuil, on met � jour l'�tat gr�ce au seuil/valeur et on r��crit le groupe
//Dans le deuxi�me cas, l'id�e est de simplement mettre � jour la trame contenant d�j� tout le groupe (plus haut dans le programme) 
//On fait la comparaison et on met � jour la trame contenant le groupe. Dans ce cas, on ne touche pas le groupe car le groupe est mis � jour plus tard dans le pgm.
void ModificationParametresGroupes(char cParametre, int iSeuilBas, int iSeuilHaut, int iValeur, unsigned char cLectureEcriture) {
    unsigned char cEtat;
    unsigned char cIndex = cParametre * OctetsCapteur;
    if (cLectureEcriture) { //Si �criture directement dans le groupe d'une modif de seuil
        EESequRead(0, PageParametreGroupe, cTrameLectureEcriture, 128); //On lit la page correspondant � la voie
        iValeur = Char2_Int(cTrameLectureEcriture[cIndex + 3], cTrameLectureEcriture[cIndex + 4]); // On r�cup�re la valeur associ�e � ce seuil
    }
    //Selon l'emplacement, l'�tat se calcule diff�rement
    if (((cParametre >= 0) && (cParametre <= 5)&&(cParametre != 2)) || (cParametre == 7)) {
        if ((iSeuilBas <= iValeur)&&(iValeur <= iSeuilHaut)) {
            cEtat = 0; //OK
        } else {
            cEtat = 30; //Alarme
        }
    }
    if ((cParametre == 2) || (cParametre == 8) || (cParametre == 9)) {
        if (iValeur >= iSeuilHaut) {
            cEtat = 0;
        } else {
            cEtat = 30;
        }
    }
    if (cParametre == 6) {
        cEtat = 0;
    }
    Int_2Char(iSeuilBas,&cTrameLectureEcriture[cIndex]);//Ecriture du seuil
    cTrameLectureEcriture[cIndex + 2] = cEtat; //Mise � jour de l'�tat
    Int_2Char(iValeur,&cTrameLectureEcriture[cIndex + 3]); //Ecriture de la valeur
    Int_2Char(iSeuilHaut,&cTrameLectureEcriture[cIndex + 5]);//Ecriture du seuil
    if (cLectureEcriture) {
        EEPaWrite(0x0, PageParametreGroupe, cTrameLectureEcriture); //Envoie dans l'Eeprom de la page contenant les donn�e de la voie � l'adresse correspondante
        AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
        RemplissageTrame(0, 128, 0);
    }
}
//Ecriture du nom du site ou du mot de passe
void EcritureNomSiteMDP(unsigned char* cTrameNom, unsigned int iEmplacement) {
    unsigned char cBcl;
    for (cBcl = 0; cBcl < 20; cBcl++) { //On prend les 20 premiers caract�res
        cTrameLectureEcriture[cBcl] = cTrameNom[cBcl];
    }
    RemplissageTrame(20, 128, 0xFF); //On compl�te avec des 0xFF
    EEPaWrite(0x0, iEmplacement, cTrameLectureEcriture); //On �crit � l'emplacement consid�r�
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    RemplissageTrame(0, 128, 0);
}
//On modifie l'emplacement de la voie du groupe. En parall�le, on met � jour l'�tat de la voie pr�c�demment utilis�e pour fair le groupe
unsigned char EcritureEmplacementParam(unsigned char cVoieAbs) {
    unsigned char cTampon;
    EESequRead(0, PageParametreGroupe +70, &cTampon, 1); //Lecture de l'emplacement de la voie
    if(cTampon != VoieCarteGroupeNonDeclare){ //Si voie pr�c�demment d�clar�e dans le groupe
        EESequRead(0, cTampon * Page, cTrameLectureEcriture, 128); //On lit la page correspondant � la voie
        MiseajourEtatVoie(); //On met � jour l'�tat
        EEPaWrite(0, (cTampon * Page), cTrameLectureEcriture); //On �crit la page correspondant � la voie
        AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
        RemplissageTrame(0, 128, 0);
    }
    EESequRead(0, PageParametreGroupe, cTrameLectureEcriture, 128); //On lit tout le groupe
    cTrameLectureEcriture[70] = cVoieAbs; //On change la voie utilis�e
    EEPaWrite(0x0, PageParametreGroupe, cTrameLectureEcriture); //Envoie dans l'Eeprom de la page contenant les donn�e de la voie � l'adresse correspondante
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    RemplissageTrame(0, 128, 0);
    return 1;
}
//Ecriture des donn�es d'une voie dans l'Eeprom
void EcritureDonneesVoie(unsigned char cVoieAbs, unsigned char* cEtatCapteur,
        unsigned int* iSeuil,
        unsigned int* iPression,
        unsigned char* cConso,
        unsigned char* cModul,
        unsigned char cEtatVoie,
        unsigned int iRepos) { 
    unsigned char cIndex;
    unsigned char cBcl;
    char cCapteurMax;

    cCapteurMax = 0;

    for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) { //Ecriture des donn�es des 16 capteurs dans la trame
        cIndex = cBcl*OctetsCapteur;
        cTrameLectureEcriture[cIndex] = cEtatCapteur[cBcl]; //Ecriture de l'�tat
        Int_2Char(iSeuil[cBcl],&cTrameLectureEcriture[cIndex + 1]); //Ecriture du seuil
        Int_2Char(iPression[cBcl],&cTrameLectureEcriture[cIndex + 3]); //Ecriture de la pression
        cTrameLectureEcriture[cIndex + 5] = cConso[cBcl]; //Ecriture du courant de conso
        cTrameLectureEcriture[cIndex + 6] = cModul[cBcl]; //Ecriture du courant de modulation
        if (iSeuil[cBcl] != 0) {
            cCapteurMax = cBcl + 1;
        }
    }

    cTrameLectureEcriture[PositionEtatVoie] = cEtatVoie; //Ecriture de l'�tat de la voie

    Int_2Char(iRepos,&cTrameLectureEcriture[PositionEtatVoie + 1]);//Ecriture du courant de repos de la voie (partie haute)

    cTrameLectureEcriture[PositionEtatVoie + 3] = cCapteurMax; //Emplacement capteur max

    RemplissageTrame(PositionEtatVoie + 4, 128, 0xFF);

    EEPaWrite(0x0, (cVoieAbs * Page), cTrameLectureEcriture); //Envoie dans l'Eeprom de la page contenant les donn�e de la voie � l'adresse correspondante
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    RemplissageTrame(0, 128, 0);
}
//Nettoyage de l'EEPROM
void InitEeprom(void) {
    unsigned char cBcl;
    unsigned char cIndex;

    //D�but de l'�criture

    for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) { //Ecriture des donn�es des 16 capteurs dans la trame
        cIndex = cBcl*OctetsCapteur;
        cTrameLectureEcriture[cIndex] = 99; //Ecriture de l'�tat
        cTrameLectureEcriture[cIndex + 1] = 0; //Ecriture du seuil (partie haute)
        cTrameLectureEcriture[cIndex + 2] = 0; //Ecriture du seuil (partie basse)
        cTrameLectureEcriture[cIndex + 3] = 0; //Ecriture de la pression (partie haute)
        cTrameLectureEcriture[cIndex + 4] = 0; //Ecriture de la pression (partie basse)
        cTrameLectureEcriture[cIndex + 5] = 0; //Ecriture du courant de conso
        cTrameLectureEcriture[cIndex + 6] = 0; //Ecriture du courant de modulation
    }
    cTrameLectureEcriture[PositionEtatVoie] = 02; //Ecriture de l'�tat de la voie
    cTrameLectureEcriture[PositionEtatVoie + 1] = 0; //Ecriture du courant de repos de la voie (partie haute)
    cTrameLectureEcriture[PositionEtatVoie + 2] = 0; //Ecriture du courant de repos de la voie (partie basse)
    cTrameLectureEcriture[PositionEtatVoie + 3] = 0;
    RemplissageTrame(PositionEtatVoie + 4, 128, 0xFF);

    for (cBcl = 0; cBcl < 100; cBcl++) {
        EEPaWrite(0, cBcl * Page, cTrameLectureEcriture); //Envoie dans l'Eeprom de la page contenant les donn�e de la voie � l'adresse correspondante
        AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    }
    RemplissageTrame(0, 128, 0);
}

void LectureDonneesTelgat(unsigned char cVoieAbs, unsigned char* cEtatCapteur,
        unsigned int* iSeuil,
        unsigned int* iPression) { //R�cup�ration de toutes les donn�es d'une voie pour les ranger dans des tableaux
    unsigned char cIndex;
    unsigned char cBcl;
    EESequRead(0, (cVoieAbs * Page), cTrameLectureEcriture, PositionEtatVoie + 1); //On lit la page correspondant � la voie
    for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) {
        cIndex = cBcl * OctetsCapteur;
        cEtatCapteur[cBcl] = cTrameLectureEcriture[cIndex];
        iSeuil[cBcl] = Char2_Int(cTrameLectureEcriture[cIndex + 1], cTrameLectureEcriture[cIndex + 2]);
        iPression[cBcl] = Char2_Int(cTrameLectureEcriture[cIndex + 3], cTrameLectureEcriture[cIndex + 4]);
    }
    RemplissageTrame(0, 128, 0);
}
//R�cup�ration du nombre de capteurs dans une voie
unsigned char RecuperationNombreCapteur(unsigned char cVoieAbs) {
    unsigned char cNombreCapteur;
    unsigned char cBcl;
    cNombreCapteur = 0;
    EESequRead(0, (cVoieAbs * Page), cTrameLectureEcriture, 128); //On lit la page correspondant � la voie
    for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) {
        if (cTrameLectureEcriture[cBcl * OctetsCapteur] != 99) {
            cNombreCapteur++;
        }
    }
    RemplissageTrame(0, 128, 0);
    return cNombreCapteur;
}
//Stockage des infos de connexion : cDelta = 0 : IP, cDelta = 1 : Masque, cDelta = 3 : Passerelle
void StockageIP0Masque1Pass2(unsigned char cE1, unsigned char cE2, unsigned char cE3, unsigned char cE4, unsigned char cDelta){
    unsigned char cNewDelta = cDelta*4;
    EESequRead(0, PageIP, cTrameLectureEcriture, 12);
    cTrameLectureEcriture[cNewDelta] = cE1;
    cTrameLectureEcriture[cNewDelta + 1] = cE2;
    cTrameLectureEcriture[cNewDelta + 2] = cE3;
    cTrameLectureEcriture[cNewDelta + 3] = cE4;
    RemplissageTrame(12, 128, 0xFF);

    EEPaWrite(0x0, PageIP, cTrameLectureEcriture);
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    RemplissageTrame(0, 128, 0);
}
//Lecture des infos de connexion : cDelta = 0 : IP, cDelta = 1 : Masque, cDelta = 3 : Passerelle
void LectureIP0Masque1Pass2(unsigned char* cE1, unsigned char* cE2, unsigned char* cE3, unsigned char* cE4, unsigned char cDelta) {
    EESequRead(0, PageIP + (cDelta * 4), cTrameLectureEcriture, 4);

    *cE1 = cTrameLectureEcriture[0];
    *cE2 = cTrameLectureEcriture[1];
    *cE3 = cTrameLectureEcriture[2];
    *cE4 = cTrameLectureEcriture[3];

    RemplissageTrame(0, 4, 0);
}
//R�initialisation compl�te du bo�tier
void ProtocoleReset(void) {
    unsigned char cTableNom[21] = "-ADECEF  TECHNOLOGY-";
    unsigned char i;
    DWORD tDelai;
    //Toutes les voies
    InitEeprom();

    //Nom du site
    EcritureNomSiteMDP(cTableNom, PageNomSite);

    //MDP
    strcpypgm2ram((char *) cTableNom, (ROM char *) "1234");
    for (i = 4; i < 21; i++) {
        cTableNom[i] = 0x00;
    }
    EcritureNomSiteMDP(cTableNom, PageMDP);

    //Param�tres groupe
    InitialisationParametresGroupe();

    //Emplacement carte param�tres groupe
    EcritureEmplacementParam(VoieCarteGroupeNonDeclare);

    //Bloc-notes
    InitialisationEEPROM();

    //IP
    StockageIP0Masque1Pass2((unsigned char) MY_DEFAULT_IP_ADDR_BYTE1, (unsigned char) MY_DEFAULT_IP_ADDR_BYTE2, (unsigned char) MY_DEFAULT_IP_ADDR_BYTE3, (unsigned char) MY_DEFAULT_IP_ADDR_BYTE4,0);
    StockageIP0Masque1Pass2((unsigned char) MY_DEFAULT_MASK_BYTE1, (unsigned char) MY_DEFAULT_MASK_BYTE2, (unsigned char) MY_DEFAULT_MASK_BYTE3, (unsigned char) MY_DEFAULT_MASK_BYTE4,1);
    StockageIP0Masque1Pass2((unsigned char) MY_DEFAULT_GATE_BYTE1, (unsigned char) MY_DEFAULT_GATE_BYTE2, (unsigned char) MY_DEFAULT_GATE_BYTE3, (unsigned char) MY_DEFAULT_GATE_BYTE4,2);
    tDelai = TickGet();
    while ((Raz == 0)&&(TickGet() - tDelai <= (DWORD) TICK_SECOND * 10ul)); //Condition sur le bouton RAZ. S�curit� si le bouton est d�fectueux
    cReset = 1;
    cAttente = 0;
}
//Init bloc-notes
void InitialisationEEPROM(void) {
    unsigned char cBcl;
    RemplissageTrame(0, 128, 0);
    for (cBcl = 0; cBcl < NbPageMaxBN; cBcl++) {
        EEPaWrite(0, (cBcl * Page + PageBlocNotes), cTrameLectureEcriture);
        AckPolling(0xA0);
    }
}
//Actualisation des donn�es EEPROM suite � r�cup�ration des infos via minitel. Fonctionne pour UN capteur
void MiseajourSeuil(unsigned char cVoieREI, unsigned char cCapteur, unsigned int iSeuil, signed int iPression) {
    unsigned char cIndex = cCapteur * OctetsCapteur;
    if (cVoieREI == 0) //Voir fonctionnement minitel, simplification de la lecture des voies. 01 -> 100 : Si lecture 00 : voie 100
        cVoieREI = 100;
    EESequRead(0, ((cVoieREI - 1) * Page), cTrameLectureEcriture, 128); //On lit la page correspondant � la voie
    if (iSeuil == 0) { //Capteur non pr�sent
        cTrameLectureEcriture[cIndex] = 99;
    } else {
        Int_2Char(iSeuil, &cTrameLectureEcriture[cIndex + 1]);
        if ((iPression == 0) || (iPression == 9999)) { //Etat en fonction de la pression et du seuil
            cTrameLectureEcriture[cIndex] = 50;
        } else if (iPression == -1) {
            cTrameLectureEcriture[cIndex] = 80;
            iPression = 0;
        } else if (iSeuil < iPression) {
            cTrameLectureEcriture[cIndex] = 0;
        } else if (cTrameLectureEcriture[cIndex] != 31) {
            cTrameLectureEcriture[cIndex] = 30;
        }
        Int_2Char(iPression, &cTrameLectureEcriture[cIndex + 3]);
    }
    MiseajourEtatVoie();
    EEPaWrite(0, ((cVoieREI - 1) * Page), cTrameLectureEcriture); //On �crit la page correspondant � la voie
    AckPolling(0xA0); //Ackpolling pour attendre la fin du transfert sur l'Eeprom
    RemplissageTrame(0, 128, 0);
}
//Suite � des modification sur un capteur dans la voie, on r�v�rifie si l'�tat g�n�ral de la voie est � jour.
void MiseajourEtatVoie(void){
    unsigned char cBcl;
    cTrameLectureEcriture[PositionEtatVoie] = 2; //Par d�faut, OK
    for (cBcl = 0; cBcl < NombreCapteursVoie; cBcl++) { //Pour chaque capteur
        switch (cTrameLectureEcriture[cBcl * OctetsCapteur]) { //On lit son �tat
            case 0: //OK
                if (cTrameLectureEcriture[PositionEtatVoie] == 2) { //Si la voie est consid�r�e vide, elle est pass�e en �tat OK
                    cTrameLectureEcriture[PositionEtatVoie] = 0;
                }
                break;
            case 30: //Pr�-alarme -> Alarme CMS
                cTrameLectureEcriture[PositionEtatVoie] = 30; //L'alarme doit �tre mont�e de mani�re inconditionnelle
                break;
            case 50: //Sans-r�ponse -> Alarme CMS
                cTrameLectureEcriture[PositionEtatVoie] = 30; //L'alarme doit �tre mont�e de mani�re inconditionnelle
                break;
            case 31: //Intervention
                if (cTrameLectureEcriture[PositionEtatVoie] != 30) { //L'�tat d'intervention doit �tre mont� uniquement si la voie n'est pas en alarme                                                cEtatVoie = 31;
                    cTrameLectureEcriture[PositionEtatVoie] = 31;
                }
                break;
            default:
                break;
        }
    }
}
void RemplissageTrame(unsigned char cDebut, unsigned char cFin, unsigned char cCarac) {
    unsigned char cBcl;
    for (cBcl = cDebut; cBcl < cFin; cBcl++) { //R�initialisation du tableau de stockage
        cTrameLectureEcriture[cBcl] = cCarac;
    }
}
//Mise � jour d'un commentaire. Fonctionne par 60 caract�res
void EcritureCommentaire(unsigned char cVoieAbs, unsigned char* cCommentaire) {
    char cPaire;
    unsigned char cBcl;
    EESequRead(0, ((cVoieAbs / 2) * Page + PageBlocNotes), cTrameLectureEcriture, 128);
    if (cVoieAbs % 2 == 1) {
        cPaire = 60;
    } else {
        cPaire = 0;
    }
    for (cBcl = cPaire; cBcl < 60 + cPaire; cBcl++) {
        cTrameLectureEcriture[cBcl] = cCommentaire[cBcl - cPaire];

    }
    EEPaWrite(0, ((cVoieAbs / 2) * Page + PageBlocNotes), cTrameLectureEcriture);
    AckPolling(0xA0);
    RemplissageTrame(0, 128, 0);
}
//R�cup�ration d'un commmentaire. Fonctionne par 60 caract�res
void RecupCommentaire(unsigned char cVoieAbs, unsigned char* cCommentaire) {
    unsigned char cPaire;
    unsigned char cBcl;
    EESequRead(0, ((cVoieAbs / 2) * Page + PageBlocNotes), cTrameLectureEcriture, 128);
    if (cVoieAbs % 2 == 1) {
        cPaire = 60;
    } else {
        cPaire = 0;
    }
    for (cBcl = 0; cBcl < 60; cBcl++) {
        cCommentaire[cBcl] = cTrameLectureEcriture[cBcl + cPaire];
    }
    RemplissageTrame(0, 128, 0);
}
//V�rification au d�marrage, si le bloc-notes est correctement initialis�
void RemplissageEEPROM(void) {
    unsigned char cBcl;
    unsigned char cBcl2;
    unsigned char cOffset;
    unsigned char cFlag;
    for (cBcl = 0; cBcl < NbPageMaxBN; cBcl++) {
        EESequRead(0, (cBcl * Page + PageBlocNotes), cTrameLectureEcriture, 128);
        cOffset = 0;
        cFlag = 0;
        do {
            if ((cTrameLectureEcriture[cOffset] == 0xFF)||(cTrameLectureEcriture[cOffset] == ' ')) {
                cFlag = 1;
                for (cBcl2 = 0; cBcl2 < 60; cBcl2++) {
                    cTrameLectureEcriture[cBcl2 + cOffset] = 0x00;
                }
            }
            cOffset += 60;
        } while (cOffset <= 60);
        if (cFlag == 1) {
            EEPaWrite(0, (cBcl * Page + PageBlocNotes), cTrameLectureEcriture);
            AckPolling(0xA0);
        }
    }
    RemplissageTrame(0, 128, 0);
}

unsigned char REI_Abs(unsigned char cCarteREI, unsigned char cVoieREI){
    return ((cCarteREI-1)*NombreVoiesCarte+cVoieREI-1);
}

unsigned int Char2_Int(unsigned char cHaut, unsigned char cBas){
    return ((unsigned int)cHaut*256+(unsigned int)cBas);
}

void Int_2Char(unsigned int iValeur, unsigned char* cResult){
    *cResult = iValeur/256;
    *(cResult+1)=iValeur%256;
    return;
}
