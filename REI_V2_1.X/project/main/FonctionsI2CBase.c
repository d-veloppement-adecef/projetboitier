/* 
 * File:   FonctionsI2CBase.c
 * Author: LFRAD
 *
 * Created on 7 f�vrier 2022, 09:53
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <p18F97J60.h>
#include <main.h>
#include <i2c.h>
//Tir� des projets d�mo de microchip et du compilateur. 
signed char EESequRead(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;


    if (address == 128) {
        cADDE = 0;
    }
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;
    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (Write(cADDE)) // write 1 byte 
        {
            StopI2C();
            return ( -3); // set error for write collision
        }

        if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
        {
            if (Write(cAdressH)) // write word address for EEPROM
            {
                StopI2C();
                return ( -3); // set error for write collision
            }

            while (SSPCON2bits.ACKSTAT);
            if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
            {
                if (Write(cAdressL)) // data byte for EEPROM
                {
                    StopI2C();
                    return ( -3); // set error for write collision
                }

                while (SSPCON2bits.ACKSTAT);
                if (!SSPCON2bits.ACKSTAT) // test for ACK condition received
                {
                    RestartI2C(); // generate I2C bus restart condition
                    if (Write(cADDE + 1))// WRITE 1 byte - R/W bit should be 1 for read
                    {
                        StopI2C();
                        return ( -3); // set error for write collision
                    }

                    while (SSPCON2bits.ACKSTAT);
                    if (!SSPCON2bits.ACKSTAT)// test for ACK condition received
                    {
                        if (gets(rdptr, length))// read in multiple bytes
                        {
                            return ( -1); // return with Bus Collision error
                        }

                        NotAckI2C(); // send not ACK condition 
                        StopI2C(); // send STOP condition
                        if (PIR2bits.BCLIF) // test for bus collision
                        {
                            PIR2bits.BCLIF = 0;
                            return ( -1); // return with Bus Collision error 
                        }
                    } else {
                        StopI2C();
                        return ( -2); // return with Not Ack error
                    }
                } else {
                    StopI2C();
                    return ( -2); // return with Not Ack error
                }
            } else {
                StopI2C();
                return ( -2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return ( -2); // return with Not Ack error
        }
    }
    return (0); // return with no error
}
//Modification du programme de MChip. Ecriture de 128 caract�res dans l'EEPROM.
signed char EEPaWrite(unsigned char cControl, unsigned int address, unsigned char *wrptr) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;

    if (address == 128) {
        cADDE = 0;
    }
    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (address) & 0xFF;
    cAdressH = (address) >> 8;

    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    } else {
        if (Write(cADDE)) // write 1 byte - R/W bit should be 0
        {
            StopI2C();
            return ( -3); // return with write collision error
        }
        IdleI2C();
        if (Write(cAdressH)) // write word address for EEPROM
        {
            StopI2C();
            return ( -3); // set error for write collision
        }
        IdleI2C();
        if (Write(cAdressL)) // data byte for EEPROM
        {
            StopI2C();
            return ( -3); // set error for write collision
        }
        IdleI2C();
        if (put(wrptr, 128)) {
            StopI2C();
            return ( -4); // bus device responded possible error
        }
    }

    //IdleI2C();                      // ensure module is idle
    StopI2C(); // send STOP condition
    if (PIR2bits.BCLIF) // test for Bus collision
    {
        PIR2bits.BCLIF = 0;
        return ( -1); // return with Bus Collision error 
    }
    return ( 0); // return with no error
}

//Utilisation du AckPolling pour savoir quand l'EEPROM a fini de travailler et est disponible
signed char AckPolling(unsigned char cControl) {
    IdleI2C1(); // ensure module is idle 
    StartI2C1(); // initiate START condition
    while (SSP1CON2bits.SEN); // wait until start condition is over 
    if (PIR2bits.BCL1IF) // test for bus collision
    {
        return ( -1); // return with Bus Collision error 
    } else {
        if (Write(cControl) == -1) // write byte - R/W bit should be 0
        {
            StopI2C1();
            return ( -3); // set error for write collision
        }

        while (SSP1CON2bits.ACKSTAT) // test for ACK condition received
        {
            RestartI2C1(); // initiate Restart condition
            while (SSP1CON2bits.RSEN); // wait until re-start condition is over 
            if (PIR2bits.BCL1IF) // test for bus collision
            {
                return ( -1); // return with Bus Collision error 
            }
            if (Write(cControl) == -1) // write byte - R/W bit should be 0
            {
                StopI2C1();
                return ( -3); // set error for write collision
            }
        }
    }

    StopI2C1(); // send STOP condition
    while (SSP1CON2bits.PEN); // wait until stop condition is over         
    if (PIR2bits.BCL1IF) // test for bus collision
    {
        return ( -1); // return with Bus Collision error 
    }
    return ( 0); // return with no error     
}

//Fonctions de base d'acquisition et d'�criture dans l'EEPROM
signed char gets(unsigned char *rdptr, unsigned char length) {
    while (length--) // perform getcI2C1() for 'length' number of bytes
    {
        *rdptr++ = getcI2C1(); // save byte received
        while (SSP1CON2bits.RCEN); // check that receive sequence is over    

        if (PIR2bits.BCL1IF) // test for bus collision
        {
            return ( -1); // return with Bus Collision error 
        }


        if (((SSP1CON1 & 0x0F) == 0x08) || ((SSP1CON1 & 0x0F) == 0x0B)) //master mode only
        {
            if (length) // test if 'length' bytes have been read
            {
                SSP1CON2bits.ACKDT = 0; // set acknowledge bit state for ACK        
                SSP1CON2bits.ACKEN = 1; // initiate bus acknowledge sequence
                while (SSP1CON2bits.ACKEN); // wait until ACK sequence is over 
            }
        }

    }
    return ( 0); // last byte received so don't send ACK      
}

signed char Write(unsigned char data_out) {
    SSP1BUF = data_out; // write single byte to SSP1BUF
    if (SSP1CON1bits.WCOL) // test if write collision occurred
        return ( -1); // if WCOL bit is set return negative #
    else {
        if (((SSP1CON1 & 0x0F) != 0x08) && ((SSP1CON1 & 0x0F) != 0x0B)) //slave mode only 
        {
            SSP1CON1bits.CKP = 1; // release clock line 
            while (!PIR1bits.SSP1IF); // wait until ninth clock pulse received
#if defined (I2C_V6_2)
            {
                if ((!SSP1STATbits.R_NOT_W) && (!SSP1STATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
                {
                    return ( -2); //Return NACK
                } else return (0); //Return ACK
            }
#else
            {
                if ((!SSP1STATbits.R_W) && (!SSP1STATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
                {
                    return ( -2); //Return NACK
                } else return (0); //Return ACK
            }
#endif
        } else if (((SSP1CON1 & 0x0F) == 0x08) || ((SSP1CON1 & 0x0F) == 0x0B)) //master mode only	
        {
            while (SSP1STATbits.BF); // wait until write cycle is complete      
            IdleI2C1(); // ensure module is idle
            if (SSP1CON2bits.ACKSTAT) // test for ACK condition received
                return ( -2); //Return NACK	
            else return ( 0); //Return ACK
        }
    }
}

signed char put(unsigned char *wrptr, unsigned char cLong) {
    unsigned char temp;
    while (cLong--) // transmit data until null character 
    {
        if (SSP1CON1bits.SSPM3) // if Master transmitter then execute the following
        {
            temp = Write(*wrptr);
            if (temp) return ( temp);
            //if ( putcI2C1( *wrptr ) )    // write 1 byte
            //{
            //  return ( -3 );             // return with write collision error
            //}
            //IdleI2C1();                  // test for idle condition
            //if ( SSP1CON2bits.ACKSTAT )  // test received ack bit state
            //{
            //  return ( -2 );             // bus device responded with  NOT ACK
            //}                            // terminate putsI2C1() function
        } else // else Slave transmitter
        {
            PIR1bits.SSP1IF = 0; // reset SSP1IF bit
            SSP1BUF = *wrptr; // load SSP1BUF with new data
            SSP1CON1bits.CKP = 1; // release clock line 
            while (!PIR1bits.SSP1IF); // wait until ninth clock pulse received

            if ((SSP1CON1bits.CKP) && (!SSP1STATbits.BF))// if R/W=0 and BF=0, NOT ACK was received
            {
                return ( -2); // terminate PutsI2C1() function
            }
        }

        wrptr++; // increment pointer

    } // continue data writes until null character

    return ( 0);
}

void Open(unsigned char sync_mode, unsigned char slew) {
    SSP1STAT &= 0x3F; // power on state 
    SSP1CON1 = 0x00; // power on state
    SSP1CON2 = 0x00; // power on state
    SSP1CON1 |= sync_mode; // select serial mode 
    SSP1STAT |= slew; // slew rate on/off 

    I2C1_SCL = 1; // Set SCL1 (PORTC,3) pin to input
    I2C1_SDA = 1; // Set SDA1 (PORTC,4) pin to input

    SSP1CON1 |= SSPENB; // enable synchronous serial port 
}