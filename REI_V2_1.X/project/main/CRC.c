/* 
 * File:   CRC.c
 * Author: LFRAD
 *
 * Created on 30 novembre 2021, 15:22
 */

#include <p18F97J60.h>
#include <stdio.h>
#include <stdlib.h>
#include <main.h>
#include <i2c.h>
#include <string.h>
//Calcul du CRC selon Telgat. Tir� du programme du 1er CMS
unsigned int CRC16(unsigned char *cTrame, unsigned int iLongueur) {
    unsigned char cBcl;
    unsigned int iBcl;
    unsigned int iCRC;

    iCRC = CRC_START;

    for(iBcl = 0; iBcl < iLongueur; iBcl++){
        iCRC ^= cTrame[iBcl];
        for(cBcl = 0; cBcl < 8; cBcl++){
            if (iCRC & 0x8000) {
                iCRC <<= 1;
                iCRC ^= CRC_POLY;
            } else {
                iCRC <<= 1;
            }
        }
    }
    return iCRC;
}
