/*********************************************************************
 *
 *	Generic TCP Server Example Application
 *  Module for Microchip TCP/IP Stack
 *   -Implements an example "ToUpper" TCP server on port 9760 and 
 *	  should be used as a basis for creating new TCP server 
 *    applications
 *
 *********************************************************************
 * FileName:        GenericTCPServer.c
 * Dependencies:    TCP
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    	Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     10/19/06	Original
 * Microchip            08/11/10    Added ability to close session by
 *                                  pressing the ESCAPE key.
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Description of how to run the demo:
 *   1) Connect the ethernet port of the programmed demo board to a 
 *        computer either directly or through a router.
 *   2) Determine the IP address of the demo board.  This can be done several
 *        different ways.
 *      a) If you are using a demo setup with an LCD display (e.g. Explorer 16
 *           or PICDEM.net 2), the IP address should be displayed on the second
 *           line of the display.
 *      b) Open the Microchip Ethernet Device Discoverer from the start menu.
 *           Press the "Discover Devices" button to see the addresses and host
 *           names of all devices with the Announce Protocol enabled on your
 *           network.  You may have to configure your computer's firewall to 
 *           prevent it from blocking UDP port 30303 for this solution.
 *      c) If your board is connected directly with your computer with a
 *           crossover cable: 
 *              1) Open a command/DOS prompt and type 'ipconfig'.  Find the 
 *                   network adaptor that is connected to the board.  The IP
 *                   address of the board is located in the 'Default Gateway'
 *                   field
 *              2) Open up the network status for the network adaptor that
 *                   connects the two devices.  This can be done by right clicking
 *                   on the network connection icon in the network settings folder 
 *                   and select 'status' from the menu. Find the 'Default Gateway'
 *                   field.
 *   3) Open a command/DOS prompt.  Type "telnet ip_address 9760" where
 *        ip_address is the IP address that you got from step 2.
 *   4) As you type characters, they will be echoed back in your command prompt
 *        window in UPPER CASE.
 *   5) Press Escape to end the demo.
 *
 ********************************************************************/
#define __GENERICTCPSERVER_C

#include <TCPIPConfig.h>
#include <main.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)

#include <TCPIP.h>


// Defines which port the server will listen on
#define SERVER_PORT	1111

/*****************************************************************************
  Function:
    void GenericTCPServer(void)

  Summary:
    Implements a simple ToUpper TCP Server.

  Description:
    This function implements a simple TCP server.  The function is invoked
    periodically by the stack to listen for incoming connections.  When a 
    connection is made, the server reads all incoming data, transforms it
    to uppercase, and echos it back.
	
    This example can be used as a model for many TCP server applications.

  Precondition:
    TCP is initialized.

  Parameters:
    None

  Returns:
    None
 ***************************************************************************/

extern unsigned char cTrameTest[404];
extern unsigned char cMalDeclare;

char GenericTCPServer(void) {
//    unsigned char cBcl;
    unsigned char* cPointeurTrame;
    char cEnvoi = 0;
//    char cTampon[128] = "";
    static unsigned int iLong;
    static TCP_SOCKET MySocket;
//    static unsigned char cIndex = 0;
    static char cConnexion = 0;
    static char cCompteur = 0;
//    char cCompteurMem = 0;
    static char cVoie = 0;
    static DWORD tTimeout = 0;
    char cIcog[10] = "I:COG,C:N";
//    char cImem[10] = "I:MEM,C:N";
    WORD wIndex = 0xFFFF, wAck = 0xFFFF, wNack = 0xFFFF;

    static enum _EtatTelgat {
        Attente = 0,
        NomSite,
        Mode,
        Cable,
    } EtatTelgat = Attente;

    static enum _TCPServerState {
        SM_HOME = 0,
        SM_LISTENING,
       // SM_EEPROM,
        SM_CLOSING,
    } TCPServerState = SM_HOME;

    cPointeurTrame = cTrameTest;

    switch (TCPServerState) {
        case SM_HOME:
            // Allocate a socket for this server to listen and accept connections on
            MySocket = TCPOpen(0, TCP_OPEN_SERVER, SERVER_PORT, TCP_PURPOSE_GENERIC_TCP_SERVER);
            if (MySocket == INVALID_SOCKET)
                return (0);
            TCPServerState = SM_LISTENING;
            break;

        case SM_LISTENING:
            // See if anyone is connected to us
            if ((!TCPIsConnected(MySocket))||(DSR)) { //Si on a perdu la connexion
                //Si on �tait connect� et que cela fait plus de 30 secondes 
                if ((cConnexion != 0)&&((DWORD) (TickGet() - tTimeout) >= (DWORD) (TICK_SECOND * 30ul))) {
                    TCPPut(MySocket, EOT); //On d�connecte
                    TCPServerState = SM_CLOSING;
                }
                return (0);
            }
            if (TCPWasReset(MySocket)) { //A la premi�re connexion, on retient que l'on est connect� et on commence un timer
                cConnexion = 1;
                tTimeout = TickGet();
            } else {
                if ((DWORD) (TickGet() - tTimeout) >= (DWORD) (TICK_SECOND * 30ul)) { //Si on ne renvoie rien depuis 30 secondes, on ferme la connexion
                    TCPPut(MySocket, EOT);
                    TCPServerState = SM_CLOSING; //On ferme la communication
                    return (0);
                }
            }
//            if ((TCPIsGetReady(MySocket))&&(cCompteur < 9)) {
//                do {
//                    wIndex = TCPFindEx(MySocket, cImem[cCompteurMem], 0, 0, FALSE);
//                    if (wIndex != 0xFFFF)
//                        cCompteurMem++;
//                } while ((wIndex != 0xFFFF)&&(cCompteurMem < 9));
//                if (cCompteurMem == 9) { //Si trame de d�marrage d�tect�e
//                    TCPServerState = SM_EEPROM;
//                    break;
//                }
//            }
            if (TCPIsGetReady(MySocket)) { //On re�oit des donn�es
//                for (cBcl = 0; cBcl < 128; cBcl++) {
//                    cTampon[cBcl] = 0x00;
//                }
//                EEPaWrite(0x00, PageIP + Page + cIndex*Page, cTampon);
//                AckPolling(0xA0);
//                TCPPeekArray(MySocket, cTampon, 128, 0);
//                IntToChar(cIndex, &cTampon[124], 4);
//                EEPaWrite(0x00, PageIP + Page + cIndex*Page, cTampon);
//                AckPolling(0xA0);
//                cIndex++;
                wAck = TCPFindEx(MySocket, ACK, 0, 0, FALSE); //On regarde si on trouve un ACK
                wNack = TCPFindEx(MySocket, 0x0F, 0, 0, FALSE); //On regarde si on trouve un 'NACK telgat' (bug de Telgat, NACK = 0x15 et non 0x0F)
                //NE PAS MODIFIER CE 'IF'. Telgat est souvent instable. Les connexions peuvent �chouer.
                if (((wAck == 0xFFFF)&&(wNack == 0xFFFF)&&(cCompteur >= 9)) ||
                        (((wAck != 0xFFFF) || (wNack != 0xFFFF))&&(cCompteur < 9)) ||
                        ((wAck != 0xFFFF) && (wNack != 0xFFFF))) { //On contr�le si on a bien re�u ACK, 'NACK telgat', ou 'I:COG', et au bon moment. Sinon on sort
                    EtatTelgat = Attente;
                    cPointeurTrame[0] = NACK;
                    cPointeurTrame[1] = NACK;
                    cPointeurTrame[2] = NACK; //Protocole CMS V1. 3 'NACK' et une fermeture de comm'.
                    iLong = 3;
                    TCPServerState = SM_CLOSING; //On ferme la communication
                    TCPPutArray(MySocket, cPointeurTrame, iLong); //On envoie la trame stock�e dans cPointeurTrame sur iLong caract�res
                } else {
                    if (cCompteur < 9) { //On a pas encore re�u 'I:COG'
                        do {
                            wIndex = TCPFindEx(MySocket, cIcog[cCompteur], 0, 0, FALSE);
                            if (wIndex != 0xFFFF)
                                cCompteur++;
                        } while ((wIndex != 0xFFFF)&&(cCompteur < 9)); //'On cherche un maximum de caract�res faisant 'I:COG'
                        if (cCompteur == 9) { //Si trame de d�marrage d�tect�e
                            TypeDeCom(cPointeurTrame, 'N', &iLong); //On cr�� la trame type de com
                            cEnvoi = 1; //On renvoie � Telgat la trame voulue, cr�ee dans 'TypeDeCom'
                            cVoie = 0; //On r�initialise les voies � envoyer � Telgat
                        }
                    } else { //On a d�j� re�u 'I:COG'
                        if (wNack != 0xFFFF) { //On revoie la trame si on voit un 'NACK Telgat'
                            cEnvoi = 1;
                        } else if (wAck != 0xFFFF) { //On passe � la trame suivante si on a re�u un 'ACK'
                            cEnvoi = 1;
                            if (EtatTelgat != Cable) //On change d'�tape sauf si on est dans la derni�re
                                EtatTelgat++;
                            switch (EtatTelgat) { //Cr�ation des trames � envoyer � telgat
                                case Attente: //Etat d'attente du d�but de la com avec telgat
                                    break;

                                case NomSite: //Trame d'envoie du nom du site
                                    NomDuSite(cPointeurTrame, &iLong); //On cr�� la trame avec le nom du site
                                    break;

                                case Mode: //Trame d'envoie du mode de com
                                    ModeDeCom(cPointeurTrame, &iLong); //On cr�� la trame avec le mode de com
                                    break;

                                case Cable: //Trames d'envoie des infos cables
                                    if (cVoie == 0) {
                                        if (cMalDeclare) {
                                            cVoie++;
                                        }
                                    }
                                    TrameCable(cPointeurTrame, &cVoie, &iLong); //On cr�� la trame avec les donn�es cables
                                    if (iLong == 1) { //Si la trame � envoyer ne fait qu'un caract�re : EOT (N'arrive que lorsque toutes les trames ont �t� envoyer et qu'il reste le EOT)
                                        TCPServerState = SM_CLOSING; //On ferme la communication
                                    }
                                    break;
                            }
                        }
                    }
                    if (cEnvoi != 0) { //Si on doit envoyer une trame
                        cEnvoi = 0;
                        TCPPutArray(MySocket, cPointeurTrame, iLong); //On envoie la trame stock�e dans cPointeurTrame sur iLong caract�res
                    }
                    TCPDiscard(MySocket); //On vide le tampon
                    tTimeout = TickGet(); //On actualise le timer de timeout
                }
//                for (cBcl = 0; cBcl < 128; cBcl++) { 
//                    cTampon[cBcl] = 0x00;
//                }
//                EEPaWrite(0x00, PageIP + Page + cIndex*Page, cTampon);
//                AckPolling(0xA0);
//                if (iLong >= 125) {
//                    for (cBcl = 0; cBcl < 124; cBcl++) {
//                        cTampon[cBcl] = cPointeurTrame[cBcl];
//                    }
//                    IntToChar(cIndex, &cTampon[124], 4);
//                } else if (cPointeurTrame[iLong - 1] == CarriageReturn) {
//                    for (cBcl = 0; cBcl < (iLong-1); cBcl++) {
//                        cTampon[cBcl] = cPointeurTrame[cBcl];
//                    }
//                    IntToChar(cIndex, &cTampon[iLong - 1], 4);
//                    for (cBcl = (iLong + 3); cBcl < 128; cBcl++) {
//                        cTampon[cBcl] = 0x00;
//                    }
//                } else {
//                    for (cBcl = 0; cBcl < iLong; cBcl++) {
//                        cTampon[cBcl] = cPointeurTrame[cBcl];
//                    }
//                    IntToChar(cIndex, &cTampon[iLong], 4);
//                    for (cBcl = (iLong + 4); cBcl < 128; cBcl++) {
//                        cTampon[cBcl] = 0x00;
//                    }
//                }
//                EEPaWrite(0x00, PageIP + Page + cIndex*Page, cTampon);
//                AckPolling(0xA0);
//                cIndex++;
            }
            // No need to perform any flush.  TCP data in TX FIFO will automatically transmit itself after it accumulates for a while.  If you want to decrease latency (at the expense of wasting network bandwidth on TCP overhead), perform and explicit flush via the TCPFlush() API.
            break;

//        case SM_EEPROM:
//            if (cIndex == 0) {
//                TCPServerState = SM_CLOSING;
//                return (0);
//            }
//            cIndex--;
//            EESequRead(0, PageIP + Page + cIndex*Page, cPointeurTrame, 128);
//            cPointeurTrame[128] = 0x0D;
//            cPointeurTrame[129] = 0x0A;
//            TCPPutArray(MySocket, cPointeurTrame, 130); //On envoie la trame stock�e dans cPointeurTrame sur iLong caract�res
//            TCPFlush(MySocket);
//            break;

        case SM_CLOSING: //Fermeture de la discussion avec Telgat
            // Close the socket connection.
            if (TCPGetTxFIFOFull(MySocket) >= 1) { //On vide le tampon d'envoi
                TCPFlush(MySocket);
            }
            TCPClose(MySocket); //On ferme et d�truit le socket de discussion avec Telgat
            cConnexion = 0; //On r�initialise les variables
            EtatTelgat = Attente;
            TCPServerState = SM_HOME;
            cCompteur = 0;
//            cCompteurMem = 0;
            return (1);
            break;
    }
}

#endif //#if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)
