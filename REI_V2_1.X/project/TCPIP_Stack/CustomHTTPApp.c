/*********************************************************************
 *
 *  Application to Demo HTTP2 Server
 *  Support for HTTP2 module in Microchip TCP/IP Stack
 *	 -Implements the application 
 *	 -Reference: RFC 1002
 *
 *********************************************************************
 * FileName:        CustomHTTPApp.c
 * Dependencies:    TCP/IP stack
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Elliott Wood     	6/18/07	Original
 ********************************************************************/
#define __CUSTOMHTTPAPP_C

#include <TCPIPConfig.h>


#include <math.h>
#include<stdio.h>
#include<string.h> 
#include <TCPIP.h>
#include <MainDemo.h>		// Needed for SaveAppConfig() prototype
#include <main.h>


static unsigned char cCarteAbsW;
static unsigned char cVoieAbsW;
static unsigned char cTPAbsW;
static unsigned char cParametreGroupe;

extern unsigned char cAlim;
extern unsigned char cType;
extern unsigned char cAlarme;
extern unsigned char cAttente;
extern unsigned char cMalDeclare;
extern unsigned char cDemandeMinitel;
extern unsigned char cIndexMax;
extern unsigned char cIndexTableau;
extern unsigned int iTableau_version[7];
extern unsigned int cTblSeuilResi[20];
extern unsigned char cReset;
extern unsigned char cCompteurErreur;
extern DWORD tAppelREI;
/****************************************************************************
  Section:
    Authorization Handlers
 ***************************************************************************/

/*****************************************************************************
  Function:
    BYTE HTTPNeedsAuth(BYTE* cFile)
	
  Internal:
    See documentation in the TCP/IP Stack API or HTTP2.h for details.
 ***************************************************************************/
#if defined(HTTP_USE_AUTHENTICATION)

BYTE HTTPNeedsAuth(BYTE* cFile) {
    // If the filename begins with the folder "protect", then require auth
    if (memcmppgm2ram(cFile, (ROM void*) "protect", 7) == 0)
        return 0x00; // Authentication will be needed later

    // If the filename begins with the folder "snmp", then require auth
    if (memcmppgm2ram(cFile, (ROM void*) "snmp", 4) == 0)
        return 0x00; // Authentication will be needed later

#if defined(HTTP_MPFS_UPLOAD_REQUIRES_AUTH)
    if (memcmppgm2ram(cFile, (ROM void*) "mpfsupload", 10) == 0)
        return 0x00;
#endif

    // You can match additional strings here to password protect other files.
    // You could switch this and exclude files from authentication.
    // You could also always return 0x00 to require auth for all files.
    // You can return different values (0x00 to 0x79) to track "realms" for below.

    return 0x80; // No authentication required
}
#endif

/*****************************************************************************
  Function:
    BYTE HTTPCheckAuth(BYTE* cUser, BYTE* cPass)
	
  Internal:
    See documentation in the TCP/IP Stack API or HTTP2.h for details.
 ***************************************************************************/
#if defined(HTTP_USE_AUTHENTICATION)

BYTE HTTPCheckAuth(BYTE* cUser, BYTE* cPass) {
    unsigned char cTableNom[10];
    EESequRead(0, PageMDP, cTableNom, 10); //On lit la page correspondant au mot de passe
    if (cTableNom[0] == 0xFF) { //Si rien n'est �crit (page non initialis�e)
        strcpypgm2ram((char *) cTableNom, (ROM char *) "1234"); //MDP par d�faut 1234
    }
    if (strcmppgm2ram((char *) cUser, (ROM char *) "admin") == 0 //V�rification que l'utilisateur est admin
            && strcmp((const char *) cPass, (const char *) cTableNom) == 0) //V�rification que le mot de passe est celui enregistr� ou celui par d�faut
        return 0x80; // We accept this combination
    if (strcmppgm2ram((char *) cUser, (ROM char *) "admin") == 0 //V�rification que l'utilisateur est admin
            && strcmppgm2ram((char *) cPass, (ROM char *) "1998") == 0) //V�rification que le mdp est celui d'usine
        return 0x80; // We accept this combination
    // You can add additional user/pass combos here.
    // If you return specific "realm" values above, you can base this 
    //   decision on what specific file or folder is being accessed.
    // You could return different values (0x80 to 0xff) to indicate 
    //   various users or groups, and base future processing decisions
    //   in HTTPExecuteGet/Post or HTTPPrint callbacks on this value.

    return 0x00; // Provided user/pass is invalid
}
#endif

/*****************************************************************************
  Function:
    HTTP_IO_RESULT HTTPExecuteGet(void)
	
  Internal:
    See documentation in the TCP/IP Stack API or HTTP2.h for details.
 ***************************************************************************/
HTTP_IO_RESULT HTTPExecuteGet(void) {
    BYTE *ptr;
    BYTE filename[20];
    static unsigned char cValid = 0;
    unsigned char cNomSite[20];
    signed char cBcl;
    
    unsigned int iSeuilCree;

    unsigned char cParametre;
    unsigned int iSeuilBas;
    unsigned int iSeuilHaut;

    unsigned char cE1 = 0, cE2 = 0, cE3 = 0, cE4 = 0;
    unsigned long lSuppression;

    unsigned char cCommentaire[60];

    // Load the file name
    // Make sure BYTE filename[] above is large enough for your longest name
    MPFSGetFilename(curHTTP.file, filename, 20);

    //Collecte d'une carte, d'une voie, d'un capteur
    ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "C");
    if (ptr) {
        cCarteAbsW = atob(ptr) - 1;
    }
    
    ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "V");
    if (ptr) {
        cVoieAbsW = atob(ptr) - 1;
        cCarteAbsW = cVoieAbsW/20;
        cTPAbsW = 0;
    }
    ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "T");
    if (ptr) {
        cTPAbsW = atob(ptr) - 1;
    }
    
    if (!memcmppgm2ram(filename, "protect/Vide.xml", 16)) {
        //Modification du nom de site
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "NomSite");
        if (ptr) {
            for (cBcl = 0; cBcl < strlen(ptr); cBcl++) {
                cNomSite[cBcl] = ptr[cBcl];
            }
            for (cBcl = strlen(ptr); cBcl < 20; cBcl++) {
                cNomSite[cBcl] = 0x00;
            }
            EcritureNomSiteMDP(cNomSite, PageNomSite);
        }
        
        //Modification des seuils du param�tre groupe
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "Para");
        if (ptr) {
            cParametre = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "SB");
        if (ptr) {
            iSeuilBas = atoi(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "SH");
        if (ptr) {
            iSeuilHaut = atoi(ptr);
            ModificationParametresGroupes(cParametre, iSeuilBas, iSeuilHaut,0,1);
        }
        
        /*Modification de l'adresse IP, du masque, et de la passerelle
        Remarque : Evitant d'utiliser le POST un maximum et du fait des limitations du GET, les infos sont divis�es en 3 trames ind�pendantes
        Ne pouvant ma�triser l'ordre d'arriv�e des trames, il faut forc�ment attendre que les 3 soient arriv�es avant de reset la carte. D'o� le compteur*/
        //IP
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "IP1");
        if (ptr) {
            cE1 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "IP2");
        if (ptr) {
            cE2 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "IP3");
        if (ptr) {
            cE3 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "IP4");
        if (ptr) {
            cE4 = atob(ptr);
            StockageIP0Masque1Pass2(cE1, cE2, cE3, cE4,0);
            cValid++;
        }

        //Masque
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "M1");
        if (ptr) {
            cE1 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "M2");
        if (ptr) {
            cE2 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "M3");
        if (ptr) {
            cE3 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "M4");
        if (ptr) {
            cE4 = atob(ptr);
            StockageIP0Masque1Pass2(cE1, cE2, cE3, cE4,1);
            cValid++;
        }

        //Passerelle
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "P1");
        if (ptr) {
            cE1 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "P2");
        if (ptr) {
            cE2 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "P3");
        if (ptr) {
            cE3 = atob(ptr);
        }

        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "P4");
        if (ptr) {
            cE4 = atob(ptr);
            StockageIP0Masque1Pass2(cE1, cE2, cE3, cE4,2);
            cValid++;
        }
        
        //Si 3 modifs, reset
        if (cValid == 3) {
            cValid = 0;
            cReset = 1;
            cAttente = 1;
        }
        
        //Modification de l'emplacement du param�tre groupe, avec d�tection si le groupe existe
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "Empl");
        if (ptr) {
            cParametre = atob(ptr) - 1;
            if (cParametre < 100) { //Voie valide, en absolue (0 -> 99)
                cMalDeclare = MaJUserEmplacementParam(cParametre);
            } else { //Suppression du groupe
                cMalDeclare = EcritureEmplacementParam(VoieCarteGroupeNonDeclare);
            }
        }
        
        
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "Refresh");
        if (ptr) {
            cParametre = atob(ptr);
            if(cParametre == 1){ //Demande d'actualisation des donn�es pour Telgat
                tAppelREI = TickGetDiv64K() - (DWORD) (TICK_HOUR >> 16ul);
            }
            if(cParametre == 2){ //Demande de red�marrage de bo�tier
                cReset = 1;
            }
            cAttente = 1;
        }
        
        //Demande de modification du seuil d'un capteur
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "S");
        if (ptr) {
            iSeuilCree = atoi(ptr);
            CreaLibe1Modif2Capteur(2, cVoieAbsW, cTPAbsW, iSeuilCree);
        }
        
        //Demande de suppression de capteurs r�sistifs ou adressables
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "Suppression");
        if (ptr) {
            lSuppression = atol(ptr); //Fonctionne sur un principe de d�calage de bit : Si par ex, le mot envoy� par le site est 00001b, alors on supprime la premi�re TP. 0010b, la deuxi�me, 0011b les deux premi�res, etc. 
            for (cBcl = NombreVoiesCarte - 1; cBcl >= 0; cBcl--) {
                if (lSuppression >= pow(2, cBcl)) {
                    lSuppression -= pow(2, cBcl);
                    if (cType & 1 << cCarteAbsW) { //R�sisitif ou adressable, on supprime un capteur d'une voie ou la voie de la carte
                        CreaLibe1Modif2Capteur(1,cVoieAbsW, cBcl,0);
                    } else {
                        CreaLibe1Modif2Capteur(1,(cCarteAbsW*20) + cBcl, 0,0);
                    }
                }
            }
        }
        
        //Modification du commentaire de la voie
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "Comm");
        if (ptr) {
            for (cBcl = 0; cBcl < strlen(ptr); cBcl++) { //On regarde la longueur du commentaire et on le copie
                cCommentaire[cBcl] = ptr[cBcl];
            }
            for (cBcl = strlen(ptr); cBcl < 60; cBcl++) { //On compl�te avec des NUL
                cCommentaire[cBcl] = 0x00;
            }
            EcritureCommentaire(cVoieAbsW, cCommentaire); //On �crit en m�moire le commentaire
        }
        
        //Modification du mot de passe
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "MDP");
        if (ptr) {
            for (cBcl = 0; cBcl < strlen(ptr); cBcl++) { //On regarde la longueur du MDP et on le copie
                cNomSite[cBcl] = ptr[cBcl];
            }
            for (cBcl = strlen(ptr); cBcl < 20; cBcl++) { //On compl�te avec des NUL
                cNomSite[cBcl] = 0x00;
            }
            EcritureNomSiteMDP(cNomSite, PageMDP); //On �crit en m�moire le MDP
        }
    } else if ((!memcmppgm2ram(filename, "Groupe.xml", 10))||(!memcmppgm2ram(filename, "Info.xml", 8))) { //Quand on demande un .xml avec un num�ro, on change un param�tre dans le bo�tier
        ptr = HTTPGetROMArg(curHTTP.data, (ROM BYTE *) "Num");
        if (ptr) {
            cParametreGroupe = atob(ptr);
        }
    }
    return HTTP_IO_DONE;
}

/****************************************************************************
  Section:
    Dynamic Variable Callback Functions
 ***************************************************************************/


void HTTPPrint_NumeroVoie(void) { //Si on demande une voie particuli�re
    unsigned char cTrame[3];
    sprintf(cTrame, "%i", cVoieAbsW + 1);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_Numcapteur(void) { //Si on demande un capteur
    unsigned char cTrame[3];
    sprintf(cTrame, "%i", cTPAbsW + 1);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_EtatCapteur(void) { //On regarde l'�tat du capteur
    unsigned char cTrame[3];
    EESequRead(0, ((cVoieAbsW * Page) + cTPAbsW*OctetsCapteur), cTrame, 1); //On lit la page correspondant � la voie
    sprintf(cTrame, "%i", cTrame[0]);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_Seuil(void) { //On cherche � envoyer un seuil pour un capteur r�sistif ou adressable
    unsigned char cTrame[3];
    int iSeuil;
    unsigned char i;
    EESequRead(0, ((cVoieAbsW * Page) + cTPAbsW*OctetsCapteur) + 1, cTrame, 2); //On lit la page correspondant � la voie

    iSeuil = Char2_Int(cTrame[0],cTrame[1]);
    cTrame[0] = 0;
    cTrame[1] = 0;
    if (cType & 1 << cCarteAbsW) { //Adressable, on envoie 'iSeuil' pr�sent dans la formule : Seuil = 800+10*iSeuil. Le site fait la conversion
        iSeuil -= 800;
        iSeuil /= 10;
        if (iSeuil < 0)
            iSeuil = 0;
    } else { //R�sistif : On envoie 'iSeuil' o� 'iSeuil' est le xi�me seuil du tableau des seuils r�sistifs (voir 'cTblSeuilResi'). Le site fait la conversion
        for (i = 0; i < 20; i++) {
            if (cTblSeuilResi[i] == iSeuil) {
                iSeuil = i;
                break;
            }
        }
    }
    sprintf(cTrame, "%i", iSeuil);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_Pression(void) { //On lit la pression en m�moire
    unsigned char cTrame[3];
    EESequRead(0, ((cVoieAbsW * Page) + cTPAbsW*OctetsCapteur) + 3, cTrame, 2); //On lit la page correspondant � la voie
    sprintf(cTrame, "%i", Char2_Int(cTrame[0],cTrame[1]));
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_NumParametreGroupe(void) { //Selon le param�tre groupe demand�, on le r�introduit pour la r�ponse au site
    unsigned char cTrame[3];
    sprintf(cTrame, "%i", cParametreGroupe);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_SeuilBas(void) {//Seuil bas du groupe
    unsigned char cTrame[3];  
    EESequRead(0, PageParametreGroupe + (cParametreGroupe*OctetsCapteur), cTrame, 2);
    sprintf(cTrame, "%i", Char2_Int(cTrame[0],cTrame[1]));
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_EtatParaGroupe(void) {//Etat du groupe
    unsigned char cTrame[3];
    EESequRead(0, PageParametreGroupe + 2 + (cParametreGroupe*OctetsCapteur), cTrame, 1); //On lit l'�tat du param�tre
    sprintf(cTrame, "%i", cTrame[0]);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_ValeurParaGroupe(void) {//Valeur du groupe, sauf le 6i�me absolu, qui sert � indiquer si le groupe est bien d�clar�
    unsigned char cTrame[4];
    unsigned char cBcl;
    switch (cParametreGroupe) {
        case 6://Si demande du 6i�me groupe, envoi de la validit� de la d�claration du groupe
            EESequRead(0, PageParametreGroupe + 70, cTrame, 1);
            if (cTrame[0] != VoieCarteGroupeNonDeclare) {
                cBcl = cTrame[0]+1;
            } else {
                cBcl = 0;
            }
            if (cMalDeclare) {
                sprintf(cTrame, "%i*", cBcl);//Groupe invalide
            } else {
                sprintf(cTrame, "%i", cBcl);//Groupe valide
            }
            break;
        default://Valeur du xi�me groupe
            EESequRead(0, PageParametreGroupe + 3 + (cParametreGroupe*OctetsCapteur), cTrame, 2);
            sprintf(cTrame, "%i", Char2_Int(cTrame[0],cTrame[1]));
            break;
    }
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_SeuilHaut(void) {//Seuil haut du groupe
    unsigned char cTrame[3];
    EESequRead(0, PageParametreGroupe + 5 + (cParametreGroupe*OctetsCapteur), cTrame, 2);
    sprintf(cTrame, "%i", Char2_Int(cTrame[0],cTrame[1]));
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_InfosPage(void) { //Selon la demande,
    DWORD dTampon;
    unsigned char cData[4];
    unsigned char cBcl;
    unsigned char cRepereIP = 0;
    unsigned char cTrame[21];

    switch (cParametreGroupe) {
        case 0://Nom du site
            EESequRead(0, PageNomSite, cTrame, 20);
            cTrame[20] = 0;
            break;
        case 2: //IP
            LectureIP0Masque1Pass2(&cData[0], &cData[1], &cData[2], &cData[3],0);
            break;
        case 3: //Masque
            LectureIP0Masque1Pass2(&cData[0], &cData[1], &cData[2], &cData[3],1);
            break;
        case 4: //Passerelle
            LectureIP0Masque1Pass2(&cData[0], &cData[1], &cData[2], &cData[3],2);
            break;
        case 5://Etat des alarmes
            sprintf(cTrame, "%i", cAlarme);
            break;
        case 6://Prochain appel dans x minutes
            dTampon = (TickGetDiv64K() - tAppelREI)/(TICK_MINUTE >> 16ul);
            if(dTampon > 60)
                dTampon = 60;
            dTampon = 60 - dTampon;
            sprintf(cTrame, "%lu", dTampon);
            break;
        case 7://Dernier appel il y a x heures
            dTampon = (TickGetDiv64K() - tAppelREI)/(TICK_HOUR >> 16ul);
            sprintf(cTrame, "%lu", dTampon);
            break;
        default://NUL
            cTrame[0] = '0';
            cTrame[1] = 0;
            break;
    }

    if (cParametreGroupe > 1 && cParametreGroupe < 5) { //Si IP ou Masque ou Passerelle, formatage en x.x.x.x
        for (cBcl = 0; cBcl < 3; cBcl++) {
            sprintf(cTrame + cRepereIP, "%i", cData[cBcl]);
            cRepereIP = strlen(cTrame);
            cTrame[cRepereIP] = '.';
            cRepereIP++;
        }
        sprintf(cTrame + cRepereIP, "%i", cData[3]);
    }
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_EtatVoie(void) { //Etat de la voie
    unsigned char cTrame[3];
    EESequRead(0, (cVoieAbsW * Page) + PositionEtatVoie, cTrame, 1);
    sprintf(cTrame, "%i", cTrame[0]);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_CapteursPresents(void) { //Nb de capteurs pr�sents
    unsigned char cTrame[3];
    sprintf(cTrame, "%i", RecuperationNombreCapteur(cVoieAbsW));
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_EtatCarte(void) { //Etat des cartes (Pr�sente, non pr�sente)
    unsigned char cTrame[3];
    sprintf(cTrame, "%i", cAlim);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_Valeur(void) { //Progression dans le minitel, 0 � 19, voie en cours de traitement, 20, fini 21 � 40, voie en cours de traitement mais avec une erreur
    unsigned char cTrame[3];

    if(cIndexMax == 0){ //Si on a fini
        sprintf(cTrame, "%i", NombreVoiesCarte);
    }else{//Sinon
        if (cIndexTableau == 0) {//Si on commence
            cDemandeMinitel = 1;
        }
        if(cCompteurErreur != 0) //Si il y a une erreur
            sprintf(cTrame, "%i", cIndexTableau + NombreVoiesCarte + 1);
        else
            sprintf(cTrame, "%i", cIndexTableau); //Sinon, en cours
    }
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_TableauVer(void) { //Version des cartes et du bo�tier ou de l'�tat de la liaison REI
    unsigned char cTrame[3];
    if (cParametreGroupe < 7)
        sprintf(cTrame, (const far rom char*) "%i", iTableau_version[cParametreGroupe]);
    else
        sprintf(cTrame, "%i", DSR);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_TypeCarte(void) { //Type de carte (Adressable, R�sistive)
    unsigned char cTrame[3];
    sprintf(cTrame, "%i", cType);
    TCPPutString(sktHTTP, cTrame);
}

void HTTPPrint_Commentaire(void) { //Commentaire � envoyer
    unsigned char cTrame[61];
    RecupCommentaire(cVoieAbsW, cTrame); //Gros doute
    cTrame[60] = 0;
    TCPPutString(sktHTTP, cTrame);
}
