#include <stdio.h>
#include <stdlib.h>
#include <p18F97J60.h>
#include <main.h>
#include <i2c.h>
#include <string.h>
#include <math.h>

unsigned char cEtatCapteur[NombreCapteursVoie];
unsigned int iSeuil[NombreCapteursVoie];
unsigned int iPression[NombreCapteursVoie];
unsigned char cConso[NombreCapteursVoie];
unsigned char cModul[NombreCapteursVoie];
extern unsigned char cMalDeclare;

void ConversionParamGroupeREICMS(unsigned char *cTrameLectEEPROM) { //Fonction de conversion de la trame REI en infos des param�tres groupe
    unsigned int iSeuilBas[10];
    unsigned char cEtat[10];
    unsigned int iValeur[10];
    unsigned int iSeuilHaut[10];
    unsigned char cValeurASCIIGroupe[3];
    unsigned int iValeurBrut;
    char cBcl, t;

    cMalDeclare = 0; //Variable globale permettant d'indiquer une erreur de d�claration des param�tres groupe

    LectureParametresGroupe(iSeuilBas, cEtat, iValeur, iSeuilHaut); //On lit les param�tres groupe contenus en m�moire pour r�cuperer les seuils actuels

    for (cBcl = 0; cBcl < 10; cBcl++) { //On regarde sur toute la trame, jusqu'au capteur 10, dernier capteur du groupe
        if (cBcl != 6) //Pas de capteur 7
        {
            if (cTrameLectEEPROM[6 + cBcl * 4] == '8') { //Signifie que le capteur n'est pas pr�sent.
                cMalDeclare = 1; //Indication qu'il y a un probl�me dans la d�claration du groupe
            }
        }
    }
    if (cMalDeclare == 0) { //Si tout se passe bien
        for (cBcl = 0; cBcl < 10; cBcl++) { //Pour chaque capteur pr�sent
            for (t = 0; t < 3; t++) { //Initialisation valeur � convertir
                cValeurASCIIGroupe[t] = 0;
            }
            if (cBcl != 6) //Pas de capteur 7
            {
                cValeurASCIIGroupe[0] = cTrameLectEEPROM[6 + cBcl * 4]; //R�cup�ration des valeurs
                cValeurASCIIGroupe[1] = cTrameLectEEPROM[7 + cBcl * 4];
                cValeurASCIIGroupe[2] = cTrameLectEEPROM[8 + cBcl * 4];
                iValeurBrut = HexASCIIToInt(cValeurASCIIGroupe, 3); //Conversion en une valeur 'int'
                iValeur[cBcl] = CalculGroupe(cBcl + 1, iValeurBrut); //Conversion selon le capteur consid�r�, en valeur groupe
            }
            if (cBcl == 6) { //Si capteur 7, on met 0
                iValeur[cBcl] = 0;
            }
        }
    } else {
        for (cBcl = 0; cBcl < 10; cBcl++) { //Si rien ne va, on met � z�ro
            iValeur[cBcl] = 0;
        }
    }

    EcritureParametresGroupe(iSeuilBas, iValeur, iSeuilHaut); //On actualise tous les r�sultats
}

void InitialisationParametresGroupe(void) { //Initialisation des param�tres groupe aux valeurs usine
    unsigned int iSeuilBas[10] = {TamponBas,SortieBas,SecoursTresBas,RoseeBas,DebitBas,CycleBas,0,MaintenanceBas,V220Bas,V48Bas};
    unsigned int iSeuilHaut[10] = {TamponHaut,SortieHaut,SecoursBas,RoseeHaut,DebitHaut,CycleHaut,0,MaintenanceHaut,V220Haut,V48Haut};
    unsigned int iValeur[10] = {0,0,0,0,0,0,0,0,0,0};
    EcritureParametresGroupe(iSeuilBas, iValeur, iSeuilHaut);
}

int CalculGroupe(char selection, int valeur) { //Voir feuille conversion IGP pour le d�tail
    int valcal;

    switch (selection) //analyse de la fonction
    {
        case 1: // Tampon
            if (valeur >= 1600) {
                valcal = 7000;
            } else if (valeur <= 900) {
                valcal = 0;
            } else {
                valcal = 10 * (valeur - 900);
            }
            break;

        case 2: // Sortie
            if (valeur >= 1600) {
                valcal = 700;
            } else if (valeur <= 900) {
                valcal = 0;
            } else {
                valcal = valeur - 900;
            }
            break;

        case 3: // Secours
            if (valeur >= 1600) {
                valcal = 7000;
            } else if (valeur <= 900) {
                valcal = 0;
            } else {
                valcal = 10 * (valeur - 900);
            }
            break;

        case 4: // PR2
            if (valeur >= 1600) {
                valcal = 257;
            } else if (valeur <= 900) {
                valcal = 235;
            } else {
                valcal = 235 + 0.0314 * (valeur - 900);
            }
            break;

        case 5: // D�bit
            if (valeur <= 900) {
                valcal = 0;
            } else if (valeur <= 1100) {
                valcal = 2 * (valeur - 900);
            } else if (valeur <= 1300) {
                valcal = 401 + 4 * (valeur - 1101);
            } else if (valeur <= 1600) {
                valcal = 1201 + 10 * (valeur - 1301);
            } else {
                valcal = 4200;
            }
            break;

        case 6: // Temps cycle moteur
            if (valeur <= 900) {
                valcal = 0;
            } else if (valeur <= 1100) {
                valcal = valeur - 900;
            } else if (valeur <= 1300) {
                valcal = 201 + 8 * (valeur - 1101);
            } else if (valeur <= 1600) {
                valcal = 1801 + 18 * (valeur - 1301);
            } else {
                valcal = 7200;
            }
            break;

        case 8: // Temps fonctionnement moteur
            if (valeur <= 900) {
                valcal = 0;
            } else if (valeur <= 1100) {
                valcal = valeur - 900;
            } else if (valeur <= 1300) {
                valcal = 201 + 8 * (valeur - 1101);
            } else if (valeur <= 1600) {
                valcal = 1801 + 18 * (valeur - 1301);
            } else {
                valcal = 7200;
            }
            break;

        case 9: // 220V
            if (valeur <= 1200) {
                valcal = 0;
            } else {
                valcal = 1;
            }
            break;

        case 10: // 48V
            if (valeur <= 1200) {
                valcal = 0;
            } else {
                valcal = 1;
            }
            break;

        default:
            break;
    }
    return (valcal);
}

void VoieLogiqueAdressable(unsigned char *cTrameLectEEPROM1, unsigned char *cTrameLectEEPROM2) {
    unsigned char cValeurASCII[5], cEtatVoie, cBcl1, cNumVoie, cNumCarte; //Variables de boucles.
    unsigned int iValeurBrut, iRepos; //Il faut �tre capable de savoir dans quelle page de l'EEPROM on travaille. Cet index est multipli� par la taille des pages renseign� dans une variable du pr�processeur. Il faut une variable pour stocker le r�sultat de la conversion ASCII vers int. Accessoirement, le courant de repos sera �ventuellement calcul�.

    //R�initialisation des variables pour s'assurer qu'aucun travail sur les autres voies ne puisse contaminer la voie actuelle.
    InitialisationDonneesCMS(&cEtatVoie, &iRepos, cEtatCapteur, iSeuil, iPression, cConso, cModul);
    //Fin r�initialisation

    //R�cup�ration du num�ro de voie et de la carte.
    cNumCarte = cTrameLectEEPROM1[1] - 48;
    cValeurASCII[0] = cTrameLectEEPROM1[3];
    cValeurASCII[1] = cTrameLectEEPROM1[4];
    cValeurASCII[2] = 0;
    cValeurASCII[3] = 0;
    cValeurASCII[4] = 0;
    cNumVoie = HexASCIIToInt(cValeurASCII, strlen(cValeurASCII));
    //Fin de r�cup�ration du num�ro de voie et de la carte.

    //Traitement des 16 capteurs
    for (cBcl1 = 0; cBcl1 < NombreCapteursVoie; cBcl1++) {
        //On traite le quatuor 'mmmm' en premier car il contient l'�tat du capteur 
        cValeurASCII[0] = cTrameLectEEPROM1[5 + 4 * cBcl1];
        cValeurASCII[1] = cTrameLectEEPROM1[6 + 4 * cBcl1];
        cValeurASCII[2] = cTrameLectEEPROM1[7 + 4 * cBcl1];
        cValeurASCII[3] = cTrameLectEEPROM1[8 + 4 * cBcl1];
        cValeurASCII[4] = 0;
        iValeurBrut = HexASCIIToInt(cValeurASCII, strlen(cValeurASCII));

        if (iValeurBrut & 0b0000100000000000) { //Cf. la documentation REI. Si ce bit est mont�, le capteur n'existe pas.
            iPression[cBcl1] = 0;
            cEtatCapteur[cBcl1] = 99; //Non cr��
            iSeuil[cBcl1] = 0;
        } else {
            iPression[cBcl1] = (iValeurBrut & 0b0000011111111111); //Cf. la documentation REI. La pression repr�sente les 11 bits de poids faible.

            //Etat : Cf. la documentation REI. 
            if (iValeurBrut & 0b0001000000000000) {//SR : Si ce bit est mont�, le capteur est sans r�ponse
                cEtatCapteur[cBcl1] = 50;
                cEtatVoie = 30; //L'alarme doit �tre mont�e de mani�re inconditionnelle
            } else {
                switch ((iValeurBrut & 0b1110000000000000) >> 13) {//PAI : Cf. la documentation REI. Les 3 derniers bits repr�sentent l'�tat du capteur hors SR. On d�cale (>> 13) pour faciliter la lecture du code
                    case 0b000: //Normal
                        cEtatCapteur[cBcl1] = 0;
                        if (cEtatVoie == 2) { //Si la voie est consid�r�e vide, elle est pass�e en �tat OK
                            cEtatVoie = 0;
                        }
                        break;
                    case 0b100: //Pr�-alarme -> Alarme CMS
                        cEtatCapteur[cBcl1] = 30;
                        cEtatVoie = 30; //L'alarme doit �tre mont�e de mani�re inconditionnelle
                        break;
                    case 0b110: //Alarme
                        cEtatCapteur[cBcl1] = 30;
                        cEtatVoie = 30; //L'alarme doit �tre mont�e de mani�re inconditionnelle
                        break;
                    case 0b101: //Intervention
                        cEtatCapteur[cBcl1] = 31;
                        if (cEtatVoie != 30) { //L'�tat d'intervention doit �tre mont� uniquement si la voie n'est pas en alarme                                                cEtatVoie = 31;
                            cEtatVoie = 31;
                        }
                        break;
                    case 0b001: //Sortie d'intervention -> Normal CMS
                        cEtatCapteur[cBcl1] = 0;
                        if (cEtatVoie == 2) { //Si la voie est consid�r�e vide, elle est pass�e en �tat OK
                            cEtatVoie = 0;
                        }
                        break;
                    case 0b010: //Sortie d'alarme -> Normal CMS
                        cEtatCapteur[cBcl1] = 0;
                        if (cEtatVoie == 2) { //Si la voie est consid�r�e vide, elle est pass�e en �tat OK
                            cEtatVoie = 0;
                        }
                        break;
                    default: //Capteur inexistant / Erreur de lecture ?
                        cEtatCapteur[cBcl1] = 99;
                        break;
                }
            }
            //Traitement du seuil si le capteur est d�clar�
            cValeurASCII[0] = cTrameLectEEPROM2[5 + 2 * cBcl1];
            cValeurASCII[1] = cTrameLectEEPROM2[6 + 2 * cBcl1];
            cValeurASCII[2] = 0;
            cValeurASCII[3] = 0;
            cValeurASCII[4] = 0;
            iValeurBrut = HexASCIIToInt(cValeurASCII, strlen(cValeurASCII));
            iSeuil[cBcl1] = (iValeurBrut * 10) + 800; //Cf documentation REI. Le seuil vaut 800 + 10 fois la valeur inscrite dans la trame (sauf si le capteur est inexistant mais les conditions pr�c�d�tes excluent ce cas)
            //Fin de traitement du seuil
        }//Fin du traitement des capteurs existants
    } //Fin du traitement des 16 capteurs
    //Ecriture des donn�es
    EcritureDonneesVoie(REI_Abs(cNumCarte, cNumVoie), cEtatCapteur, iSeuil, iPression, cConso, cModul, cEtatVoie, iRepos);
    //Fin de l'�criture des donn�es
}

void LogiqueResistive(unsigned char *cTrameLectEEPROM1, unsigned char *cTrameLectEEPROM2) {
    unsigned char cValeurASCII[5], cEtatVoie, cNumCarte, cBcl1;
    unsigned int iValeurBrut, iRepos;

    //R�initialisation des variables pour s'assurer qu'aucun travail sur les autres cartes ne puisse contaminer la carte actuelle.
    InitialisationDonneesCMS(&cEtatVoie, &iRepos, cEtatCapteur, iSeuil, iPression, cConso, cModul);
    //Fin r�initialisation

    //Remarque, dans le cas d'une logique r�sistive, seul le capteur 0 est modifi�.
    //Bien que la m�thode d'�criture du programme ne soit pas s�re, il ne devrait pas y avoir trop de d�fauts si les r�gles de bonne conduite sont prises.
    cNumCarte = cTrameLectEEPROM1[1] - 48;

    //Boucle de conversion des voies REI -> CMS. 
    for (cBcl1 = 0; cBcl1 < NombreVoiesCarte; cBcl1++) {
        cValeurASCII[0] = cTrameLectEEPROM2[5 + 2 * cBcl1]; //Lecture du duo de seuil de la voie concern�e.
        cValeurASCII[1] = cTrameLectEEPROM2[6 + 2 * cBcl1]; //On traite d'abord le seuil, car il contient l'information si le capteur existe ou non.
        cValeurASCII[2] = 0;
        cValeurASCII[3] = 0;
        cValeurASCII[4] = 0;
        iValeurBrut = HexASCIIToInt(cValeurASCII, strlen(cValeurASCII)); //Conversion du duo en ASCII en integer.
        if (iValeurBrut != 0x16) { //L'�tat 0x16 correspond � un capteur vide. Sa non pr�sence indique que le capteur existe et qu'il doit �tre trait�.
            //Etat : On r�cup�re ces �tats conform�ment � la doc REI.
            switch ((iValeurBrut & 0b11100000) >> 5) {//PDI. Conform�ment � la doc REI, les trois derniers bits de la valeur servent � indiquer l'�tat de la voie. Ils sont r�cup�r�s et mis en forme (>> 5) pour faciliter le traitement et la lecture du code.
                case 0b000: //Etat normal
                    cEtatCapteur[0] = 0;
                    cEtatVoie = 0;
                    break;
                case 0b100: //Pr�-alarme -> Alarme CMS
                    cEtatCapteur[0] = 30;
                    cEtatVoie = 30;
                    break;
                case 0b010: //Alarme
                    cEtatCapteur[0] = 30;
                    cEtatVoie = 30;
                    break;
                case 0b001: //Intervention
                    cEtatCapteur[0] = 31;
                    cEtatVoie = 31;
                    break;
                case 0b101: //Sortie d'intervention -> Etat normal CMS
                    cEtatCapteur[0] = 0;
                    cEtatVoie = 0;
                    break;
                case 0b110: //Sortie d'alarme -> Etat normal CMS
                    cEtatCapteur[0] = 0;
                    cEtatVoie = 0;
                    break;
                default: //Si �tat inconnu -> Potentielle erreur de conversion ? Voie libre CMS
                    cEtatCapteur[0] = 99;
                    cEtatVoie = 2;
                    break;
            }

            //Seuil : Report du tableau REI dans le code. Comparaison faite avec les bits de poids faible du seuil.
            iSeuil[0] = retour_valeur_tableau_REI((iValeurBrut & 0b00011111));

            //Mesure : R�cup�ration du duo de la mesure de la pression, conversion en integer et r�utilisation du m�me tableau REI que le seuil. 
            cValeurASCII[0] = cTrameLectEEPROM1[5 + 2 * cBcl1];
            cValeurASCII[1] = cTrameLectEEPROM1[6 + 2 * cBcl1];
            cValeurASCII[2] = 0;
            cValeurASCII[3] = 0;
            cValeurASCII[4] = 0;
            iValeurBrut = HexASCIIToInt(cValeurASCII, strlen(cValeurASCII));
            iPression[0] = retour_valeur_tableau_REI(iValeurBrut);
            if(iPression[0] == 9999) //Capteur absent
                cEtatCapteur[0] = 50;
            if(iPression[0] == 0) //Capteur en CC
                cEtatCapteur[0] = 80;
        } else {
            iSeuil[0] = 0;
            cEtatVoie = 2;
            cEtatCapteur[0] = 99;
            iPression[0] = 0;
        }
        EcritureDonneesVoie(REI_Abs(cNumCarte, cBcl1+1), cEtatCapteur, iSeuil, iPression, cConso, cModul, cEtatVoie, iRepos);
    } //Fin de la boucle de conversion pour chaque voie
} //Fin du cas de la logique r�sistive

unsigned int retour_valeur_tableau_REI(unsigned int valeur) {
    unsigned int iResultat;
    switch (valeur) {
        case 0x00:
            iResultat = 900;
            break;
        case 0x01:
            iResultat = 934;
            break;
        case 0x02:
            iResultat = 969;
            break;
        case 0x03:
            iResultat = 1003;
            break;
        case 0x04:
            iResultat = 1038;
            break;
        case 0x05:
            iResultat = 1072;
            break;
        case 0x06:
            iResultat = 1107;
            break;
        case 0x07:
            iResultat = 1141;
            break;
        case 0x08:
            iResultat = 1176;
            break;
        case 0x09:
            iResultat = 1210;
            break;
        case 0x0A:
            iResultat = 1245;
            break;
        case 0x0B:
            iResultat = 1279;
            break;
        case 0x0C:
            iResultat = 1314;
            break;
        case 0x0D:
            iResultat = 1348;
            break;
        case 0x0E:
            iResultat = 1383;
            break;
        case 0x0F:
            iResultat = 1417;
            break;
        case 0x10:
            iResultat = 1452;
            break;
        case 0x11:
            iResultat = 1486;
            break;
        case 0x12:
            iResultat = 1521;
            break;
        case 0x13:
            iResultat = 1555;
            break;
        case 0x14:
            iResultat = 9999;
            break;
        case 0x15:
            iResultat = 0;
            break;
    }
    return (iResultat);
}

unsigned int HexASCIIToInt(char *cTableHex, char cTaille) { //JUSQUA 4 HEX. Ajust� d'un exemple en ligne de conversion d'ascii hexa en integer
    char t;
    float fPuissance;
    unsigned char cTampon[4] = {0, 0, 0, 0};
    unsigned int calcl = 0;
    for (t = 0; t < cTaille; t++) {
        if (cTableHex[t] >= 65) //'A'
            cTampon[t] = cTableHex[t] - 55;
        else
            cTampon[t] = cTableHex[t] - 48; // s' '0'.........'9'...  
    }
    for (t = 0; t <= cTaille - 1; t++) {
        fPuissance = pow(16, cTaille - 1 - t);
        calcl += (cTampon[t] * (unsigned int) fPuissance);
    }
    return (calcl);
}

void InitialisationDonneesCMS(unsigned char *cEtatVoie, unsigned int *iRepos, unsigned char *cEtatCapteur, unsigned int *iSeuil, unsigned int *iPression, char *cConso, char *cModul) {
    char i;
    *cEtatVoie = 2; //Etat voie libre, par d�faut.
    *iRepos = 0; //Courant de repos nul car inconnu. A voir si utile dans une prochaine mise � jour.
    for (i = 0; i < NombreCapteursVoie; i++) {
        cEtatCapteur[i] = 99;
        iSeuil[i] = 0;
        iPression[i] = 0;
        cConso[i] = 0;
        cModul[i] = 0;
    }
}
