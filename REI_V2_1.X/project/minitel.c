//Attente fin de trame
#include <stdio.h>
#include <stdlib.h>
#include <p18F97J60.h>
#include <main.h>
#include <string.h>
#include <i2c.h>
#include <Tick.h>
#include <math.h>

extern unsigned char cAlim;
extern unsigned char cType;
extern unsigned char cDemandeMinitel;
extern unsigned char cDemarrage;
extern DWORD tAppelREI;
volatile unsigned char* pTable;
unsigned char cTableauModif[NombreVoiesCarte][4];
unsigned char cTrameLecture[90];
unsigned char cIndexMax = 0;
unsigned char cIndexTableau = 0;
unsigned int iTableau_version[7] = {0, 0, 0, 0, 0, 0, VersionBtr};
unsigned int cTblSeuilResi[20] = {900, 934, 969, 1003, 1038, 1072, 1107, 1141, 1176, 1210, 1245, 1279, 1314, 1348, 1383, 1417, 1452, 1486, 1521, 1555};
unsigned char cCompteurErreur = 0;
DWORD tTimeout = 0;

void minitel(void) {
    signed int iMesure = 0;
    signed int iSeuil = 0;
    signed int iValeur = 0;
    unsigned char cIndex = 0;
    unsigned char cVoie = 0;
    unsigned char cCapteur = 0;
    static unsigned int iBcl = 0;
    static unsigned int iBoucle = 0;
    static unsigned char cSens = 0;
    static unsigned char cEncours = 0;
    static unsigned char cAdressable = 0;
    static unsigned char cRouleau = 1;

    static enum _EtatMinitel {
        SM_HOME = 0,
        SM_DIALOG,
        SM_CLOSING,
    } EtatMinitel = SM_HOME;

    static enum _EtatREI {
        Accroche = 0,
        Debut,
        RepREI,
        DeblocRoul,
        InitCom,
        SelectionVoie,
        SelectionCapteur,
        Traitement,
        Report,
        Increment,
    } EtatREI = Accroche;

    switch (EtatMinitel) {
        case SM_HOME: //Etat par d�faut quand il n'y a aucune communication
            if ((cDemandeMinitel)&&(!DSR)) { //Si on demande une connexion et que le rack est dispo
                LEDe = 0; //On �teint la LED rouge, et on allume la verte
                LEDd = 1;
                DTR = 0; //On demande une communication avec le rack
                //InitRecept();
                cTrameLecture[0] = 0x13; // Il faut envoyer '<DC3>Y<DC3>[<DC3>S' ()
                cTrameLecture[1] = 0x59;
                cTrameLecture[2] = 0x13;
                cTrameLecture[3] = 0x5B;
                cTrameLecture[4] = 0x13;
                cTrameLecture[5] = 0x53;
                RS232_EnvoiTrame((unsigned char *) cTrameLecture, 6);
                InitRecept();
                EtatMinitel++; //Etape suivante
            }
            break;

        case SM_DIALOG: //On communique
            switch (EtatREI) { //Gestion de la communication avec REI.
                case Accroche: //Tentatives d'accroche avec le rack REI.
                    if (TickGet() - tTimeout > TICK_SECOND * 10ul) { //10 secondes maxi
                        cCompteurErreur++;
                        EtatMinitel = SM_CLOSING; //On sort de la communication
                        return;
                    }
                    cIndex = CheckRecept('a'); //On v�rifie si on a re�u un 'a'
                    if ((cIndex != 0xFF)&&(cTrameLecture[cIndex - 1] == 0x1B)) { //On a demand� la communication avec REI, celui-ci doit r�pondre <FF><ESC>a (0x0C 0x1B 0x61)
                        FinRecept(); //Si on a bien re�u '<ESC>a'
                        EtatREI++; //Etape suivante
                    }
                    break;
                case Debut: //Initiation connexion minitel
                    cTrameLecture[0] = 0x1F; // Pour r�pondre au rack, il faut renvoyer '<US>AA' (0x1F 0x41 0x41)
                    cTrameLecture[1] = 'A';
                    cTrameLecture[2] = 'A';
                    RS232_EnvoiTrame((unsigned char *) cTrameLecture, 3);
                    InitRecept();
                    EtatREI++; //Etape suivante
                    break;

                case RepREI:
                    if (TickGet() - tTimeout > (DWORD) (TICK_SECOND * 5ul)) { //5 sec maxi
                        cCompteurErreur++;
                        EtatMinitel = SM_CLOSING; //On sort de la communication
                        return;
                    }
                    cIndex = CheckRecept('C'); //On regarde si on a re�u un 'C'
                    if ((cIndex != 0xFF)&&(cTrameLecture[cIndex - 3] == 0x1B)) { //REI va nous demander de passer en mode rouleau '<FF><ESC>:iC'
                        FinRecept(); //Si on a bien re�u '<FF><ESC>:iC'
                        tTimeout = TickGet();
                        cCompteurErreur = 0;
                        if (cRouleau) { //Si on doit v�rifier que le mode rouleau existe
                            EtatREI++; //Etape suivante
                        } else {
                            EtatREI += 2;
                        }
                    }
                    break;

                case DeblocRoul:
                    if ((TickGet() - tTimeout > (DWORD) (TICK_SECOND * 8ul))) { //Attente
                        if (iTableau_version[0] < 265){ //Si on est avec REI 2.65-
                            RS232_EnvoiCarac(0); //On doit envoyer un 'NUL'
                            DELAY_X50MS(100);
                        }
                        RS232_EnvoiCarac('3'); // Il faut envoyer '3' puis 'Enter' pour passer en mode rouleau.
                        DELAY_X50MS(2);
                        cTrameLecture[0] = 0x13;
                        cTrameLecture[1] = 0x41;
                        RS232_EnvoiTrame((unsigned char *) cTrameLecture, 2);
                        RS232_EnvoiCarac(0);
                        tTimeout = TickGet();
                        EtatREI++;
                    }
                    break;

                case InitCom:
                    if (TickGet() - tTimeout > (DWORD) (TICK_SECOND * 6ul)) { //Attente
                        DELAY_X50MS(5);
                        if (iTableau_version[0] < 300)
                            DELAY_X50MS(50);
                        Reinit_mdp_plus_saisie(); //On r�initialise le MDP du rack
                        if (cRouleau) { //Dans le cas de la d�tection du mot rouleau, on fait des commandes suppl�mentaires
                            DELAY_X50MS(15);
                            ModeRouleauDefaut();
                            cRouleau = 0;
                        }
                        EtatREI++; //Etape suivante
                    }
                    break;
                case SelectionVoie:
                    if (cTableauModif[cIndexTableau][0] != 0) { //On regarde si on a des ordres
                        if (cTableauModif[cIndexTableau][0] != 4) { //Si ce n'est pas pour r�cup�rer les versions
                            if (!cAdressable){// Si c'est r�sistif
                                cVoie = cTableauModif[cIndexTableau][1]; //On prend la voie et on l'utilise pour la s�lectionner sur le minitel
                                Selection_voieD_capteurd(cVoie, 'D');
                                cAdressable = cType & (0x01 << ((cVoie - 1) / NombreVoiesCarte)); //On v�rifie qu'on est bien en r�sistif
                                InitRecept();
                            }
                            if (cAdressable) //Si c'est r�sistif, on passe la partie capteur (168 : EtatREI +=2), sinon, on incr�mente de 1 seulement
                                EtatREI--;
                        } else {
                            Selection_voieD_capteurd(cIndexTableau * NombreVoiesCarte + 1, 'D'); //R�cup�ration de la version des cartes
                            DELAY_X50MS(60);
                            RS232_EnvoiCarac('v');
                            InitRecept();
                        }
                        EtatREI += 2; //Passage au capteur ou � l'ordre
                    } else {
                        EtatREI = Increment; //Ordre suivant
                    }
                    break;

                case SelectionCapteur: //Si adressable
                    if (TickGet() - tTimeout > (DWORD) (TICK_SECOND * 5ul)) {
                        cCompteurErreur++;
                        EtatREI--;
                        cAdressable = 0;
                        if(cCompteurErreur >= 5)
                             EtatREI = SM_CLOSING; //On sort de la communication
                        return;
                    }
                    if (!strchr(cTrameLecture, 0x0A)) //On attend la fin d'envoi d'une trame avec 'CR' (0x0A)
                        return;
                    pTable = (volatile unsigned char *) cTrameLecture;
                    cIndex = CheckRecept('.'); //Si on a un point, on arrive � la derni�re trame �mise par le minitel et on peut envoyer de nouveaux caract�res
                    if ((cIndex == 0xFF) || (cTrameLecture[cIndex - 1] < '0') || (cTrameLecture[cIndex - 1] > '9'))
                        return;
                    FinRecept(); //Si on a bien re�u un point.
                    cCapteur = cTableauModif[cIndexTableau][2]; //On envoie le capteur
                    Selection_voieD_capteurd(cCapteur, 'd');
                    InitRecept();
                    EtatREI++; //Etape suivante
                    break;
                    
                case Traitement: //Lecture des ordres et execution
                    if (TickGet() - tTimeout > TICK_SECOND * 5ul) {
                        cCompteurErreur++;
                        EtatREI -= 2;
                        cAdressable = 0;
                        if(cCompteurErreur >= 5)
                             EtatREI = SM_CLOSING; //On sort de la communication
                        return;
                    }
                    if (!strchr(cTrameLecture, 0x0A))//Attente fin d'envoi des trames minitel
                        return;
                    pTable = (volatile unsigned char *) cTrameLecture;
                    cIndex = CheckRecept('.');
                    if ((cIndex == 0xFF) || (cTrameLecture[cIndex - 1] < '0') || (cTrameLecture[cIndex - 1] > '9'))
                        return;
                    FinRecept(); //Si on a bien re�u un point.
                    cCompteurErreur = 0;
                    switch (cTableauModif[cIndexTableau][0]) {
                        case 1: //Suppression capteur
                            if (!strchr(cTrameLecture, 'L')) { //Si capteur pas libre
                                if (cAdressable) { //Si adressable
                                    Declaration_Liberation();
                                    InitRecept();
                                } else {//Si r�sistif
                                    cTableauModif[cIndexTableau][0] = 2;
                                    cTableauModif[cIndexTableau][3] = 20;
                                }
                                return; //Return pour attendre envoi des trames du minitel
                            }
                            break;
                        case 2: //D�claration d'un capteur
                            if (cEncours == 0) { //En deux �tapes
                                if (cAdressable) { //Si adressable
                                    if (cTrameLecture[cIndex + 20] == 'L') { //V�rif capteur d�clar�
                                        Declaration_Liberation();
                                        InitRecept();
                                        return; //Return pour attendre envoi des trames du minitel
                                    }
                                    iValeur = 800 + (int) cTableauModif[cIndexTableau][3]*10; //Valeur voulue
                                    iSeuil = atoi(cTrameLecture + 24); //Valeur du seuil actuel
                                    if ((cTrameLecture[8] >= '0')&&(cTrameLecture[8] <= '9')) {
                                        iMesure = atoi(cTrameLecture + 8); //Valeur de pression, utilis�e pour l'action "a" dans le minitel
                                        if (iMesure >= 860) {//Ne fonctionne que si la pression vaut au minimum 860mbar. (Seuil mini = 800mbar) (Seuil descendu avec "a" : 60mbar)
                                            if (fabs(iValeur - (iMesure - 60)) < fabs(iValeur - iSeuil)) { //V�rification si utiliser "a" est rentable.
                                                RS232_EnvoiCarac('a');
                                                iSeuil = iMesure - 60; //Si oui la nouvelle valeur du seuil = la pression - 60.
                                            }
                                        }
                                    }
                                    iBoucle = fabs(iValeur - iSeuil); //D�termination du sens de modification du seuil + haut, ou - haut
                                    if (iBoucle <= 500) {
                                        cSens = (iSeuil < iValeur) ? '+' : '-';
                                    } else {
                                        iBoucle = 1010 - iBoucle;
                                        cSens = (iSeuil < iValeur) ? '-' : '+';
                                    }
                                    iBoucle /= 10;
                                } else { //Si r�sistif
                                    if (cTrameLecture[cIndex + 13] == 'L') { //Si capteur non d�clar�
                                        iSeuil = 20;
                                    } else {
                                        iSeuil = atoi(cTrameLecture + cIndex + 16); //On r�cup�re le seuil actuel et on regarde dans un tableau de correspondance de combien on doit incr�menter
                                        for (cIndex = 0; cIndex < 20; cIndex++) {
                                            if ((cTblSeuilResi[cIndex] % 100) == iSeuil) {
                                                iSeuil = cIndex;
                                                break;
                                            }
                                        }
                                    }
                                    iValeur = cTableauModif[cIndexTableau][3];
                                    iBoucle = fabs(iValeur - iSeuil); //Cf. partie adressable
                                    if (iBoucle <= 10) {
                                        cSens = (iSeuil < iValeur) ? '+' : '-';
                                    } else {
                                        iBoucle = 21 - iBoucle;
                                        cSens = (iSeuil < iValeur) ? '-' : '+';
                                    }
                                }
                                cEncours = 1; //On a tout initialis�, on passe � la suite
                            }
                            if (iBcl < iBoucle) { //tant que l'incr�mentation n'est pas finie
                                Augmentation_Diminution_Seuil(cSens);
                                InitRecept();
                                iBcl++;
                                return;
                            }
                            iBcl = 0;
                            iBoucle = 0;
                            cEncours = 0;
                            cSens = 0;
                            break;
                            //                        case 3:
                            //                            Intervention_Alarme();
                            //                            DELAY_X50MS(5);
                            //                            break; //Pr�voir la modification de l'�tat d'alarme
                        case 4: //r�cup�ration des versions de la carte choisie
                            if (cEncours == 0) {
                                InitRecept();
                                cEncours = 1;
                            } else {
                                iTableau_version[cIndexTableau + 1] = (int) (cTrameLecture[cIndex - 1] - 48)*100 + (int) (cTrameLecture[cIndex + 1] - 48)*10 + (int) (cTrameLecture[cIndex + 2] - 48);
                                cEncours = 0;
                                EtatREI = Increment;
                            }
                            return;
                    }
                    EtatREI++;
                    break;
                case Report: //Actualisation des donn�es du bo�tier avec les donn�es re�ues par minitel
                    cIndex = CheckRecept('.'); //On attend un '.'
                    if (cIndex != 0xFF) {
                        cVoie = 0;
                        if ((cTrameLecture[cIndex - 2] >= '0') && (cTrameLecture[cIndex - 2] <= '9')){ //On lit la voie. Si voie 100, on lit 0
                            cVoie += (cTrameLecture[cIndex - 2] - 48)*10;
                        }
                        cVoie += cTrameLecture[cIndex - 1] - 48;
                        cCapteur = ((cTrameLecture[cIndex + 1] - 48)*10 + (cTrameLecture[cIndex + 2] - 48)) - 1; //On lit le capteur
                        if ((cTrameLecture[cIndex + 20] == 'L') || (cTrameLecture[cIndex + 13] == 'L')) { //Capteur libre
                            MiseajourSeuil(cVoie, cCapteur, 0, 0);
                        } else {
                            if (cTrameLecture[cIndex + 5] == 'D') { //D�faut
                                iMesure = -1;
                            } else if (cTrameLecture[cIndex + 5] == '9') { //Sans r�ponse
                                iMesure = 9999;
                            } else {
                                iMesure = 0;
                                if (cTrameLecture[cIndex + 5] == '1'){ //Au moins 1000
                                    iMesure = 1000;
                                }
                                iMesure += (signed int)(cTrameLecture[cIndex + 6] - 48)*100; //Evite un d�bordement de char
                                iMesure += (cTrameLecture[cIndex + 7] - 48)*10;
                                iMesure += (cTrameLecture[cIndex + 8] - 48);
                            }
                            if (cAdressable) { //Si adressable
                                iSeuil = atoi(cTrameLecture + cIndex + 21); //Valeur du seuil actuel
                            } else {//si r�sistif
                                iSeuil = atoi(cTrameLecture + cIndex + 16); //r�cup�ration du seuil et conversion dans le tableau de correspondance
                                for (cIndex = 0; cIndex < 20; cIndex++) {
                                    if ((cTblSeuilResi[cIndex] % 100) == iSeuil) {
                                        iSeuil = cTblSeuilResi[cIndex];
                                        break;
                                    }
                                }
                            }
                            MiseajourSeuil(cVoie, cCapteur, iSeuil, iMesure); //Actualisation dans l'EEPROM
                        }
                    }
                    EtatREI++;
                    break;
                    
                case Increment://Ordre suivant
                    cIndexTableau++;
                    tTimeout = TickGet();
                    if (cIndexTableau >= NombreVoiesCarte) { //On a fini
                        cCompteurErreur = 0;
                        EtatMinitel = SM_CLOSING; //Fermeture
                    } else {//On continue
                        EtatREI = SelectionVoie;
                    }
                    break;
            }
            break;
        case SM_CLOSING:
            LEDd = 0; //Extinction de la LED verte
            cTrameLecture[0] = 0x13; // Il faut envoyer '<DC3>I' pour fermer la comm'.
            cTrameLecture[1] = 'I';
            RS232_EnvoiTrame((unsigned char *) cTrameLecture, 2);
            DELAY_X50MS(5);
            DTR = 1; //Fermeture de la communication avec REI
            EtatMinitel = SM_HOME; //R�initialisation des �tats
            EtatREI = Accroche;
            if ((cCompteurErreur != 0)&&(cCompteurErreur < 5))
                return;
            for(cIndexTableau = 0; cIndexTableau < NombreVoiesCarte;cIndexTableau++){
                for (iBcl = 0; iBcl < 4; iBcl++)
                    cTableauModif[cIndexTableau][iBcl] = 0;
            }
            if(cDemarrage){
                cDemarrage = 0;
                tAppelREI = TickGetDiv64K(); //Mise � jour du timer pour la prochaine connexion avec REI
            }
            iBcl = 0;
            cDemandeMinitel = 0; //On r�initialise la demande
            cIndexTableau = 0;
            cIndexMax = 0;
            cCompteurErreur = 0;
            break;
    }
}
//D�lai pour le minitel de x fois 50 ms
void DELAY_X50MS(unsigned char cTemps) {
    DWORD t = 0;
    t = TickGet();
    while ((TickGet() - t <= (DWORD) ((TICK_SECOND * cTemps) / 20ul)));
    LEDd ^= 1;
}
//Cr�ation ou modification du capteur
void CreaLibe1Modif2Capteur(unsigned char cOrdre, unsigned char cVoieAbs, unsigned char cCaptAbs, unsigned int iSeuil) {
    if (cIndexMax >= NombreVoiesCarte)
        return;
    cTableauModif[cIndexMax][0] = cOrdre;
    cTableauModif[cIndexMax][1] = cVoieAbs + 1;
    cTableauModif[cIndexMax][2] = cCaptAbs + 1;
    cTableauModif[cIndexMax][3] = iSeuil;
    cIndexMax++;
}
//Proc�dure de r�initialisation 
void ModeRouleauDefaut(void) {
    char cEnter[2] = {0x13, 0x41};
    RS232_EnvoiCarac('P');
    DELAY_X50MS(10);
    RS232_EnvoiTrame(cEnter, 2);
    DELAY_X50MS(10);
    RS232_EnvoiTrame(cEnter, 2);
    DELAY_X50MS(7);
    RS232_EnvoiCarac('O');
    DELAY_X50MS(7);
    RS232_EnvoiCarac('N');
    DELAY_X50MS(7);
    RS232_EnvoiCarac('O');
    DELAY_X50MS(30);
}
//Proc�dure de r�initialisation du mot de passe
char Reinit_mdp_plus_saisie(void) {
    char cBcl;
    char cEnter[2] = {0x13, 0x41};
    RS232_EnvoiCarac('[');
    RS232_EnvoiCarac('5');
    RS232_EnvoiCarac('*');
    RS232_EnvoiCarac(']');
    RS232_EnvoiCarac('D');
    DELAY_X50MS(15);
    RS232_EnvoiCarac('O');
    DELAY_X50MS(30);
    RS232_EnvoiCarac('K');
    DELAY_X50MS(10);
    for (cBcl = 0; cBcl < 5; cBcl++) {
        RS232_EnvoiCarac('*');
        DELAY_X50MS(1);
    }
    RS232_EnvoiTrame(cEnter, 2);
    DELAY_X50MS(25);
    return 0;
}
//S�lection de voie ou de capteur : 'D' pour une voie et 'd' pour un capteur
void Selection_voieD_capteurd(char cValeur, char cType) {
    char cEnter[2] = {0x13, 0x41};
    RS232_EnvoiCarac(cType);
    DELAY_X50MS(10);
    if (cType == 'D') {
        RS232_EnvoiCarac(48 + cValeur / 100);
        DELAY_X50MS(6);
    }
    RS232_EnvoiCarac(48 + (cValeur % 100) / 10);
    DELAY_X50MS(6);
    RS232_EnvoiCarac(48 + cValeur % 10);
    DELAY_X50MS(6);
    RS232_EnvoiTrame(cEnter, 2);
}

void Augmentation_Diminution_Seuil(char cModif) {
    if (cModif == '+')
        RS232_EnvoiCarac('S'); //S = +
    if (cModif == '-')
        RS232_EnvoiCarac('s'); //s = -
}

void Declaration_Liberation(void) {
    RS232_EnvoiCarac('!');
}

//void Intervention_Alarme(void) {
//    RS232_EnvoiCarac('C'); //Si pas en alarme pour passage en intervention, apparition de "BEL" sur la ligne du capteur.
//}
//r�initialisation des tableaux pour la r�ception
void InitRecept(void) {
    unsigned char cBcl;
    for (cBcl = 0; cBcl < 89; cBcl++) {
        cTrameLecture[cBcl] = 0xFF;
    }
    cTrameLecture[89] = 0x00;
    pTable = (volatile unsigned char *) cTrameLecture;
    PIR1bits.RC1IF = 0;
    PIE1bits.RC1IE = 1; // Enable interrupt
    tTimeout = TickGet();
    RCSTA1bits.CREN = 0; //Clear buffer
    RCSTA1bits.CREN = 1;
}

void FinRecept(void) {
    LEDd ^= 1;
    PIE1bits.RC1IE = 0; // Disable interrupt
}
//V�rification si un caract�re est pr�sent dans le tableau de r�ception
unsigned char CheckRecept(unsigned char cCarac) {
    char* cBcl;
    if (cBcl = strchr(cTrameLecture, cCarac))
        return (cBcl - cTrameLecture);
    return 0xFF;
}
//R�ception d'un caract�re
void Reception(void) {
    if (PIR1bits.RC1IF) { //si on re�oit quelque chose
        if (RCSTA1bits.OERR) { //Si erreur OERR ou d�passement watchdog
            RCSTA1bits.CREN = 0;
            RCSTA1bits.CREN = 1;
        }
        *pTable = (RCREG1 & 0x7F); //On lit le caract�re
        pTable++; //On incr�mente dans le tableau de r�ception
        if(pTable - cTrameLecture >= 89) //Si on est au taquet
            pTable = (volatile unsigned char *) cTrameLecture; //On roll la r�ception
        else
            *pTable = 0x00; //Sinon on termine la trame
    }
}
