"use strict";
const url = new URL(window.location.href);
const Voie = parseInt(url.searchParams.get('V'), 10);
const Carte = url.searchParams.has('C') ? parseInt(url.searchParams.get('C'), 10) : Math.floor((Voie - 1) / 20) + 1;
const CarteOffset = (Carte - 1) * 20;
var CarteOuVoie = url.searchParams.get('R') ? 1 : 0;
var Alim = 0;
var Type = 0;
var lintervalle = 0;
var activeRequests = 0;
var textbase = "Actualisation automatique des donn\xE9es dans ";
var txtCom1 = " <img hidden class='aide' tabindex='0' src='./exclamation.svg' alt='Logo' id='Bulle";
var txtCom2 = "'/><span class='hide' id='Cache";
var txtCom3 = "'></span>";
var CarteResis = "Resistive";
var CarteAdres = "Adressable";
var cTblSeuilResi = [900, 934, 969, 1003, 1038, 1072, 1107, 1141, 1176, 1210, 1245, 1279, 1314, 1348, 1383, 1417, 1452, 1486, 1521, 1555];
var NomParam = ["P. Réservoir", "P. Sortie", "P. Secours", "Point de Rosée", "Débit Global", "Temps Cycle Moteur", 0, "Temps Moteur", "Présence 220V", "Présence 48V"];
var Unité = ["mbar", "mbar", "mbar", "°K", "l/h", "s", 0, "h", "N/A", "N/A"];

function Debutpage() {
    var menu = '<meta name="viewport" content="width=device-width, initial-scale=1">';
    menu += '<meta charset="utf-8" />';
    menu += '<title id="TitrePage">CMS</title>';
    menu += '<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />';
    menu += '<meta http-equiv="Pragma" content="no-cache" />';
    menu += '<meta http-equiv="Expires" content="0" />';
    document.head.insertAdjacentHTML("afterbegin", menu);

    menu = '<nav id="colonnegauche">';
    menu += '<img class="logo" src="./logo_soderel.svg" alt="Logo"/>';
    menu += '<label for="hamburger">&#9776;</label>';
    menu += '<input type="checkbox" id="hamburger"/>';
    menu += '<div id ="Nav">';
    menu += '<a href="./Site.html" id="LienSite">Site</a>';
    menu += '<a href="./ParamGroupe.html" id="LienParam">Groupe</a>';
    menu += '<a href="./BlocNotes.html?C=6" id="LienBN">Bloc-Notes</a>';
    menu += '</div>';
    menu += '</nav>';
    menu += '<h1 id="NomSiteActu">CMS</h1>';
    menu += '<h2 id="HeureAppel">' + textbase + '0 minute(s) 0 seconde(s).</h2>';
    menu += '<h2 id="HeureDerAppel"></h2>';
    menu += '<h2 id="EtatConn"></h2>';
    menu += '<div id="snOK">✅ Donn\xE9es re\xE7ues !</div>';
    document.body.insertAdjacentHTML("afterbegin", menu);
    let x = document.getElementById("Nav");
    x.hidden = true;
    for (let i = 0; i < 5; i++) {
        x.innerHTML += "<a tabindex=" + i + " id='Menu" + i + "'>Carte " + (i + 1) + "</a>";
    }
    x.innerHTML += '<a href="./PageIP.html" id="LienIP">Bo\xEEtier</a>';
    var p = Promise.resolve();
    p = p.then(newAJAXCommand('./Carte.xml', TypeVoie));
    p = p.then(newAJAXCommand("./Info.xml?Num=6", function (xmlData) {
                ActuDate(parseInt(getXMLValue(xmlData, 'InfosPage'), 10))
            }));
	p = p.then(newAJAXCommand("./Info.xml?Num=7", function (xmlData) {
            document.getElementById('HeureDerAppel').innerHTML = 'Dernier appel il y a ' + parseInt(getXMLValue(xmlData, 'InfosPage'), 10) + ' heure(s)';
			if (parseInt(getXMLValue(xmlData, 'Ver'), 10) == 1){
				document.getElementById('EtatConn').innerHTML = 'La logique semble indisponible';
				document.getElementById('EtatConn').style.color = 'red';
			}else{
				document.getElementById('EtatConn').innerHTML = 'La logique est disponible';
				document.getElementById('EtatConn').style.color = 'green';
			}
			}));
    p = p.then(newAJAXCommand('./Info.xml?Num=0', NomSite));
    p = p.catch((err) => alert(err));
}

function loadScript(src) {
    return new Promise(function (resolve, reject) {
        const s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = src;
        s.async = false;
        s.onload = () => resolve();
        s.onerror = () => reject("Erreur de chargement du script '" + src + "'. Merci de recharger la page. Contactez le support si le probl\xE8me persiste.");
        document.head.appendChild(s);
    }).catch((err) => alert(err)); ;
}

function initScript() {
    var p = Promise.resolve();
    if ((url.searchParams.get('R')) || (url.searchParams.get('V'))) {
        p = p.then(loadScript("./Capteurs.js"));
    }
    if ((url.searchParams.get('C')) && (!url.searchParams.get('R'))) {
        p = p.then(loadScript("./Voies.js"));
    }
    if (url.searchParams.get('C')) {
        p = p.then(loadScript("./BlocNotes.js"));
    }
}

// Initiates a new AJAX command
//	url: the url to access
//	container: the document ID to fill, or a function to call with response XML (optional)
//	repeat: true to repeat this call indefinitely (optional)
//	data: an URL encoded string to be submitted as POST data (optional)
function newAJAXCommand(url, container, data) {
    return new Promise(function (resolve, reject) {
        // Set up our object
        var newAjax = {};
        var theTimer = new Date();
        newAjax.url = url;
        newAjax.container = container;

        newAjax.ajaxReq = new XMLHttpRequest();
        newAjax.ajaxReq.open((data == null) ? "GET" : "POST", newAjax.url, true);
        if (newAjax.url.includes("protect")) {
            newAjax.ajaxReq.withCredentials = true;
        }
        newAjax.ajaxReq.setRequestHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        newAjax.ajaxReq.onload = function () {
            activeRequests--;
            if (this.status === 200) {
                // If we suceeded
                // If it has a container, write the result
                if (typeof(newAjax.container) === 'function') {
                    newAjax.container(newAjax.ajaxReq.responseXML.documentElement);
                } else if (typeof(newAjax.container) === 'string') {
                    document.getElementById(newAjax.container).innerHTML = newAjax.ajaxReq.responseText;
                } // (otherwise do nothing for null values)
                if (newAjax.url.includes("protect")) {
                    SN("snOK");
                }
                resolve();
            } else if (this.status >= 400) {
                reject(new Error("Erreur\nStatut de la r\xE9ponse : " + newAjax.ajaxReq.status + " " + newAjax.ajaxReq.statusText));
            }
        };
        newAjax.ajaxReq.send(data);
        activeRequests++;
    });
}

// Parses the xmlResponse returned by an XMLHTTPRequest object
//	xmlData: the xmlData returned
//  field: the field to search for
function getXMLValue(xmlData, field) {
    try {
        if (xmlData.getElementsByTagName(field)[0].firstChild.nodeValue)
            return xmlData.getElementsByTagName(field)[0].firstChild.nodeValue;
        else
            return null;
    } catch (err) {
        return null;
    }
}

function NomSite(xmlData) {
    let Nom = getXMLValue(xmlData, 'InfosPage')
        document.getElementById('NomSiteActu').innerHTML = Nom;
    document.getElementById('TitrePage').innerHTML = Nom;
}

function TypeVoie(xmlData) {
    Type = getXMLValue(xmlData, 'TypeCarte');
    Alim = getXMLValue(xmlData, 'EtatCarte');
    let R;
    let Ttext;
    for (let i = 0; i < 5; i++) {
        if (Alim & (1 << i)) {
            if (Type & (1 << i)) {
                Ttext = ' - ' + CarteAdres;
                R = '';
            } else {
                Ttext = ' - ' + CarteResis;
                R = '&R=1';
            }
            document.getElementById('Menu' + i + '').innerHTML += Ttext;
            document.getElementById('Menu' + i + '').href = './InfoRack.html?C=' + (i + 1) + R;
        } else {
            document.getElementById('Menu' + i).style.cursor = "not-allowed";
            document.getElementById('Menu' + i).style.backgroundColor = "grey";
        }
    }
    document.getElementById("Nav").hidden = false;
}

function SN(idSN) {
    var x = document.getElementById(idSN);
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 3000);
}

function ActuDate(x) {
    if (lintervalle)
        clearInterval(lintervalle);
    if (x > 60) {
        window.location.href = './Site.html';
    }
    // Set the date we're counting up to
    var Dateplus = new Date();
    Dateplus.setMinutes(Dateplus.getMinutes() + x);
    Dateplus = Dateplus.getTime();
    // Update the count down every 1 second
    lintervalle = setInterval(function () {
        // Find the distance between now and the count down date
        var distance = Dateplus - new Date().getTime();

        // Time calculations for days, hours, minutes and seconds
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        if (minutes < 0)
            minutes = 0;
        if (seconds < 0)
            seconds = 0;
        // Display the result in the element with id="demo"
        document.getElementById("HeureAppel").innerHTML = textbase + minutes + " minute(s) " + seconds + " seconde(s).";

        // If the count down is finished, write some text
        if (distance <= 1) {
            clearInterval(lintervalle);
            newAJAXCommand("./Info.xml?Num=6", function (xmlData) {
                ActuDate(parseInt(getXMLValue(xmlData, 'InfosPage') + 1, 10))
            }).catch((err) => alert(err));
            textbase = "Actualisation en cours ! Vérification dans ";
        }
    }, 1000);
}
	
	
Debutpage();
if(window.location.pathname == '/InfoRack.html')
	initScript();
if(window.location.pathname == '/BlocNotes.html')
	loadScript("./BlocNotes.js");