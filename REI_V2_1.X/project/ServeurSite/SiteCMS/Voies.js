"use strict";
function DebutJsVoies(){
	let MaTable = document.createElement('table');
	MaTable.createCaption();
	MaTable.caption.textContent = "Données carte n°" + Carte;
	let Thead = MaTable.createTHead();
	let ligne = Thead.insertRow(0);
	for (let i = 0; i < 3; i++) {
		ligne.insertCell(-1);
	}
	let x = MaTable.getElementsByTagName("td");
	x[0].textContent = "Voie";
	x[1].textContent = "Etat";
	x[2].textContent = "Capteurs présents";

	let Tbody = MaTable.createTBody();

	for (let i = 1; i < 21; i++) {
		var Maligne = Tbody.insertRow(-1);
		Maligne.id = "LigneVoie" + i;
		Maligne.style.cursor = "pointer";
		Maligne.setAttribute('onmouseover', 'DessusVoie(' + i + ');');
		Maligne.setAttribute('onmouseOut', 'DeselectionVoie(' + i + ');');
		for (let j = 0; j < 3; j++) {
			let cell = Maligne.insertCell(-1);
			cell.textContent = "???";
		}
		let y = Maligne.getElementsByTagName("td");
		let textlinkdebut = "<a class='fulltd' href='./InfoRack.html?V="+ (CarteOffset + i) +"'><div id='";
		let textlinkfin = i+"' class='fulltd'>";
		y[0].innerHTML = textlinkdebut + "Voie" + textlinkfin + "Voie " + (CarteOffset + i) + txtCom1 + i + txtCom2 + i + txtCom3 + "</div>";
		y[1].innerHTML = textlinkdebut + "Etat" + textlinkfin + "???</div>";
		y[2].innerHTML = textlinkdebut + "CapteursPresents" + textlinkfin + "???</div>";
	}
	document.getElementById('Tableau').appendChild(MaTable);
	MiseAJour();
}

function MiseAJourVoie(xmlData) {
	let NumVoie = getXMLValue(xmlData, 'NumeroVoie') - CarteOffset;
	switch (getXMLValue(xmlData, 'Etat')) {
	case "0":
		document.getElementById('Etat' + NumVoie).innerHTML = "OK";
		document.getElementById('LigneVoie' + NumVoie).style.backgroundColor = "green";
		break;
	case "1":
		document.getElementById('Etat' + NumVoie).innerHTML = "Groupe";
		document.getElementById('LigneVoie' + NumVoie).style.backgroundColor = "blue";
		document.getElementById('LigneVoie' + NumVoie).style.cursor = "not-allowed";
		Array.from(document.getElementById('LigneVoie' + NumVoie).querySelectorAll("a")).forEach((element) => element.removeAttribute('href'));
		break;
	case "2":
		document.getElementById('Etat' + NumVoie).innerHTML = "Non présent";
		document.getElementById('LigneVoie' + NumVoie).style.backgroundColor = "grey";
		break;
	case "30":
		document.getElementById('Etat' + NumVoie).innerHTML = "Alarme";
		document.getElementById('LigneVoie' + NumVoie).style.backgroundColor = "red";
		break;
	case "31":
		document.getElementById('Etat' + NumVoie).innerHTML = "En intervention";
		document.getElementById('LigneVoie' + NumVoie).style.backgroundColor = "orange";
		break;
	default:
		document.getElementById('Etat' + NumVoie).innerHTML = getXMLValue(xmlData, 'Etat');
		break;
	}
	document.getElementById('CapteursPresents' + NumVoie).innerHTML = getXMLValue(xmlData, 'CapteursPresents');
}

function MiseAJour() {
	let p = Promise.resolve();
	for (let i = 1; i < 21; i++) {
		p = p.then(newAJAXCommand('./Voie.xml?V=' + (CarteOffset + i), MiseAJourVoie));
	}
	p = p.catch((err) => alert(err));
}

function DessusVoie(VoieSel) {
	document.getElementById('LigneVoie' + VoieSel).style.backgroundColor = "dark" + document.getElementById('LigneVoie' + VoieSel).style.backgroundColor;
}

function DeselectionVoie(VoieSel) {
	document.getElementById('LigneVoie' + VoieSel).style.backgroundColor = document.getElementById('LigneVoie' + VoieSel).style.backgroundColor.replace("dark", "");
}

DebutJsVoies();