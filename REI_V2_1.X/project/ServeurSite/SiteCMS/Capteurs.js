"use strict";
var SupprInit = 0;
var seconds = 0;
var imax = 0;
var TableSeuilsModifs = 0;
var Seuilini = 0;
var ValExcl = 0;
var tAttBarre = 20;
var NomId = "Voie";
if (url.searchParams.get('R')) {
    CarteOuVoie = 1;
    imax = 21;
    TableSeuilsModifs = Array(20).fill(20);
    Seuilini = Array(20).fill(20);
    ValExcl = 20;
} else {
    CarteOuVoie = 0;
    imax = 17;
    TableSeuilsModifs = Array(16).fill(101);
    Seuilini = Array(16).fill(101);
    ValExcl = 101;
	NomId = "Capteur";
}

let formChanged = false;
window.addEventListener('beforeunload', (event) => {
  if (formChanged) {
		event.preventDefault();
		event.returnValue = 'You have unfinished changes!';
  }
});

function DebutJsCapteurs() {
    let menu = '<div id="wrapper" hidden>';
    menu += '<div id=wp-background></div>';
    menu += '<p id="letemps"></p>';
    menu += '<div id=container>';
    menu += '<div id="loader" ></div>';
    menu += '<progress id="barreprogres" max='+(tAttBarre+20)+' value="0"></progress>';
    menu += '</div>';
    menu += "<p id='message'>En raison du fonctionnement du minitel, un délai de "+tAttBarre+" secondes est nécessaire sans compter les modifications des TP. Quitter la page est sans incidence et le process continuera automatiquement.</p>";
    menu += "<p id='infoErreur' hidden>Il semblerait que le boitier ne communique pas correctement avec le rack. Tentative de rétablissement en cours. La demande peut ne pas aboutir.</p>";
    menu += '</div>';
    document.body.insertAdjacentHTML("afterbegin", menu);
    menu = '<fieldset>';
    menu += '<legend>Modification des capteurs</legend>';
    menu += '<span>Pour modifier les seuils/déclarations du rack selon les informations du tableau ci-dessus, cliquer ici > </span>';
    menu += '<button id="MAJ" onclick="MiseAJourVoie()">Valider les modifications</button>';
    menu += '</fieldset>';
    menu += '<span class="hide">Icones provenants de svgrepo.com, sous licence CC0.</span>';
    document.getElementById("article").insertAdjacentHTML("beforeend", menu);

    let MaTable = document.createElement('table');
    MaTable.createCaption();
    let textCaption = "Données ";
    if (CarteOuVoie) {
        textCaption += "carte n° " + Carte;
    } else {
        textCaption += "voie n° " + Voie;
    }
    MaTable.caption.textContent = textCaption;

    let Thead = MaTable.createTHead();
    let ligne = Thead.insertRow(0);
    for (let i = 0; i < 5; i++) {
        ligne.insertCell(-1);
    }
    let x = MaTable.getElementsByTagName("td");
    let Txtseuil;
    if (CarteOuVoie) {
		for (let i = 0; i < 20; i++) {
            Txtseuil += "<option value='" + i + "'>" + cTblSeuilResi[i] + "</option>";
        }
    } else {
        for (let i = 0; i < 101; i++) {
            Txtseuil += "<option value='" + i + "'>" + (800 + i * 10) + "</option>";
        }
    }
    x[0].textContent = NomId;
    x[1].textContent = "Etat";
    x[2].innerHTML = "Seuil";
    x[2].innerHTML += " <img src='./question.svg' tabindex='0' class='aide' alt='Logo'/>";
    x[2].innerHTML += "<span class='hide'>La valeur affichée est celle du seuil contenu dans le rack. Vous pouvez modifier un seuil en cliquant dessus et en validant en bas de page. Le seuil n'est modifiable que si le capteur est déclaré.</span>";
    x[3].textContent = "Pression";
    x[4].id = "LabelTP";
    let TamponHTML = "TP <img class='aide' tabindex='0' src='./question.svg' alt='Logo'/><span class='hide'>Le curseur indique si le capteur est déclaré ou non sur le site. Vous pouvez déclarer ou libérer un capteur en cliquant sur le curseur correspondant et en validant avec le bouton en bas de page.</span>";
    let Debutlarge = "Libération/Déclaration du ";
    if (window.innerWidth <= 768) {
        x[4].innerHTML = TamponHTML;
    } else {
        x[4].innerHTML = Debutlarge + TamponHTML;
    }
    window.addEventListener('resize', event => {
        if (window.innerWidth <= 768) {
            document.getElementById("LabelTP").innerHTML = TamponHTML;
        } else {
            document.getElementById("LabelTP").innerHTML = Debutlarge + TamponHTML;
        }
    });

    let Tbody = MaTable.createTBody();

    for (let i = 1; i < imax; i++) {
        var Maligne = Tbody.insertRow(-1);
        Maligne.id = "LigneCapteur" + i;
        if (i % 2) {
            Maligne.style.background = '#305496';
        } else {
            Maligne.style.background = '#4472c4';
        }
        for (let j = 0; j < 5; j++) {
            let cell = Maligne.insertCell(-1);
            cell.textContent = "???";
        }
        let y = Maligne.getElementsByTagName("td");
        if (!CarteOuVoie) {
            y[0].textContent = NomId + " " + i;
        }else{
			y[0].innerHTML = NomId + " " + (CarteOffset + i) + txtCom1 + i + txtCom2 + i + txtCom3;
		}
		y[0].id = NomId + i;
        y[1].id = "Etat" + i;
        y[2].innerHTML = "<select tabindex='"+i+"' id='Seuil" + i + "' disabled>" + Txtseuil + "</select>"
        y[3].id = "Pression" + i;
        y[4].id = "Decla" + i;
        y[4].innerHTML = "<label style='cursor: pointer;'><input type='checkbox' tabindex='0' class='switch_1' id='SupCapteur" + i + "'></label>";
    }
    document.getElementById('Tableau').appendChild(MaTable);

    document.getElementById('MAJ').addEventListener("click", function (event) {
        event.preventDefault();
    }, false);

    for (let i = 1; i < imax; i++) {
        document.getElementById("SupCapteur" + i).checked = false;
        document.getElementById("SupCapteur" + i).addEventListener("change", function (event) {
            if (this.checked === true) {
                document.getElementById('Seuil' + i).removeAttribute('disabled');
                document.getElementById(NomId + i).style.color = "white";
                document.getElementById('Pression' + i).style.color = "white";
                Seuilini[i - 1] = ValExcl;
            } else {
                document.getElementById('Seuil' + i).setAttribute('disabled', '');
                document.getElementById(NomId + i).style.color = "darkgrey";
                document.getElementById('Pression' + i).style.color = "darkgrey";
            }
            formChanged = true;
        }, false);
        document.getElementById("Seuil" + i).addEventListener("change", function (event) {
            formChanged = true;
        }, false);
    }
	MiseAJour();
}

/*Actualisation des capteurs*/
function MiseAJour() {
    let NumVoie = Voie;
    let NumCapteur = 1;
	let p = Promise.resolve();
    for (let i = 1; i < imax; i++) {
        if (CarteOuVoie) {
            NumVoie = CarteOffset + i;
        } else {
            NumCapteur = i;
        }
        p = p.then(newAJAXCommand('./Capteur.xml?V=' + NumVoie + '&T=' + NumCapteur, MiseAJourCapteur));
    }
	p = p.catch((err) => alert(err));
}

function MiseAJourCapteur(xmlData) {
    var Numero = 0;
    if (CarteOuVoie) {
        Numero = parseInt(getXMLValue(xmlData, 'NumeroVoie'), 10) - CarteOffset;
    } else {
        Numero = getXMLValue(xmlData, 'Numcapteur');
    }
    let ValSeuil = getXMLValue(xmlData, 'Seuil');
    document.getElementById('Seuil' + Numero).value = ValSeuil;
    Seuilini[Numero - 1] = ValSeuil;
    document.getElementById('Pression' + Numero).innerHTML = getXMLValue(xmlData, 'Pression');
    document.getElementById("SupCapteur" + Numero).checked = true;
    document.getElementById('Seuil' + Numero).removeAttribute('disabled');
    document.getElementById('Etat' + Numero).style.backgroundColor = "auto";
    document.getElementById(NomId + Numero).style.color = "white";
    document.getElementById('Pression' + Numero).style.color = "white";
    document.getElementById('Etat' + Numero).style.color = "white";
    switch (getXMLValue(xmlData, 'Etat')) {
    case "0":
        document.getElementById('Etat' + Numero).textContent = "OK";
        document.getElementById('Etat' + Numero).style.color = "greenyellow";
        break;
    case "99":
        document.getElementById('Etat' + Numero).textContent = "Non présent";
        document.getElementById('Pression' + Numero).innerHTML = "N/A";
        document.getElementById("SupCapteur" + Numero).checked = false;
        document.getElementById('Seuil' + Numero).setAttribute('disabled', '');
        document.getElementById('Etat' + Numero).style.color = "darkgrey";
        document.getElementById(NomId + Numero).style.color = "darkgrey";
        document.getElementById('Pression' + Numero).style.color = "darkgrey";
        SupprInit += Math.pow(2, (Numero - 1));
        break;
    case "31":
        document.getElementById('Etat' + Numero).textContent = "En intervention";
        document.getElementById('Etat' + Numero).style.color = "orange";
        break;
    case "50":
        document.getElementById('Etat' + Numero).textContent = "Sans réponse";
        document.getElementById('Etat' + Numero).style.backgroundColor = "red";
        break;
    case "80":
        document.getElementById('Etat' + Numero).textContent = "Court-circuit";
        document.getElementById('Pression' + Numero).innerHTML = 'DEF!';
        document.getElementById('Etat' + Numero).style.backgroundColor = "red";
        break;
    default:
        document.getElementById('Etat' + Numero).textContent = "Alarme";
        document.getElementById('Etat' + Numero).style.backgroundColor = "red";
        break;
    }
}

function MiseAJourVoie() {
    let Valeur = 0;
    for (let i = 1; i < imax; i++) {
        if (document.getElementById("SupCapteur" + i).checked == false) {
            Valeur += Math.pow(2, (i - 1));
        } else {
            TableSeuilsModifs[i - 1] = document.getElementById("Seuil" + i).value;
        }
    }
    SupprInit ^= Valeur;
    Valeur &= SupprInit;
    var p = Promise.resolve();
    if (Valeur !== 0) {
        p = p.then(newAJAXCommand("./protect/Vide.xml?Suppression=" + Valeur));
    }
    let CaptApp = 1;
    let VoieApp = Voie;
    for (let i = 1; i < imax; i++) {
        if (TableSeuilsModifs[i - 1] !== ValExcl) {
            if (TableSeuilsModifs[i - 1] !== Seuilini[i - 1]) {
                if (CarteOuVoie) {
                    VoieApp = CarteOffset + i;
                } else {
                    CaptApp = i;
                }
                p = p.then(newAJAXCommand('./protect/Vide.xml?V=' + VoieApp + '&T=' + CaptApp + '&S=' + TableSeuilsModifs[i - 1]));
            }
            TableSeuilsModifs[i - 1] = ValExcl;
        }
    }
	p = p.catch((err) => alert(err));
	setTimeout(function(){newAJAXCommand('./protect/Barre.xml', ActuBarreInit).catch((err) => alert(err));}, 500);
}

function ActuBarreInit(xmlData) {
    var countDownDate = new Date().getTime();
    var lintervalle = setInterval(function () {
        var now = new Date().getTime();
        var distance = now - countDownDate;
        seconds = tAttBarre - Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("letemps").innerHTML = "Connexion. Encore " + seconds + " secondes.";
        document.getElementById("barreprogres").value = tAttBarre - seconds;
        if (seconds <= 0) {
            clearInterval(lintervalle);
            document.getElementById("letemps").innerHTML = "Actualisation en cours...";
            newAJAXCommand('./protect/Barre.xml', ActuBarre).catch((err) => alert(err));
        }
    }, 1000);
    document.getElementById('wrapper').hidden = false;
}

function ActuBarre(xmlData) {
    var cProgres = parseInt(getXMLValue(xmlData, 'Valeur'), 10);
    if (cProgres >= 21) {
        cProgres -= 21;
        document.getElementById('infoErreur').hidden = false;
    }
    document.getElementById("barreprogres").value = tAttBarre + cProgres;
    if (document.getElementById("barreprogres").value >= document.getElementById("barreprogres").max) {
        formChanged = false;
        window.location.reload();
    } else {
        setTimeout(function(){newAJAXCommand('./protect/Barre.xml', ActuBarre).catch((err) => alert(err));}, 5000);
    }
}

DebutJsCapteurs();