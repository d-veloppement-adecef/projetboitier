"use strict";
function DebutJsBlocNotes(){ 
	let menu = '<fieldset>';
	menu += '<legend>Modification des commentaires</legend>';
	menu += '<p>';
	menu += '<label for="Parametre">Commentaire à choisir : </label>';
	menu +=	'<select name="Paramètre" id="Parametre" required>';
	menu += '<option value="">--Choisir un commentaire--</option>';
	menu += '</select>';
	menu += '<br><label for="Commentaire">Commentaire : </label>';
	menu += '<input type="text" name="Commentaire" style="min-width:40%" id="Commentaire" maxlength="60" autocomplete="off"/>';
	menu += '<button type="button" id="EnvoiComm" onclick="ValidationComm()">Modifier Commentaire</button>';
	menu += '</p>';
	menu += '</fieldset>';
	menu += "<div id='snErr'>❌ Merci d'utiliser des caractères valides (Ascii et Ascii étendu)</div>";
	document.getElementById("article").insertAdjacentHTML("beforeend",menu);
	for (let i = 1; i < 21; i++) {
        document.getElementById("Parametre").innerHTML += "<option value=" + i + " id = 'VoieCom" + i + "'>"+ document.getElementById("Voie"+i).textContent + "</option>";
    }
    document.querySelector("#Parametre").addEventListener("change", function (event) {
		let Comm = document.getElementById('Commentaire');
		let Param = document.getElementById("Parametre").value;
        Comm.value = null;
		if (Param != ""){
			if(document.getElementById('Cache' + Param).innerHTML != null)
				Comm.value = document.getElementById('Cache' + Param).innerHTML; 
        }
    }, false);
	
    document.getElementById("Commentaire").addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            document.getElementById("EnvoiComm").click();
        }
    });
	ActuCommentaire();
}

function ValidationComm() {
	if (/^[\u001f-\u00ff]*$/.test(document.getElementById("Commentaire").value) === true) {
		newAJAXCommand("./protect/Vide.xml?V=" + (parseInt(document.getElementById("Parametre").value)+CarteOffset) + "&Comm=" + document.getElementById("Commentaire").value, ActuCommentaire).catch((err) => alert(err));
	} else {
		SN("snErr");
	}
}
function ActuCommentaire(){
	var p = Promise.resolve();
	for (let i = 1, p = Promise.resolve(); i < 21; i++) {
	    p = p.then(newAJAXCommand('./Commentaire.xml?V=' + (CarteOffset + i), AjoutCommentaire));
	}
	p = p.catch((err) => alert(err));
}
function AjoutCommentaire(xmlData) {
	let NumVoie = (parseInt(getXMLValue(xmlData, 'NumeroVoie'))-1)%20+1;
    if (getXMLValue(xmlData, 'Commentaire') != null) {
        document.getElementById("Bulle" + NumVoie).hidden = false;
		document.getElementById("Cache" + NumVoie).innerHTML = getXMLValue(xmlData, 'Commentaire');
	}else{
		document.getElementById("Bulle" + NumVoie).hidden = true;
		document.getElementById("Cache" + NumVoie).innerHTML = null;
	}
}

DebutJsBlocNotes();