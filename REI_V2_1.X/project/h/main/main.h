/* 
 * File:   main.h
 * Author: LFRAD
 *
 * Created on 25 novembre 2021, 11:35
 */
#ifndef _MAIN_H
#define _MAIN_H

#define VersionBtr 292

//#define LED2 //Mot pour les LED aux couleurs invers�es

#define DTR PORTHbits.RH1 //Logique inverse : Si DTR = 0, on veut communiquer
#define DSR PORTHbits.RH2 //Logique inverse : Si DSR = 1, le rack n'est pas connect�

#define Raz 	PORTBbits.RB1
#define Refresh PORTBbits.RB0
#define Swap    PORTFbits.RF0

#define LEDc PORTAbits.RA2

#ifdef LED2
#define LEDd PORTAbits.RA4
#define LEDe PORTGbits.RG0
#else
#define LEDd PORTGbits.RG0
#define LEDe PORTAbits.RA4
#endif

#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF

#define ACK 0x06
#define NACK 0x15
#define STX 0x02
#define ETX 0x03
#define EOT 0x04
#define CarriageReturn 0x0D

#define VoieCarteGroupeNonDeclare 100

#define Page 128
#define PageParametreGroupe 12800
#define PageNomSite 12928
#define PageIP 13056
#define PageMDP 13184
#define PageBlocNotes 13312
#define NbPageMaxBN 60
#define NombreCapteursVoie 16
#define OctetsCapteur 7
#define PositionEtatVoie (OctetsCapteur*NombreCapteursVoie) //112 pour 7*16
#define NombreVoiesCarte 20

#define TamponHaut 4200
#define TamponBas 1500
#define SortieHaut 550
#define SortieBas 450
#define SecoursBas 3000
#define SecoursTresBas 1500
#define RoseeHaut 253
#define RoseeBas 0
#define DebitHaut 0
#define DebitBas 0
#define CycleHaut 0
#define CycleBas 0
#define MaintenanceHaut 1000
#define MaintenanceBas 0
#define V220Haut 1
#define V220Bas 0
#define V48Haut 1
#define V48Bas 0

#define EEPROM_HOLD_TRIS    TRISEbits.TRISE1
#define EEPROM_HOLD_IO      PORTEbits.RE1

typedef unsigned short int WORD;

//ConversionREICMS.c
void ConversionParamGroupeREICMS(unsigned char *cTrameLectEEPROM);
void InitialisationParametresGroupe(void);
int CalculGroupe(char selection, int valeur);
void VoieLogiqueAdressable(unsigned char *cTrameLectEEPROM1, unsigned char *cTrameLectEEPROM2);
void LogiqueResistive(unsigned char *cTrameLectEEPROM1, unsigned char *cTrameLectEEPROM2);
unsigned int retour_valeur_tableau_REI(unsigned int valeur);
unsigned int HexASCIIToInt(char *cTableHex, char cTaille);
void InitialisationDonneesCMS(unsigned char *cEtatVoie, unsigned int *iRepos, unsigned char *cEtatCapteur, unsigned int *iSeuil, unsigned int *iPression, char *cConso, char *cModul);

//CRC.c
unsigned int CRC16(unsigned char *cTrame, unsigned int iLongueur);

//FonctionsI2CBase.c
signed char EESequRead(unsigned char cControl, unsigned int address, unsigned char *rdptr, unsigned char length);
signed char EEPaWrite(unsigned char cControl, unsigned int address, unsigned char *wrptr);
signed char AckPolling(unsigned char cControl);
signed char gets(unsigned char *rdptr, unsigned char length);
signed char Write(unsigned char data_out);
signed char put(unsigned char *wrptr, unsigned char cLong);

//I2C.c
void MiseajourEtatVoie(void);
unsigned char MaJUserEmplacementParam(unsigned char cVoieAbs);
void EcritureParametresGroupe(unsigned int* iSeuilBas, unsigned int* iValeur, unsigned int* iSeuilHaut);
void LectureParametresGroupe(unsigned int* iSeuilBas, unsigned char* cEtat, unsigned int* iValeur, unsigned int* iSeuilHaut);
void ModificationParametresGroupes(char cParametre, int iSeuilBas, int iSeuilHaut, int iValeur, unsigned char cLectureEcriture);
void EcritureNomSiteMDP(unsigned char* cTrameNom, unsigned int iEmplacement);
unsigned char EcritureEmplacementParam(unsigned char cVoieAbs);
void EcritureDonneesVoie(unsigned char cVoieAbs, unsigned char* cEtatCapteur,
        unsigned int* iSeuil,
        unsigned int* iPression,
        unsigned char* cConso,
        unsigned char* cModul,
        unsigned char cEtatVoie,
        unsigned int iRepos);
void InitEeprom(void);
void LectureDonneesTelgat(unsigned char cVoie, unsigned char* cEtatCapteur,
        unsigned int* iSeuil,
        unsigned int* iPression);
unsigned char RecuperationNombreCapteur(unsigned char cVoieAbs);
void StockageIP0Masque1Pass2(unsigned char cE1, unsigned char cE2, unsigned char cE3, unsigned char cE4, unsigned char cDelta);
void LectureIP0Masque1Pass2(unsigned char* cE1, unsigned char* cE2, unsigned char* cE3, unsigned char* cE4, unsigned char cDelta);
void ProtocoleReset(void);
void RemplissageTrame(unsigned char cDebut, unsigned char cFin, unsigned char cCarac);
void MiseajourSeuil(unsigned char cVoieAbs, unsigned char cCapteur, unsigned int iSeuil, signed int iPression);
void EcritureCommentaire(unsigned char cVoieAbs, unsigned char* cCommentaire);
void RecupCommentaire(unsigned char cVoieAbs, unsigned char* cCommentaire);
void RemplissageEEPROM(void);
void InitialisationEEPROM(void);
unsigned char REI_Abs(unsigned char cCarteREI, unsigned char cVoieREI);
unsigned int Char2_Int(unsigned char cHaut, unsigned char cBas);
void Int_2Char(unsigned int iValeur, unsigned char* cResult);
//InitIO.c
void INITGESTIONCARTEIO(void);

//modem.c
void modem(void);

//TrameTelgat.c
void TypeDeCom(unsigned char* cTrame, unsigned char cType, unsigned int* iLong);
void NomDuSite(unsigned char* cTrame, unsigned int* iLong);
void ModeDeCom(unsigned char* cTrame, unsigned int* iLong);
void TrameCable(unsigned char* cTrame, char* cVoie, unsigned int* cLong);
void IntToChar(unsigned int iValeur, unsigned char *cChaineCar, char cTaille);

//RS232.c
//unsigned char RS232_RecoitTrame(unsigned char *cTableau, char cLongueur, unsigned char cCarac);
void RS232_EnvoiTrame(unsigned char *cTableau, char cLongueur);
void RS232_EnvoiCarac(unsigned char cCarac);

//minitel.c
char Reinit_mdp_plus_saisie(void);
void Selection_voieD_capteurd(char cValeur, char cType);
void Augmentation_Diminution_Seuil(char cModif);
void Declaration_Liberation(void);
//void Intervention_Alarme(void);
void minitel(void);
void DELAY_X50MS(unsigned char cTemps);
void CreaLibe1Modif2Capteur(unsigned char cOrdre, unsigned char cVoieAbs, unsigned char cCapteur, unsigned int iSeuil);
void ModeRouleauDefaut(void);
void InitRecept(void);
void FinRecept(void);
unsigned char CheckRecept(unsigned char cCarac);
void Reception(void);
#endif // _MAIN_H
