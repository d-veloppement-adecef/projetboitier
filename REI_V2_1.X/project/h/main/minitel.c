#include <stdio.h>
#include <stdlib.h>
#include <p18F97J60.h>
#include <main.h>
#include <i2c.h>
#include <tick.h>

extern unsigned char cDemandeMinitel;
extern unsigned char cCommande;
extern BYTE dataBufferLength;
unsigned char cTrameLecture[128];

void minitel(void) {
    char cCompteur = 0;
    char cBcl = 0;
    //static volatile unsigned char cTblRecept[80];
    static DWORD t = 0;

    static enum _EtatMinitel {
        SM_HOME = 0,
        SM_DIALOG,
        SM_CLOSING,
    } EtatMinitel = SM_HOME;

    static enum _EtatREI {
        Accroche = 0,
        Debut,
        Milieu,
        Fin,
        Dialogue,
    } EtatREI = Accroche;

    switch (EtatMinitel) {
        case SM_HOME: //Etat par d�faut quand il n'y a aucune communication
            //Correctif mis en place : Une fois toutes les 10-15 minutes, le rack REI se met indisponible, et envoie une trame sp�ciale d�s qu'il est disponible.
            //Attendre une seconde �vite d'avoir un conflit.
            if ((cDemandeMinitel)&&(!DSR)) { //Une fois par heure, ou si l'utilisateur le demande en local ou � distance, et que le rack est disponible
                LEDc = 0; //On �teint la LED rouge, et on allume la verte
                LEDd = 1;
                DTR = 0; //On demande une communication avec le rack
                cTrameLecture[0] = 0x13; // Il faut envoyer '<DC3>Y<DC3>[<DC3>S' ()
                cTrameLecture[1] = 0x59;
                cTrameLecture[2] = 0x13;
                cTrameLecture[3] = 0x5B;
                cTrameLecture[4] = 0x13;
                cTrameLecture[5] = 0x53;
                RS232_EnvoiTrame((unsigned char *) cTrameLecture, 6);
                EtatMinitel = SM_DIALOG; //Etape suivante
            }
            break;

        case SM_DIALOG:
            switch (EtatREI) { //Gestion de la communication avec REI.
                case Accroche: //Tentatives d'accroche avec le rack REI.
                    do {
                        RS232_RecoitTrame((unsigned char *) cTrameLecture, 3, 'a'); //On a demand� la communication avec REI, celui-ci doit r�pondre <FF><ESC>a (0x0C 0x1B 0x61)
                        if ((cTrameLecture[1] == 0x1B) && (cTrameLecture[2] == 'a')) //Si on a bien re�u '<ESC>a'
                            break; //On sort de la boucle d'accroche
                        cCompteur++; //Sinon, on incr�mente le compteur
                        if (cCompteur >= 5) { //Si le compteur d�passe 4
                            EtatMinitel = SM_CLOSING; //On sort de la communication
                            return;
                        }
                        LEDd = 1; //On s'assure que la LED verte est allum�e 
                        DTR = 1; //On coupe la demande de communcation
                        t = TickGet();
                        while ((DSR)&&(TickGet() - t <= (DWORD) (TICK_SECOND * 10ul))); //On attend que le rack soit disponible. Eventuellement, on sort au bout de 10 secondes
                        t = TickGet();
                        while (TickGet() - t <= (DWORD) (TICK_SECOND * 2ul)); //On attend 2 secondes apr�s que le rack soit disponible
                        DTR = 0; //On relance une demande de communication
                    } while ((cTrameLecture[1] != 0x1B) || (cTrameLecture[2] != 'a')); //Tant qu'on a pas re�u ce qu'il faut, on continue
                    EtatREI++; //Etape suivante
                    break;

                case Debut: //Initiation connexion minitel
                    cTrameLecture[0] = 0x1F; // Pour r�pondre au rack, il faut renvoyer '<US>AA' (0x1F 0x41 0x41)
                    cTrameLecture[1] = 0x41;
                    cTrameLecture[2] = 0x41;
                    RS232_EnvoiTrame((unsigned char *) cTrameLecture, 3);
                    EtatREI++; //Etape suivante
                    break;

                case Milieu: //Tentatives d'accroche avec le rack REI.
                    do {
                        RS232_RecoitTrame((unsigned char *) cTrameLecture, 10, 0x0A); //REI va nous demander de passer en mode rouleau '<FF><ESC>:iC'
                        if ((cTrameLecture[1] == 0x1B) && (cTrameLecture[4] == 'C')) //Si on a bien re�u '<FF><ESC>:iC'
                            break; //On sort de la boucle d'accroche
                        cCompteur++; //Sinon, on incr�mente le compteur
                        if (cCompteur >= 5) { //Si le compteur d�passe 4
                            EtatMinitel = SM_CLOSING; //On sort de la communication
                            return;
                        }
                        LEDd = 1; //On s'assure que la LED verte est allum�e 
                        DTR = 1; //On coupe la demande de communcation
                        t = TickGet();
                        while ((DSR)&&(TickGet() - t <= (DWORD) (TICK_SECOND * 10ul))); //On attend que le rack soit disponible. Eventuellement, on sort au bout de 10 secondes
                        t = TickGet();
                        while (TickGet() - t <= (DWORD) (TICK_SECOND * 2ul)); //On attend 2 secondes apr�s que le rack soit disponible
                        DTR = 0; //On relance une demande de communication
                    } while ((cTrameLecture[0] != 0x1B) || (cTrameLecture[3] != 'C')); //Tant qu'on a pas re�u ce qu'il faut, on continue
                    EtatREI++; //Etape suivante
                    break;

                case Fin: //Initiation connexion minitel
                    //                    cTrameLecture[0] = 0x1B; // Pour r�pondre au rack, il faut renvoyer '<ESC>:sB'
                    //                    cTrameLecture[1] = ':';
                    //                    cTrameLecture[2] = 's';
                    //                    cTrameLecture[3] = 'B';
                    //                    RS232_EnvoiTrame((unsigned char *) cTrameLecture, 4);
                    EtatREI++; //Etape suivante
                    break;

                case Dialogue: //Echange
                    //do {
                    //DTR = 0;
                    RS232_RecoitTrame((unsigned char *) cTrameLecture, 80, 0x0A);
                    for (cBcl = 0; cBcl < 80; cBcl++) {
                        if ((cTrameLecture[cBcl] == 0x0D) || (cTrameLecture[cBcl] == 0x0A)) {
                            dataBufferLength = cBcl - 1;
                        }
                    }
                    //                        if (dataBufferLength != 0) {
                    ////                            memcpy(curHTTP.data, (void *) cTrameLecture, dataBufferLength);
                    //                            WebSocketSendPayload_Table(WS_OPCODE_TEXT, dataBufferLength, (BYTE *)cTrameLecture);
                    //                            dataBufferLength = 0;
                    //                        }
                    //DTR = 1;
                    //} while (1);
                    //                    if (cCommande != 0) {
                    //                        RS232_EnvoiTrame(&cCommande, 1);
                    //                        cCommande = 0;
                    //                    }
                    break;
            }
            break;
        case SM_CLOSING:
            LEDd = 0; //Extinction de la LED verte
            DTR = 1; //Fermeture de la communication avec REI
            EtatMinitel = SM_HOME; //R�initialisation des �tats
            EtatREI = Accroche;
            cDemandeMinitel = 0; //On r�initialise la demande
            break;
    }
}
