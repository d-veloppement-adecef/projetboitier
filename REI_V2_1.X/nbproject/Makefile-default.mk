#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=project/main/CRC.c project/main/GenericTCPServer.c project/main/I2C.c project/main/InitIO.c project/main/RS232.c project/main/TrameTelgat.c project/ConversionREICMS.c project/main/FonctionsI2CBase.c project/main/main.c project/main/modem.c project/minitel.c project/TCPIP_Stack/ARP.c project/TCPIP_Stack/CustomHTTPApp.c project/TCPIP_Stack/ETH97J60.c project/TCPIP_Stack/Helpers.c project/TCPIP_Stack/HTTP2.c project/TCPIP_Stack/ICMP.c project/TCPIP_Stack/IP.c project/TCPIP_Stack/MPFS2.c project/TCPIP_Stack/StackTsk.c project/TCPIP_Stack/TCP.c project/TCPIP_Stack/Tick.c project/TCPIP_Stack/Reboot.c project/TCPIP_Stack/UDP.c project/MPFSImg2.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/project/main/CRC.o ${OBJECTDIR}/project/main/GenericTCPServer.o ${OBJECTDIR}/project/main/I2C.o ${OBJECTDIR}/project/main/InitIO.o ${OBJECTDIR}/project/main/RS232.o ${OBJECTDIR}/project/main/TrameTelgat.o ${OBJECTDIR}/project/ConversionREICMS.o ${OBJECTDIR}/project/main/FonctionsI2CBase.o ${OBJECTDIR}/project/main/main.o ${OBJECTDIR}/project/main/modem.o ${OBJECTDIR}/project/minitel.o ${OBJECTDIR}/project/TCPIP_Stack/ARP.o ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o ${OBJECTDIR}/project/TCPIP_Stack/IP.o ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o ${OBJECTDIR}/project/TCPIP_Stack/TCP.o ${OBJECTDIR}/project/TCPIP_Stack/Tick.o ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o ${OBJECTDIR}/project/TCPIP_Stack/UDP.o ${OBJECTDIR}/project/MPFSImg2.o
POSSIBLE_DEPFILES=${OBJECTDIR}/project/main/CRC.o.d ${OBJECTDIR}/project/main/GenericTCPServer.o.d ${OBJECTDIR}/project/main/I2C.o.d ${OBJECTDIR}/project/main/InitIO.o.d ${OBJECTDIR}/project/main/RS232.o.d ${OBJECTDIR}/project/main/TrameTelgat.o.d ${OBJECTDIR}/project/ConversionREICMS.o.d ${OBJECTDIR}/project/main/FonctionsI2CBase.o.d ${OBJECTDIR}/project/main/main.o.d ${OBJECTDIR}/project/main/modem.o.d ${OBJECTDIR}/project/minitel.o.d ${OBJECTDIR}/project/TCPIP_Stack/ARP.o.d ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o.d ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o.d ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o.d ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o.d ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o.d ${OBJECTDIR}/project/TCPIP_Stack/IP.o.d ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o.d ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o.d ${OBJECTDIR}/project/TCPIP_Stack/TCP.o.d ${OBJECTDIR}/project/TCPIP_Stack/Tick.o.d ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o.d ${OBJECTDIR}/project/TCPIP_Stack/UDP.o.d ${OBJECTDIR}/project/MPFSImg2.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/project/main/CRC.o ${OBJECTDIR}/project/main/GenericTCPServer.o ${OBJECTDIR}/project/main/I2C.o ${OBJECTDIR}/project/main/InitIO.o ${OBJECTDIR}/project/main/RS232.o ${OBJECTDIR}/project/main/TrameTelgat.o ${OBJECTDIR}/project/ConversionREICMS.o ${OBJECTDIR}/project/main/FonctionsI2CBase.o ${OBJECTDIR}/project/main/main.o ${OBJECTDIR}/project/main/modem.o ${OBJECTDIR}/project/minitel.o ${OBJECTDIR}/project/TCPIP_Stack/ARP.o ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o ${OBJECTDIR}/project/TCPIP_Stack/IP.o ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o ${OBJECTDIR}/project/TCPIP_Stack/TCP.o ${OBJECTDIR}/project/TCPIP_Stack/Tick.o ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o ${OBJECTDIR}/project/TCPIP_Stack/UDP.o ${OBJECTDIR}/project/MPFSImg2.o

# Source Files
SOURCEFILES=project/main/CRC.c project/main/GenericTCPServer.c project/main/I2C.c project/main/InitIO.c project/main/RS232.c project/main/TrameTelgat.c project/ConversionREICMS.c project/main/FonctionsI2CBase.c project/main/main.c project/main/modem.c project/minitel.c project/TCPIP_Stack/ARP.c project/TCPIP_Stack/CustomHTTPApp.c project/TCPIP_Stack/ETH97J60.c project/TCPIP_Stack/Helpers.c project/TCPIP_Stack/HTTP2.c project/TCPIP_Stack/ICMP.c project/TCPIP_Stack/IP.c project/TCPIP_Stack/MPFS2.c project/TCPIP_Stack/StackTsk.c project/TCPIP_Stack/TCP.c project/TCPIP_Stack/Tick.c project/TCPIP_Stack/Reboot.c project/TCPIP_Stack/UDP.c project/MPFSImg2.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk ${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F97J60
MP_PROCESSOR_OPTION_LD=18f97j60
MP_LINKER_DEBUG_OPTION=  -u_DEBUGSTACK
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/project/main/CRC.o: project/main/CRC.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/CRC.o.d 
	@${RM} ${OBJECTDIR}/project/main/CRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/CRC.o   project/main/CRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/CRC.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/CRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/GenericTCPServer.o: project/main/GenericTCPServer.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/GenericTCPServer.o.d 
	@${RM} ${OBJECTDIR}/project/main/GenericTCPServer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/GenericTCPServer.o   project/main/GenericTCPServer.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/GenericTCPServer.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/GenericTCPServer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/I2C.o: project/main/I2C.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/I2C.o.d 
	@${RM} ${OBJECTDIR}/project/main/I2C.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/I2C.o   project/main/I2C.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/I2C.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/I2C.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/InitIO.o: project/main/InitIO.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/InitIO.o.d 
	@${RM} ${OBJECTDIR}/project/main/InitIO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/InitIO.o   project/main/InitIO.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/InitIO.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/InitIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/RS232.o: project/main/RS232.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/RS232.o.d 
	@${RM} ${OBJECTDIR}/project/main/RS232.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/RS232.o   project/main/RS232.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/RS232.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/RS232.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/TrameTelgat.o: project/main/TrameTelgat.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/TrameTelgat.o.d 
	@${RM} ${OBJECTDIR}/project/main/TrameTelgat.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/TrameTelgat.o   project/main/TrameTelgat.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/TrameTelgat.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/TrameTelgat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/ConversionREICMS.o: project/ConversionREICMS.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project" 
	@${RM} ${OBJECTDIR}/project/ConversionREICMS.o.d 
	@${RM} ${OBJECTDIR}/project/ConversionREICMS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/ConversionREICMS.o   project/ConversionREICMS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/ConversionREICMS.o 
	@${FIXDEPS} "${OBJECTDIR}/project/ConversionREICMS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/FonctionsI2CBase.o: project/main/FonctionsI2CBase.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/FonctionsI2CBase.o.d 
	@${RM} ${OBJECTDIR}/project/main/FonctionsI2CBase.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/FonctionsI2CBase.o   project/main/FonctionsI2CBase.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/FonctionsI2CBase.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/FonctionsI2CBase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/main.o: project/main/main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/main.o.d 
	@${RM} ${OBJECTDIR}/project/main/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/main.o   project/main/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/main.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/modem.o: project/main/modem.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/modem.o.d 
	@${RM} ${OBJECTDIR}/project/main/modem.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/modem.o   project/main/modem.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/modem.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/modem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/minitel.o: project/minitel.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project" 
	@${RM} ${OBJECTDIR}/project/minitel.o.d 
	@${RM} ${OBJECTDIR}/project/minitel.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/minitel.o   project/minitel.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/minitel.o 
	@${FIXDEPS} "${OBJECTDIR}/project/minitel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/ARP.o: project/TCPIP_Stack/ARP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ARP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ARP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/ARP.o   project/TCPIP_Stack/ARP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/ARP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/ARP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o: project/TCPIP_Stack/CustomHTTPApp.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o   project/TCPIP_Stack/CustomHTTPApp.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o: project/TCPIP_Stack/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o   project/TCPIP_Stack/ETH97J60.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/Helpers.o: project/TCPIP_Stack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o   project/TCPIP_Stack/Helpers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/Helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o: project/TCPIP_Stack/HTTP2.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o   project/TCPIP_Stack/HTTP2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/ICMP.o: project/TCPIP_Stack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o   project/TCPIP_Stack/ICMP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/ICMP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/IP.o: project/TCPIP_Stack/IP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/IP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/IP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/IP.o   project/TCPIP_Stack/IP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/IP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/IP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o: project/TCPIP_Stack/MPFS2.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o   project/TCPIP_Stack/MPFS2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o: project/TCPIP_Stack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o   project/TCPIP_Stack/StackTsk.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/TCP.o: project/TCPIP_Stack/TCP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/TCP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/TCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/TCP.o   project/TCPIP_Stack/TCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/TCP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/TCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/Tick.o: project/TCPIP_Stack/Tick.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Tick.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Tick.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/Tick.o   project/TCPIP_Stack/Tick.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/Tick.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/Tick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/Reboot.o: project/TCPIP_Stack/Reboot.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o   project/TCPIP_Stack/Reboot.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/Reboot.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/UDP.o: project/TCPIP_Stack/UDP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/UDP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/UDP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/UDP.o   project/TCPIP_Stack/UDP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/UDP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/UDP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/MPFSImg2.o: project/MPFSImg2.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project" 
	@${RM} ${OBJECTDIR}/project/MPFSImg2.o.d 
	@${RM} ${OBJECTDIR}/project/MPFSImg2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/MPFSImg2.o   project/MPFSImg2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/MPFSImg2.o 
	@${FIXDEPS} "${OBJECTDIR}/project/MPFSImg2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/project/main/CRC.o: project/main/CRC.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/CRC.o.d 
	@${RM} ${OBJECTDIR}/project/main/CRC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/CRC.o   project/main/CRC.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/CRC.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/CRC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/GenericTCPServer.o: project/main/GenericTCPServer.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/GenericTCPServer.o.d 
	@${RM} ${OBJECTDIR}/project/main/GenericTCPServer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/GenericTCPServer.o   project/main/GenericTCPServer.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/GenericTCPServer.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/GenericTCPServer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/I2C.o: project/main/I2C.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/I2C.o.d 
	@${RM} ${OBJECTDIR}/project/main/I2C.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/I2C.o   project/main/I2C.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/I2C.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/I2C.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/InitIO.o: project/main/InitIO.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/InitIO.o.d 
	@${RM} ${OBJECTDIR}/project/main/InitIO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/InitIO.o   project/main/InitIO.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/InitIO.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/InitIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/RS232.o: project/main/RS232.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/RS232.o.d 
	@${RM} ${OBJECTDIR}/project/main/RS232.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/RS232.o   project/main/RS232.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/RS232.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/RS232.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/TrameTelgat.o: project/main/TrameTelgat.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/TrameTelgat.o.d 
	@${RM} ${OBJECTDIR}/project/main/TrameTelgat.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/TrameTelgat.o   project/main/TrameTelgat.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/TrameTelgat.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/TrameTelgat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/ConversionREICMS.o: project/ConversionREICMS.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project" 
	@${RM} ${OBJECTDIR}/project/ConversionREICMS.o.d 
	@${RM} ${OBJECTDIR}/project/ConversionREICMS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/ConversionREICMS.o   project/ConversionREICMS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/ConversionREICMS.o 
	@${FIXDEPS} "${OBJECTDIR}/project/ConversionREICMS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/FonctionsI2CBase.o: project/main/FonctionsI2CBase.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/FonctionsI2CBase.o.d 
	@${RM} ${OBJECTDIR}/project/main/FonctionsI2CBase.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/FonctionsI2CBase.o   project/main/FonctionsI2CBase.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/FonctionsI2CBase.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/FonctionsI2CBase.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/main.o: project/main/main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/main.o.d 
	@${RM} ${OBJECTDIR}/project/main/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/main.o   project/main/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/main.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/main/modem.o: project/main/modem.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/main" 
	@${RM} ${OBJECTDIR}/project/main/modem.o.d 
	@${RM} ${OBJECTDIR}/project/main/modem.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/main/modem.o   project/main/modem.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/main/modem.o 
	@${FIXDEPS} "${OBJECTDIR}/project/main/modem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/minitel.o: project/minitel.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project" 
	@${RM} ${OBJECTDIR}/project/minitel.o.d 
	@${RM} ${OBJECTDIR}/project/minitel.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/minitel.o   project/minitel.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/minitel.o 
	@${FIXDEPS} "${OBJECTDIR}/project/minitel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/ARP.o: project/TCPIP_Stack/ARP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ARP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ARP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/ARP.o   project/TCPIP_Stack/ARP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/ARP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/ARP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o: project/TCPIP_Stack/CustomHTTPApp.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o   project/TCPIP_Stack/CustomHTTPApp.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/CustomHTTPApp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o: project/TCPIP_Stack/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o   project/TCPIP_Stack/ETH97J60.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/ETH97J60.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/Helpers.o: project/TCPIP_Stack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o   project/TCPIP_Stack/Helpers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/Helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/Helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o: project/TCPIP_Stack/HTTP2.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o   project/TCPIP_Stack/HTTP2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/HTTP2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/ICMP.o: project/TCPIP_Stack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o   project/TCPIP_Stack/ICMP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/ICMP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/ICMP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/IP.o: project/TCPIP_Stack/IP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/IP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/IP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/IP.o   project/TCPIP_Stack/IP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/IP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/IP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o: project/TCPIP_Stack/MPFS2.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o   project/TCPIP_Stack/MPFS2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/MPFS2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o: project/TCPIP_Stack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o   project/TCPIP_Stack/StackTsk.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/StackTsk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/TCP.o: project/TCPIP_Stack/TCP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/TCP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/TCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/TCP.o   project/TCPIP_Stack/TCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/TCP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/TCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/Tick.o: project/TCPIP_Stack/Tick.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Tick.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Tick.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/Tick.o   project/TCPIP_Stack/Tick.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/Tick.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/Tick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/Reboot.o: project/TCPIP_Stack/Reboot.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o   project/TCPIP_Stack/Reboot.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/Reboot.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/Reboot.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/TCPIP_Stack/UDP.o: project/TCPIP_Stack/UDP.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project/TCPIP_Stack" 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/UDP.o.d 
	@${RM} ${OBJECTDIR}/project/TCPIP_Stack/UDP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/TCPIP_Stack/UDP.o   project/TCPIP_Stack/UDP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/TCPIP_Stack/UDP.o 
	@${FIXDEPS} "${OBJECTDIR}/project/TCPIP_Stack/UDP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/project/MPFSImg2.o: project/MPFSImg2.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/project" 
	@${RM} ${OBJECTDIR}/project/MPFSImg2.o.d 
	@${RM} ${OBJECTDIR}/project/MPFSImg2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DCFG_INCLUDE_PICDN2_ETH97 -I"project" -I"project/h" -I"project/h/main" -I"project/h/TCPIP_Stack" -ml -oa- -o-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/project/MPFSImg2.o   project/MPFSImg2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/project/MPFSImg2.o 
	@${FIXDEPS} "${OBJECTDIR}/project/MPFSImg2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    project/18f97j60_g_Bootloader.lkr
	@${MKDIR} ${DISTDIR} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "project\18f97j60_g_Bootloader.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -l"project/h/main" -l"project/h/TCPIP_Stack"  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o ${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   project/18f97j60_g_Bootloader.lkr
	@${MKDIR} ${DISTDIR} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "project\18f97j60_g_Bootloader.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -l"project/h/main" -l"project/h/TCPIP_Stack"  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o ${DISTDIR}/REI_V2_1.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${OBJECTDIR}
	${RM} -r ${DISTDIR}

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
